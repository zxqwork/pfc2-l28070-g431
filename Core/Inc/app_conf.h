#ifndef _APP_CONFIG_H_
#define _APP_CONFIG_H_
//定义所使用的STM32芯片型号，为了页存贮用,只能唯一选择

#define APP_A_BEGIN_PAGE 14 //14页是28k 14-38页 48k
#define APP_B_BEGIN_PAGE 38 //38+24=62
#define APP_A_BEGIN_ADDR 0x08004000  // 
#define APP_B_BEGIN_ADDR 0x08013000  // 7
//stm32f103 256k 2k一页，
//第  0页数0X8000000 80007ff 
//    1页数0X8000800 8000fff
//  122页数0X803d000 803d7ff  
//  123页数0X803d800 803dfff
//  124页数0X803e000 803e7ff  
//  125页数0X803e800 803efff
//  126页数0X803f000 803f7ff  
//  127页数0X803f800 803ffff
#define CommAdd_IAP_init 61000
#define CommAdd_IAP_end  61032

//定义晶体
#define AT12MHZ

//定义通讯口的数量
#define IS_NEED_COMM

#ifdef  IS_NEED_COMM

#define MAX_XB_COMM_VAR 5

#define MCU_UART_NUM 1
#define COMM0_ARRAY_Rec_XB 2050
#define COMM0_ARRAY_SendXB 2050 //最大980字

#define COMM1_ARRAY_Rec_XB 1	
#define COMM1_ARRAY_SendXB 1

#define COMM2_ARRAY_Rec_XB 1 //运动控制板
#define COMM2_ARRAY_SendXB 1 //

#define COMM3_ARRAY_Rec_XB 1 //最大980字
#define COMM3_ARRAY_SendXB 1

#define COMM4_ARRAY_Rec_XB 1 //MFC
#define COMM4_ARRAY_SendXB 1

//V3.2
#define OP_TABLE_LENTH_0 1
#define OP_TABLE_LENTH_1 1  // 温度模块5008，AD模块5016（自由选配），高压电源兼容，法国电源有2个op
#define OP_TABLE_LENTH_2 1  //运动控制板（3个运动控制3个反射调节) 
#define OP_TABLE_LENTH_3 1 //高压电源 普通的读写，故障数据
#define OP_TABLE_LENTH_4 1 //预备6个流量控制器 读，写设定 写阀命令 + 2个样品温度传感器


#define ASC2RTU_TMP_BUF 1
#define MODBUS_RTU_ONLY
#define MODBUS_NoSECRET
#endif


#define USE_Protocol_Modbus


#define COUNT_INPUT_BYTE 1
#define COUNT_OUTPUT_BYTE 1

#define APP_USE_GPIO
#define APP_USE_GPIO_FILTER



//定义EEPROM
#define WR_EEP_TASK_STACK_NUM   1

#define USE_DMA_FOR_USART1  //@ app_conf.h
//#define USE_DMA_FOR_USART2  //@ app_conf.h
//#define USE_DMA_FOR_USART3  //@ app_conf.h 原來mfc在此端口，不可DMA
//#define USE_DMA_FOR_USART4  //@ app_conf.h 原来电机在此端口,现在改为电源
                            //5口不可DMA，改为mfc

#endif
