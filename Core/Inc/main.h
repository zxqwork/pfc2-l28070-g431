/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "app_conf.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define AD_CHA_Pin GPIO_PIN_0
#define AD_CHA_GPIO_Port GPIOA
#define AD_VIN_Pin GPIO_PIN_1
#define AD_VIN_GPIO_Port GPIOA
#define DA1_DEBUG1_Pin GPIO_PIN_4
#define DA1_DEBUG1_GPIO_Port GPIOA
#define DA1_DEBUG2_Pin GPIO_PIN_5
#define DA1_DEBUG2_GPIO_Port GPIOA
#define AD_VOUT_Pin GPIO_PIN_7
#define AD_VOUT_GPIO_Port GPIOA
#define RELAY_Pin GPIO_PIN_2
#define RELAY_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_12
#define LED2_GPIO_Port GPIOB
#define LED3_Pin GPIO_PIN_13
#define LED3_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_14
#define LED1_GPIO_Port GPIOB
#define AD_T3950_Pin GPIO_PIN_15
#define AD_T3950_GPIO_Port GPIOB
#define PWM_A_Pin GPIO_PIN_6
#define PWM_A_GPIO_Port GPIOC
#define USART1_DR_Pin GPIO_PIN_11
#define USART1_DR_GPIO_Port GPIOA
#define LED0_Pin GPIO_PIN_12
#define LED0_GPIO_Port GPIOA
#define SPI3_NSS_Pin GPIO_PIN_15
#define SPI3_NSS_GPIO_Port GPIOA
#define SS28070_Pin GPIO_PIN_6
#define SS28070_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;

extern DAC_HandleTypeDef hdac1;

extern SPI_HandleTypeDef hspi3;

extern TIM_HandleTypeDef htim3;

extern UART_HandleTypeDef huart1;

extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart1_tx;


/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
