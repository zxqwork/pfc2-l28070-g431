#ifndef _BSP_H_
#define  _BSP_H_
#include "base.h"
#include "app_conf.h"
#include "Comm.h"
#include "main.h"  //hal 生成的init在main里

#define RelayOn()   RELAY_GPIO_Port->BRR = RELAY_Pin
#define RelayOff()  RELAY_GPIO_Port->BSRR = RELAY_Pin

#define SS_Enable()      SS28070_GPIO_Port->BRR  = SS28070_Pin 
#define SS_Disable()     SS28070_GPIO_Port->BSRR  = SS28070_Pin 

#define PFCokDisable()   LED2_GPIO_Port->BRR  = LED2_Pin  //PFC没准备好的时候是亮的，准备好了就熄灯，常态是灭的 //PFC_READY_GPIO_Port->BSRR = PFC_READY_Pin
#define PFCokEnable()    LED2_GPIO_Port->ODR  ^= LED2_Pin  //PFC_READY_GPIO_Port->BRR = PFC_READY_Pin


#define EN_RS4850_SEND    USART1_DR_GPIO_Port->BRR = USART1_DR_Pin
#define DIS_RS4850_SEND   USART1_DR_GPIO_Port->BSRR = USART1_DR_Pin


#define Comm_LED_ON       LED0_GPIO_Port->BRR  = LED0_Pin
#define Comm_LED_OFF      LED0_GPIO_Port->BSRR = LED0_Pin
#define Comm_LED_FLASH    LED0_GPIO_Port->ODR  ^= LED0_Pin


u8 BSP_input(u8 uc_channel);
void BSP_output(void);
void DacDebug(long para,long ch);

/////////////////////////////////////////////////////////////////////////////
//板载支持包：强制收发转换切换，并同步is_sending标志位
/////////////////////////////////////////////////////////////////////////////
void BSP_Comm_485_ForceSend(unsigned char uc_is_send,st_Comm_TypeDef *p);


/////////////////////////////////////////////////////////////////////////////
//板载支持包：通讯发送
//
/////////////////////////////////////////////////////////////////////////////
void BSP_Comm_Uart_Send(u8 *uc_be_send,u16 lenth, u8 uc_port_num);

/////////////////////////////////////////////////////////////////////////////

void DMA_check_All_channel_rec_finish(u16 u16_Now_us);
void DMA_check_All_channel_send_finish(void);

//LED驱动放在此处
void DRV_Comm_Led(u8 LED_ON,st_Comm_TypeDef *C);


#endif