#ifndef _APP_VAR_H_ 
#define _APP_VAR_H_

#include "base.h"
#include "Comm.h"

#define SYS_FLAG_1MS_INT 1
extern volatile u32 gu32SysFlag;
extern volatile u32 gu32_1ms;


//////////////////////////////////////////////////////////////////////////////////////
typedef struct
{
  u16  u16_app_ver;
  u16  u16_temperature;
}SYS_Type;
extern SYS_Type SYS;

extern __flash st_CommVarListTypedef u16_CommVar[];

void init_sys_var(void);

#endif