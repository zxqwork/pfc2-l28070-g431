/*
 * Interrupt200Hz.h
 *
 *  Created on: 2021年8月3日
 *      Author: chenge-bjb06
 */

#ifndef INTERRUPT200HZ_H_
#define INTERRUPT200HZ_H_

/****************************函数声明***************************/
void StateM(void);
void VariableInit(void);
void ErrCheck(void);

#endif /* INTERRUPT200HZ_H_ */
