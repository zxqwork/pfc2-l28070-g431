#ifndef _LIB_BASE_H_
#define _LIB_BASE_H_

#include "type.h"
#include "std_def.h"

#define __flash const

#define _CLI    __disable_irq
#define _SEI    __enable_irq

#define _DISABLE_IRQ     __disable_irq
#define _ENABLE_IRQ      __enable_irq

#define _NOP             asm("nop")  
#define _WDR  IWDG->KR = 0xAAAA

#define _DEVICE_ID0  (*(u32*)(0x1FFFF7E8))//оƬID0
#define _DEVICE_ID1  (*(u32*)(0x1FFFF7EC))//оƬID1
#define _DEVICE_ID2  (*(u32*)(0x1FFFF7F0))//оƬID2

#endif