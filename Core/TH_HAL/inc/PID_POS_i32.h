#ifndef _PID_I32_POSITION_
#define _PID_I32_POSITION_

#include "base.h"

#define PID_ST_enDifferential   0 //微分使能
#define PID_ST_DIR 1
#

typedef struct
{
  long P;//比例
  long I;//积分
  long D;//微分
  long Integral; //积分量
  long InteMax;  //积分上下限
  long InteMin;
  
  u8  u8_shift;//移位
  u8  u8_st; //状态
  
  u16 u16_Output;
  u16 u16_OutMax; //输出上下限
  u16 u16_OutMin;
  
  u8  u8_RunTimes;
  u8  u8_Dtimes;    //微分环节在设定值发生变化的时候，不起作用
  long s32_old_set;  
}PID_TypeDef;

void PID_POS_I32(PID_TypeDef *pid,long u16_set,long u16_now);
void PID_Init(PID_TypeDef *pid);
#endif