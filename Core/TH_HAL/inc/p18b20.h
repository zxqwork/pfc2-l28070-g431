#ifndef H_18b20_H
#define H_18b20_H

#define CRC_ERROR_18B20         1
#define CANNOT_READ_18B20       2

#define TH18B20_READ_REST4Start             0
#define TH18B20_RESTART_CONVERT             1
#define TH18B20_WAIT_FOR_READ               2
#define TH18B20_READ_REST4Read              3
#define TH18B20_READ_TEMP                   4
#define TH18B20_WAIT_FOR_NEXT_CONVERT       5

#define TH18B20_REST4WRITE_ADJ              6 
#define TH18B20_WRITE_ADJ                   7
#define TH18B20_REST4SAVE                   8
#define TH18B20_WRITE_SAVE                  9
typedef struct
{
    u8  u8_us;
    u8  u8_step;
    u8  u8_val;
}Type18b20Step;

#define C18B20_SET_is01  0
#define C18B20_WriteADJ  1
typedef struct
{
    u8  XB;
    u8  mu8_No_18b20;
	u8  u8_ReadInInt;
    u8  u8_18b20_not_read_count;
    u8  u8_ERROR_MAX;       //错误报警上限
    
    u8 uc_read_temp_reg_lo_byte;
    u8 uc_read_temp_reg_hi_byte;
    u8 u8_crc;
    u8 u8_18b20_crc_error_count;
    u8 u8_TH,u8_TL,u8_adj;
    s16 s16_Original001;
    s16 s16_adj; //读出的修正值 从18b20 TH==TL这个无符号数当成有符号数，再转换为有符号16位
	u8  u8_50usCnt;
    u8  u8_set;    
    s16 s16_Temperature;
    u8  u8_18b20_error;
    u8  u8_Count;
    Type18b20Step Big,Sml,Tiny;
}Type18b20;

void init_18b20(Type18b20 *S); 
void about_18b20(Type18b20 *S); 
void Calibration18b20atZero(Type18b20 *S);
void Write18b20TempAdj(Type18b20 *S,s16 s16_adj);
#endif