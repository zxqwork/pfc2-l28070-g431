#ifndef _PID_INC_H_
#define _PID_INC_H_

#define PID_ST_enDifferential   0 //微分使能

typedef struct
{
  u16 u16_P;//比例
  u16 u16_I;//积分
  u16 u16_D;//微分
  u16 u16_T;//0.1S的周期 新增
  u8  u8_dir;//方向
  u8  u8_st; //状态
  
  s32 s32_old_e;
  
  u16 u16_OutMax;
  u16 u16_OutMin;
  
  //u16 u16_OutMax; 是否可以改为上升SPEED  
  u16 u16_CtrlCyc100ms;
  u16 u16_Output;
  u8  u8_RunTimes;
  
  u8  u8_Dtimes;    //微分环节在设定值发生变化的时候，不起作用
  s32 s32_old_set;
  u16 u16_UsedNow; //缓慢变化实际值，避免突变
  s32 s32_I_mod;
  
  u8 u8_SoftNoPtimes;
  u8 u8_SoftSetupNoP;
  u16 u16_PoutLim;
  u16 u16_errLim;
  
}PID_TypeDef;

void PID_INC(PID_TypeDef *pid,u16 u16_set,u16 u16_now);

#endif