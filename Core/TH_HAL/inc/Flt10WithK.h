#ifndef _Flt10WithK_h_
#define _Flt10WithK_h_

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
// 物理量结构，36字节
////////////////////////////////////////////////////////////////////////////////////
typedef struct	
{
  u16  u16_val;           //当前物理量, 电流A,电压mv,功率W,压力0.01MPa
  u16  u16_avg;          //物理量队列的均值
  u16  u16_ValArray[19];  //物理量队列
  u8   u8_T_msArray[19];  //获得时的周期队列
  u8   u8_old_T_ms;       //上次的时刻，用于计算时差	
  u16  u16_max;
  s16  s16_NowK;         //斜率,压力1kPa/s,(每秒0.001MPa,s16范围:+-3.768MPa/s),功率每秒0.1W/s 范围是 +-3.2768kw/s
  
}PhysicsParaType;

void Calc_GetPhyArrayAvg(PhysicsParaType *Phy,u16 u16_val,u8 u8_t1ms);

//u16_amp 为倍率，压机用10，cvd气压用100
void Calc_GetCtrlPhyK(PhysicsParaType *Phy,u16 u16_amp);
#endif