#ifndef H_18b20_H
#define H_18b20_H

#define TEMP_ERROR_18B20        850
#define CRC_ERROR_18B20         1
#define CANNOT_READ_18B20       2

void init_18b20(s16 *s16_Temperature_X_10,u16 *u16_18b20_error,u8 ErrorMax);

void about_18b20(void);    
#endif