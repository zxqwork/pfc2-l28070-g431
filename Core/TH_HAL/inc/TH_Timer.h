#ifndef _TH_TIMER_H_
#define _TH_TIMER_H_
#include "base.h"
///////////////////////////////////////////////////////////
//时间总是需要的
//realtime structure definition
typedef struct
{
  volatile u32 u32_count_1ms;
  
  volatile u32 u32_msg_1ms;
  volatile u32 u32_msg_10ms;
  volatile u32 u32_msg_100ms;
  volatile u32 u32_msg_1s;
  
  
  u8 u8_year;
  u8 u8_month;
  u8 u8_day;
  u8 u8_hour;
  u8 u8_minter;
  u8 u8_second;
  u8 u8_read_sec_count;
  
}st_Timer_TypeDef;

extern st_Timer_TypeDef st_sys_timer;

/////////////////////////////////////////////////////////////////////
//
//内部定时器1ms计时函数，请在中断中调用
//
void DRV_Timer1ms(void);
/////////////////////////////////////////////////////////////////////


#endif



