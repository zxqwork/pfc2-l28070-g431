#ifndef _PID_H_
#define _PID_H_

#define PID_ST_enDifferential   0 //微分使能

typedef struct
{
  u16 u16_P;//比例
  u16 u16_I;//积分
  u16 u16_D;//微分
  u16 u16_T;//0.1S的周期 新增
  u8  u8_dir;//方向
  u8  u8_st; //状态
  float f_Scale;
  
  float f_old_e;
  float f_oold_e;  
  
  u16 u16_OutMax;
  u16 u16_OutMin;
  
  //u16 u16_OutMax; 是否可以改为上升SPEED  
  u16 u16_CtrlCyc100ms;
  u16 u16_Output;
  u8  u8_RunTimes;
  
  u8  u8_Dtimes;    //微分环节在设定值发生变化的时候，不起作用
  s32 s32_old_set;
  
  u8 u8_SoftNoPtimes;
  u8 u8_SoftSetupNoP;
  u16 u16_PoutLim;
}fPID_TypeDef;

void PID_F(fPID_TypeDef *pid,s32 s32_set,s32 s32_now);

//清除比例和积分作用，适合于半中间开始
void PID_F_ClearPD(fPID_TypeDef *pid,s32 s32_set,s32 s32_now);
#endif