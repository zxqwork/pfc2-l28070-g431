//////////////////////////////////////////////////////////////////////////
//**std_def.h   这儿定义了通用的宏和数据类型的宏定义。
#ifndef _sta_def_h
#define _sta_def_h

#define  ENABLE_BIT_DEFINITIONS

#define   HEX_TO_DEC(uc_hex)              ((uc_hex) - ((uc_hex) >= 65?55:48))
#define   DEC_TO_HEX(uc_dec)              ((uc_dec)+ ((uc_dec) > 9?55:48))

#ifndef BIT
#define BIT(x)	(1 << (x))
#endif

#ifndef BIT32
#define BIT32(x)	((u32)1 << (x))
#endif


#ifndef BIT64
#define BIT64(x)	((u64)1 << (x))
#endif

#define uchar unsigned char
#define uint unsigned  int

/////////////////////////////////
union u_u32
{
  unsigned long ul;

  struct
  {
	unsigned int lo;
	unsigned int hi;
  }ui;

  struct
  {
    unsigned char ll;
    unsigned char lh;
	unsigned char hl;
    unsigned char hh;
  }uc;
};
/////////////////////////////////

/////////////////////////////////
union u_u16
{
  unsigned short uH; //int
  struct
  {
    unsigned char lo;
    unsigned char hi;
  }uO;
};
/////////////////////////////////
union u_u8
{
 unsigned char u8;
 struct
 {
  unsigned char bit0:1;
  unsigned char bit1:1;
  unsigned char bit2:1;
  unsigned char bit3:1;
  unsigned char bit4:1;
  unsigned char bit5:1;
  unsigned char bit6:1;
  unsigned char bit7:1;
 };
};
/////////////////////////////////

#define  isST16(y,x)    (y & (u16)BIT(x))
#define  isNotST16(y,x) ((y & (u16)BIT(x)) == 0)
#define  setST16(y,x)   (y |=  (u16)BIT(x))
#define  clsST16(y,x)   (y &=~ (u16)BIT(x))
#define  notST16(y,x)   (y ^= (u16)BIT(x))
#define  movST16(x,y,z) ((z) != 0 ? (x |=  (u16)BIT(y)) : (x &=~ (u16)BIT(y)))



#define isOutB16(y,x)  (y &   BIT(x))
#define SetOutB16(y,x) (y |=  BIT(x))
#define ClsOutB16(y,x) (y &=~ BIT(x))
#define NotOutB16(y,x) (y ^=  BIT(x))


//**数据类型定义

#ifndef   uchar
#define   uchar          u8
#endif

#ifndef   UCHAR
typedef   u8  UCHAR; //          u8
#endif

#ifndef   uint
#define   uint           u16
#endif

#ifndef   UINT
#define   UINT           u16
#endif

//********************** macro function define
#define   LO_UINT(ui_digit)               (u8)((ui_digit)& 0x00ff)        //得到一个16位数字的低8位
#define   HI_UINT(ui_digit)               (u8)((ui_digit) >> 8)           //得到一个16位数字的高8位
#define   LO_UCHAR(uc_digit)              (u8)((uc_digit)&0x0f)           //得到一个8位数的低4位
#define   HI_UCHAR(uc_digit)              (u8)((uc_digit)>>4)             //得到一个8位数的高4位
#define   MAKE_UINT(uchHi,uchLo)          (((u16)(uchHi) << 8) | (uchLo))		 //将两个8位合成一个16位数
#define   MAKE_U32(ucHH,ucHL,ucLH,ucLL)   (((u32)ucHH << 24) + ((u32)ucHL << 16) + ((u32)ucLH << 8) + ucLL)

#define   DEC_TO_HEX(uc_dec)              ((uc_dec)+ ((uc_dec) > 9?55:48))    //将十进制转为16进制
#define   LO_UCHAR_TO_HEX(uc_digit)       ( DEC_TO_HEX(LO_UCHAR((uc_digit)))) //得到一个8位数的低四位,并转换为16进制的字符
#define   HI_UCHAR_TO_HEX(uc_digit)       ( DEC_TO_HEX(HI_UCHAR((uc_digit)))) //得到一个8位数的高四位,并转换为16进制的字符

#define   HEX_TO_DEC(uc_hex)              ((uc_hex) - ((uc_hex) >= 65?55:48)) //16进制字符转为10进制数
#define   MAKE_HEX_TO_DEC(hi_hex,lo_hex)  (u8)(((HEX_TO_DEC((hi_hex)))<<4)|(HEX_TO_DEC((lo_hex)))) //将两个十六进制字符合成为8位数

#define   ABS(x) ((x) < 0?(-(x)):(x))
#define   Unsinged_ABS(x,y)   ((x > y)?(x - y):(y - x))

#define K   1024 

#endif    // end (#ifndef _sta_def_h )
