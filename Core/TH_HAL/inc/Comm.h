#ifndef _COMM_H_
#define _COMM_H_
#include "app_conf.h" //20220727 内部全部是define，有可能包含CPU CLK信息等，必须在base前
#include "base.h"
#include "TH_Timer.h"
#include "stm32g4xx_hal.h" //包含HAL库上因为要用 DMA_HandleTypeDef类

typedef enum
{
    en_Comm_protocol_Modbus	= 0,
    en_Comm_protocol_YaoHua_running = 1,
    en_Comm_protocol_YaoHua_ask_ans = 2,
    en_Comm_protocol_ADAM			  = 3,
    en_Comm_protocol_PPI			  = 4,
    en_Comm_protocol_USS			  = 5,
    en_Comm_protocol_FREE_4CMD  	  = 6,
    en_Comm_protocol_0203CRLF   	  = 7,  //MFC：FCON W: 02 地址x3,命令x4, :+
    en_Comm_protocol_YUDIAN_AiBus	  = 15,
    en_Comm_protocol_YUDIAN_6		  = 16, 
    en_Comm_protocol_YUDIAN_7		  = 17,
    en_Comm_protocol_YUDIAN_EXT_LED   = 18,//EXTLED
    en_Comm_protocol_YUDIAN_9		  = 19, 
    en_Comm_protocol_WtznAT           = 20, 
}enum_Comm_Protocol;

typedef enum
{ 	
    en_HEAD_MB_ASCII = 0x3a,     //**modbus asc模式的帧头  3a
}enum_Protocol_Head;

typedef enum
{
    en_PRE_END_MB_ASCII = 0x0d,
    en_END_MB_ASCII = 0x0a,
}enum_Protocol_End;

typedef enum
{
 	en_Comm_Status_is_sending			= 0,
 	en_Comm_Status_is_need_led_tip      = 1,
 	en_Comm_Status_is_need_send			= 2,
 	en_Comm_Status_is_RecFinished       = 3, //用于DMA判断，接受串处理完毕，可以重启DMA了，在DMA里清除
    en_Comm_Status_is_tmpTCP = 4, //检测发现是tcp
}enum_Comm_Status;

typedef enum
{
	en_Comm_MASTER_SEND_EN	= 0,
	en_Comm_MASTER_REC_EN	= 1,
	en_Comm_SLAVER_SEND_EN	= 2,
	en_Comm_SLAVER_REC_EN	= 3,
    en_Comm_MONITOR_REC_EN  = 4,
    en_Comm_TH_CRC  = 5, //为了通讯的校验加密，将标准的校验码做特殊处理，具体处理看CRC函数
    en_Comm_TH_SECRET = 6,  //报文是否加密，加密方式与最后两个校验码有关，具体看相关函数 
    en_Comm_DisableSpeedUp  = 7
}enum_Comm_MASTER_SLAVER_SEND_REC_EN;
// en_Comm_TH_CRC,en_Comm_TH_SECRET, 通讯口和op都用到这个设置：
 //注意事项，关于加密对于从设备，初始化的是 st_comm[0].uc_master_slaver_status，因为对于从设备只负责接收和相应，所以只需要知道st_comm[0].uc_master_slaver_status
 //对于主设备  初始化st_comm[1].st_Master_OP[0].uc_status = BIT(en_Comm_TH_CRC) | BIT(en_Comm_TH_SECRET)，每个OP表的加密类型都不一样 
 //使用加密时，只需在原来的基础上加上加密位en_Comm_TH_CRC,en_Comm_TH_SECRET或者即可


//注意是字节 为了明确opStatus的枚举
typedef enum
{
 	en_CommOP_MASTER_SEND_EN	= 0,
    en_CommOP_MasterGetRec = 4, //做主的时候，op收到了正确的接收串，并处理了  
    en_CommOP_TH_CRC  = 5,      //兼容保留，此位不可占用
    en_CommOP_TH_SECRET = 6,   //兼容保留，此位不可占用
}enum_CommOP_Status;

typedef enum
{
    en_bt_4800 = 0,
    en_bt_9600,
    en_bt_19200,
    en_bt_38400,
    en_bt_57600,
    en_bt_115200,
    en_bt_187500
}enum_Comm_Bt;

///////////////////////////////////////////////////////////////////
//
//	通讯相关
//
///////////////////////////////////////////////////////////////////
typedef enum
{
    en_Parity_N  = 0,
    en_Parity_E, //偶校验
    en_Parity_O  //奇校验
}enum_parity_def;


typedef enum
{
    en_485_receive	= 0,
    en_485_send		= 0xff
}enum_Comm_Send_En;


typedef struct
{
    u8 u8_StopBit;//当前的串口停止位，如果不用的op不一样，则要修改
    u8 u8_Parity; //当前的串口校验位，如果不用的op不一样，则要修改
	u8  uc_slaver_client;
	u8  uc_cmd;  //功能码
	u16 u16_lenth;      //长度
	u16 ui_plc_begin_addr; //plc的内存地址
	u16 ui_mem_begin_addr;  //本地内存开始地址
	//当从PLC读时,ui_plc_begin_addr表示读PLC的开始地址，uc_mem_begin_addr为读来后存放在表的地址
	//当写如PLC时,ui_plc_begin_addr表示写入的PLC内存的开始地址，uc_mem_begin_addr为写出数据的表内存地址
	//uc_lenth为操作的字个数或位个数，注意不是下标

	
	//下面仅仅用于0x17命令时,用于写到plc去
	u16 ui_write_plc_addr;  //写到plc的内存开始地址
	u16 ui_write_mem_addr;  //写的数据存放的内存地址
	u16 u16_write_len;       //为操作的字个数
	u16  uc_status;     //HAL库不再顾及51了
	//当写时,表从内存地址获取要写的数据.

	u8 u8_MasterFreq;  //主通讯频率 % (u8_MasterFreq)则呼叫，循环序列1-10

	u8 uc_Master_RecErrCnt;	//当主的时候，呼叫别人，接受响应丢失或错误的计数器
	u8 u8_op_protocol;
}Master_OP_TypeDef;

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//      通讯表
///////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	u16 *ui_p;
}CommU16PointOnlyType;


typedef struct
{
    u16  *ui_p;
    u8   uc_data_type;
	u16  ui_eep_addr;
}st_CommVar_Table_Typedef;

typedef struct
{
    u16  *ui_p;
    u8 uc_data_type;
	unsigned int  ui_eep_addr;
	unsigned int  ui_min;
	unsigned int  ui_max;
	unsigned int  ui_default;

}st_CommVar_bigTable_Typedef;

typedef struct
{
	u16 *ui_p;
}st_CommVarListTypedef;


typedef struct
{
    u8 uc_client;
    u8 uc_port_num;
    enum_Comm_Bt en_bt;
    u8 u8_StopBit;
    u8 u8_Parity;
    enum_Comm_Protocol en_protocol;
    u8 uc_ThCRC;

    u16 COMM_ARRAY_Rec_XB;
    u16 COMM_ARRAY_SendXB;

    u8 *u8_RecBufP;
    u8 *u8_SendBufP;

    u16 u16_rec_p;
    u16 u16_send_p;
    u16 u16_sendarray_num;

    u8 uc_status;
    u8 uc_is_Sync;   //同步
    u8 uc_is_asc;
    u8 uc_2stop_bit;
    u8 uc_master_slaver_status;    //不动程序了，因为用的地方太多了-->改回u8，为了51兼容


    u8 uc_rec_timeout;
    u8 uc_timeout_4bytes;
    u8 uc_last_judge_rec_1ms;

    u8 uc_comm_led_1ms_count;

    u8 uc_485_send_location;

    u16 u16_master_send_interval;
    u16 u16_master_lastSend_time;

    Master_OP_TypeDef *st_Master_OP;
    u8 uc_Master_OP_P;
    u8 uc_Master_OP_Lenth;
    u8 u8_MasterFreqCNT;


    u8 u8_DMA_rec_1ms;
    u16 u16_DMA_last_rec_p;//取代u16_DMA_last_Remain_bytes
    u16 u16_DMA_rec_p_Remain0Cnt;
    u16 u16_DMA_Timeout_1ms_count;
    
    DMA_HandleTypeDef *pDMArx;  //必须在初始化的时候，赋值
    DMA_HandleTypeDef *pDMAtx;
    UART_HandleTypeDef *pUart;

}st_Comm_TypeDef;


#ifdef IS_NEED_COMM

extern st_Comm_TypeDef st_comm[MCU_UART_NUM];

extern u8 u8_SendBuf0[COMM0_ARRAY_SendXB + 1];
extern u8 u8_SendBuf1[COMM1_ARRAY_SendXB + 1];
extern u8 u8_SendBuf2[COMM2_ARRAY_SendXB + 1];
extern u8 u8_SendBuf3[COMM3_ARRAY_SendXB + 1];
extern u8 u8_SendBuf4[COMM4_ARRAY_SendXB + 1];

extern u8 u8_RecBuf0[COMM0_ARRAY_Rec_XB + 1];
extern u8 u8_RecBuf1[COMM1_ARRAY_Rec_XB + 1];
extern u8 u8_RecBuf2[COMM2_ARRAY_Rec_XB + 1];
extern u8 u8_RecBuf3[COMM3_ARRAY_Rec_XB + 1];
extern u8 u8_RecBuf4[COMM4_ARRAY_Rec_XB + 1];

extern Master_OP_TypeDef st_Master0_OP[OP_TABLE_LENTH_0];
extern Master_OP_TypeDef st_Master1_OP[OP_TABLE_LENTH_1];
extern Master_OP_TypeDef st_Master2_OP[OP_TABLE_LENTH_2];
extern Master_OP_TypeDef st_Master3_OP[OP_TABLE_LENTH_3];
extern Master_OP_TypeDef st_Master4_OP[OP_TABLE_LENTH_4];

extern __flash u16  code_bt[];
extern __flash u16 uc_timeout_4byte_ms[];



///////////////////////////////////////////////////////////////
///
///////////////////////////////////////////////////////////////
//下面结构中的uc_data_type的宏
#define DATA_TYPE_IS_UCHAR	           0    //实际上，这个和系统枚举的变量类型是一样的，保留是因为
#define DATA_TYPE_IS_UINT	           1    //有的程序用到了

//#define DATA_TYPE_IS_ARRAY_UCHAR       2    //如果是队列的话，min表达队列的下标，这个主要用来初始化读
//#define DATA_TYPE_IS_ARRAY_UINT        3    //通讯也很方便，主要解决大数组问题，函数中另外处理，不用此表




////////////////////////////////////////////////////////////////////
// 通讯处理函数
// 传入通讯数组结构指针
//
////////////////////////////////////////////////////////////////////
void about_comm(st_Comm_TypeDef *p,u16 u16_Now_ms);


////////////////////////////////////////////////////////////////////
// 中断接收函数
// 传入通讯数组结构指针
// 判断头，并标志字节间隔
////////////////////////////////////////////////////////////////////

void uart_rx(st_Comm_TypeDef *st_comm_p,u8 uc_now_UDR);

////////////////////////////////////////////////////////////////////
// 中断发送函数
// 传入通讯数组结构指针
//
//character has been transmitted，最后=u16_sendarray_num+1
//大于u16_sendarray_num后，所以不会无限制地加
////////////////////////////////////////////////////////////////////
u8 uart_tx_not_over(st_Comm_TypeDef *st_comm_p,u8 *uc_be_send);
#endif

#endif
