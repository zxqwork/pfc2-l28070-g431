#ifndef _DRV_IO_H_
#define _DRV_IO_H_
#include "base.h"
#include "app_conf.h"
///////////////////////////////////////////////////////////
typedef enum
{
    en_PORTA =0,
    en_PORTB =1,
    en_PORTC =2,
    en_PORTD =3,
    en_PORTE =4,
    en_PORTF =5,
    en_PORTG =6
}enum_PORT;

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
#ifdef APP_USE_GPIO

typedef struct
{
    u8 u8_input[COUNT_INPUT_BYTE];
    u8 u8_filter_input[COUNT_INPUT_BYTE];
    u8 u8_filter_level;

    u8 u8_out[COUNT_OUTPUT_BYTE];

    u8 u8_input_flt_count[COUNT_INPUT_BYTE][8];
    u8 u8_old_input[COUNT_INPUT_BYTE];
}st_GPIO_TypeDef;

extern st_GPIO_TypeDef st_GPIO;

#endif

/////////////////////////////////////////////////////////////////////////////////
void DRV_IO_OUTPUT(u8 pl_out);
/////////////////////////////////////////////////////////////////////////////////
//因为是对应这个硬件做的函数，是按这个硬件定义的，不需要做转向
u8 DRV_IO_INPUT(void);
#endif

