#ifndef _Pluse_Sensor_h
#define _Pluse_Sensor_h

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//脉冲流量结构
#define PulseFlowSensor_NewData  0
#define PulseFlowSensor_Measuring    1
#define PulseFlowSensor_CanCalc 2
typedef struct
{
    u16 u16_Begin_t;    //记录了开始脉冲沿的时刻
    u16 u16_Event_t;    //最后一个沿的时刻
    u16 u16_dt;         //为了计算而记录的时间间隔
    u16 u16_EventCnt;    //Door_t期间的沿个数
    u16 u16_dCnt;       //为了计算而记录的事件数
    u16 u16_Door_t;     //门限时间
    u16 u16_Flow;       //实际流量
    u32 u32_Rate;//每秒，N个脉冲 N / 1S * 60S / 1080 = L/min 60s->60000ms / 1080= 55.55 放大100倍 5555
    
    u8  u8_RecLevel;
    u8  Flag;//最低位是记录状态
}PulseFlowSensorType;

// 初始化系数，
//输入：每单位多少事件  输出:60秒的物理量参数，放大100倍
//事件可以是沿，也可以是脉冲
//例：每升1080个脉冲，换成L/Min, 60s->60000ms     / 1080= 55.55 放大100倍 5555，采用周期2秒 (&PF,1080,6000000, 2000)
//例：每升1080个脉冲，换成L/Min, 60s->6000000百us / 1080= 555.5 放大100倍 55555,采用周期2秒 (&PF,1080,60000000,20000)

void initEvent_PerMin(PulseFlowSensorType *pf,u16 u16_PlusePreL,u32 minT,u16 u16_DoorT);

/////////////////////////////////////////////////////////////////////////////
//   流量
//   输入：当前时刻，当前电平状态
/////////////////////////////////////////////////////////////////////////////
void FlowSensor(PulseFlowSensorType *pf,u16 u16_t,u8 u8_Now);

//主程序循环中，根据标志，  计算流量  ，为了减少中断时间占用
void FlowSensor_Calc(PulseFlowSensorType *pf);
#endif