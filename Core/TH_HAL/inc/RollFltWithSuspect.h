#ifndef _ROLLFLTWITHSUSPECT_H_
#define _ROLLFLTWITHSUSPECT_H_

typedef struct 
{
  u16 IntialStage;
  u16 BelieveCnt;
  u32 Arry[32];
  u16 p,pTail;
  u16 u16_SuspectDoor;
  u32 u32_Avg;
  
  u32 SuspectBit;
}stRollFltWithSuspect;

u16 is_RollFlt_Believable(stRollFltWithSuspect *R,u32 u32_now);
#endif