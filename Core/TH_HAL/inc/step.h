#ifndef _STEP_H_
#define _STEP_H_

typedef struct{
    u8 u8_step;
    u16 step100mS;//内部step计时
}InnerStepType;

typedef struct
{
	u16  u8_Now;    //当前阶段，加热用的大多是加热阶段。没用内部step
	u16  u16_NowTime10mS,u16_NowTime100mS;
    InnerStepType inn;//主要用于内部阶段计时
}StepType;

void SetInnStep(StepType *me,u16 u16_StepName);
void SetPart(StepType* me,u8 u8_PartName);

void SetpTime10mS(StepType* me);
void SetpTime100mS(StepType* me);

#endif