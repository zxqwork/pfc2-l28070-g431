#ifndef _COMM_YAOHUA_EXTLED_H_
#define _COMM_YAOHUA_EXTLED_H_

#define Comm_Protocol_YaoHua_Address_EXTLED_DOTandPOL 58887
#define Comm_Protocol_YaoHua_Address_EXTLED_WIGHT_HI 58888
#define Comm_Protocol_YaoHua_Address_EXTLED_WIGHT_LO 58889

extern u8  data mu8_LED_Lenth;
extern u32 data ku32_number;
extern u8  data Dot,NegOrPos;

unsigned char YaoHua_ExtLed_reqframe_anlys_slaver(st_Comm_TypeDef *p);
void getLedBuf(u32 x, u8 PointLen, u8 isNegative, u8 *LedBuf, u8 u8LedLen);

#endif
