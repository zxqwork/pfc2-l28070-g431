#ifndef _PL_H_
#define _PL_H_

#define PL_OUTPUT_SELECT2   0
#define PL_OUTPUT_RUN       1

#define PL_OUTPUT_FAST_1	0
#define PL_OUTPUT_SLOW_1	1
#define PL_OUTPUT_FAST_2	2
#define PL_OUTPUT_SLOW_2	3
#define PL_OUTPUT_DRAIN		4
#define PL_OUTPUT_ELIGIBILITY	5
#define PL_OUTPUT_DISQUALIFICATION	6

#define PL_INP_RUN			0
#define PL_INP_STOP			1
#define PL_INP_PAUSE		2
#define PL_INP_ADD_KEY1		3
#define PL_INP_ADD_KEY2		4
#define PL_INP_DRAIN_KEY	5
#define PL_INP_ADD_ALE		6
#define PL_INP_DRAIN_ALE	7

#define PL_TYPE_NONE        0
#define PL_TYPE_INC         1
#define PL_TYPE_DEC         2
#define PL_TYPE_DEBUG       255

/////////////////////////////////////////////////////////////称重控制数据
typedef enum
{
	en_pl_auto		= 0,
	en_pl_running	= 1,
	en_pl_puase		= 2,
	
	en_pl_no_hopper	= 3,
	en_pl_over_error= 4,
	en_pl_over_time = 5,
    en_pl_StepByStep= 6,
	
}enum_control_status;

typedef enum
{
	en_PL_Step_init		= 0,	
	en_PL_Step_1_TARE	= 1,		
	en_PL_Step_1_T0		= 2,
	en_PL_Step_1_FAST	= 3,
	en_PL_Step_1_T1		= 4,
	en_PL_Step_1_SLOW	= 5,
	en_PL_Step_1_T2		= 6,
	en_PL_Step_1_JOD	= 7,

	en_PL_Step_2_TARE	= 8,		
	en_PL_Step_2_T0		= 9,
	en_PL_Step_2_FAST	= 10,
	en_PL_Step_2_T1		= 11,
	en_PL_Step_2_SLOW	= 12,
	en_PL_Step_2_T2		= 13,
	en_PL_Step_2_JOD	= 14,

	en_PL_STEP_DRAIN	= 15,
	en_PL_STEP_WAITE_NEXT_LOOP	= 17,
	
}enum_601_pl_step_status;

typedef struct
{
	u8  u8_comm_cmd;
			
	u8  u8_status;
	u8  u8_type;		// 0 加减法秤选择（0：加法秤，1：减法秤）
	u8  u8_step;
	u8  u8_auto_calc_tql;//提前量自动修正选择（0：不修正，1：修正）
	u8  u8_over_error_do;//C－超差处理选择（0－不处理，循环继续；1－等待处理至合格）
	u8  u8_lack_jod;	//欠料点补（0－不点补；1－欠料点补）
	u16 u16_loop_times; //设置一次完整加料到放料过程的次数（0～999，0 为无限次）
	u8  u8_floating_hopper;//浮动料斗
	u16 u16_hopper_min;
	u16 u16_hopper_max;
		
	u8  u8_t0;			//0.0～9.9 秒加料测量延时 避免因启动时的重量冲击造成重量误判
	u8  u8_t1;			//0.0～9.9 秒快加结束延时
	u8  u8_t2;			//0.0～9.9 秒慢加结束延时
	u16 u16_t3;			//0.0～9.9 秒点补输出时间
	u16 u16_t4;			//0.0～9.9 秒点补间歇时间
	u8  u8_t5;			//0.0～9.9 秒合格输出时间
	u8  u8_t6;			//0.0～9.9 秒放料结束延时
	u8  u8_t7;			//0.0～9.9 秒再加料延时
	u8  u8_t8;			//输出时间。如果和间歇时间都为0，则满幅度输出
	u8  u8_t9;			//间歇时间
	u16 u16_zero_range;	//零区。1、仪表放料时判断毛重小于零区即认为放料完成；2、仪表 累计时毛重需要大于零区才可以进行。

	u32 u32_weight_aim1;	//料1 定量
	u16 u16_fast_pre_1;		//料1 快加提前量
	u16 u16_slow_pre_1;		//料1 慢加提前量
	u16 u16_ale_error1;		//料1 允差量

	u32 u32_weight_aim2;	//料1 定量
	u16 u16_fast_pre_2;		//料1 快加提前量
	u16 u16_slow_pre_2;		//料1 慢加提前量
	u16 u16_ale_error2;		//料1 允差量
	
	u32 u32_real_weight_1;
	u32 u32_real_weight_2;

	u32 u32_total_weight_1;
	u32 u32_total_weight_2;
	u32 u32_total_weight;

	u16 u16_pl_count;		//配料计数器

	s32 s32_tare1;			//皮重的临时储存
	s32 s32_tare2;
	

	u8  u8_pl_output_type;	//bit0=0:快加时，仅有快加输出。bit0=1:快加时，慢加同时输出
							//bit1=0:da不作为模拟输出
							
	
	u8  u8_time_10ms_for_count;
	u16 u16_step_time10ms;
	u8  u8_time_100ms_for_count;
	u16 u16_step_time100ms;
	u16 u16_over_time1s;		//加料超时，认为料仓没料了
	u16 u16_temp_time100ms;

	u8  u8_pl_output;
	u8  u8_pl_input;
	
}st_pl_ctrl_Typedef;

//配料控制函数，有重量数据则判断，不是以时间为单位的，但应该做超时判断
void pl_ctrl(st_pl_ctrl_Typedef *p,st_Weight_Typedef *w,st_Timer_TypeDef *t);

#endif
