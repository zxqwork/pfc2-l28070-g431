#ifndef _COMM_TABLE_H_
#define _COMM_TABLE_H_
#include "base.h"
#include "sys_var.h"


void ui_CommTableRetArray(u16 ui_start_address1,u16 ui_start_address2,u8 u8_port_num, u16 len, u8 *u8p);

u16 ui_RTU_ret_Word(u16 ui_start_address,unsigned char u8_port_num);
void Rtu_write_singleReg(u16 ui_start_address,u16 ui_value,unsigned char u8_port_num);
u8 uc_RTU_get_bit(u16 ui_start_address, u8 u8_port_num);
void uc_RTU_write_bit(u16 ui_bit_address,u8 uc_h, u8 u8_port_num);

void ui_RTU_ret_Block(u16 ui_start_address,u16 *u16_DataP,u16 *u16_ByteCNT,u16 u16_count,u16 u16_Port);
void Rtu_write_BlockReg(u16 ui_start_address,u16 *dataP,u16 u16_count,u16 u16_Port);

//写本地内存串完成事件， 开始地址，长度，端口
void afterWriteRegisters(u16 ui_start_address, u16 u16_count,u8 u8_port_num);

///////////////////////////////////////////////////////////////////////////////
//
//    读取内存整串前的事件
//    读取前，准备数据，在各个协议中，ui_RTU_ret_Word之前调用
//    
///////////////////////////////////////////////////////////////////////////////
void beforeReadRegisters(u16 ui_start_address, u16 u16_count,u8 u8_port_num);


///////////////////////////////////////////////////////////////////////////////
//
//    读取内存整串完成事件
//    做从的时候，被上位机读走后，可以刷新被读走的分时数据
//    做主的时候，写下位机串完后，可以刷新写出去的分时数据
///////////////////////////////////////////////////////////////////////////////
void afterReadRegisters(u16 ui_start_address, u16 u16_count,u8 u8_port_num);


#endif
