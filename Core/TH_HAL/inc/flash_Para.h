#ifndef _FLASH_PARA_H_
#define _FLASH_PARA_H_
#include "base.h"
#include "comm.h"

#define AddBKP 30000  //允许30页，每页3000个字，30000以上是电池
#define Bat AddBKP    //简写



/* STM32F10X_LD\ STM32F10X_MD 1K  ,STM32F10X_HD  2K */
#if defined (STM32F10X_LD) || defined (STM32F10X_MD)
  #define FLASH_PARA_PAGE_SIZE  (uint16_t)0x400  /* Page size = 1KByte */
#elif defined (STM32F10X_HD) || defined (STM32F10X_CL)
  #define FLASH_PARA_PAGE_SIZE  (uint16_t)0x800  /* Page size = 2KByte */
#endif

#if defined (GD32F30X_LD) || defined (GD32F30X_MD)
  #define FLASH_PARA_PAGE_SIZE  (uint16_t)0x400  /* Page size = 1KByte */
#elif defined (GD32F30X_HD) || defined (GD32F30X_CL)
  #define FLASH_PARA_PAGE_SIZE  (uint16_t)0x800  /* Page size = 2KByte */
#endif

extern u16 eepNeedSaveMsg,batNeedSaveMsg;

//设哪一页需要存储标志
void SetNeedSave(u16 u16_Add);
void init_read_from_CommVar_Table(st_CommVar_bigTable_Typedef const *v,u16 u16_start_xb, u16 u16_end_xb,u32 u32FlashParaBeginAdd);
//////////////////////////////////////////////////////////////////////////////////////////////
void RecoveryDefaultA_B(st_CommVar_bigTable_Typedef const *v,u16 u16_BeginAdd,u16 u16_EndAdd);

void WritePara2BKP(st_CommVar_bigTable_Typedef const *v, u16 u16_xb_start,u16 u16_xb_end);
void WritePara2Rom(st_CommVar_bigTable_Typedef const *v,u32 u32_FlashBeginAdd, u16 u16_xb);

///////////////////////////////////////////////////////
//         IAP
//IAP
typedef  void (*pFunction)(void);

extern u16 IAP_u16_TotalK;
extern u16 IAP_APP_is_A; //测自己定位在哪
u32 u32IAP_getAPP_Address();

// 字指针和长度 currentK是以k为单位的值，从0起
void IAP_Write2Rom(u16 *dataP,u16 u16_count,u16 currentK);
///////////////////////////////////////////////
void IAP_init(u16 u16_HowManyKs);//多少k
#endif