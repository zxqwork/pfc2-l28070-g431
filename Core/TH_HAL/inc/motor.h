#ifndef _MOTOR_H_
#define _MOTOR_H_

#include "std_def.h"

typedef struct
{
    u32 u32_SetInc;     //命令增量
    s16 s16_SetV;		//目标速度
    u16 u16_Cmd;        //命令
	u16 u16_Acc_a;		//加速度
    u16 u16_Dec_a;		//减速度，不必负数    
	u16 u16_Stamp;
	u16 u16_LastS;
}MotorCmdType;

typedef struct
{  
    //读去    
    u16 u16_ver;        //版本
    
    u16 u16_LastCmd;    //原命令
    s32 s32_NowScale;   //当前刻度
    s16 s16_NowCycle;   //周数
    s16 s16_Now_V_Scale;//当前刻度速度 每秒走多少刻度
    //当前step速度 每秒走多少步，例如快则每秒5转，细分64，则5*64*200 = 64000，7.8us半个脉冲 
    //慢则65.535ms一个脉冲，如果细分64，则838秒转一周，如果细分1则13秒转一周
    s32 s32_Now_V_Step;	//以step计数的速度 
    s32 s32_NowSteps;   //当前步数 
	
	//代表现在的电平输出。初始状态是0，判断走到了后，不再上升。
	//第一次进入定时中断的时间间隔run函数里固定设置，有助于电流稳定建立
	//进入中断后先发出上升沿，在下降沿里判断是否不再上升。
	u16 u16_isHi;	//代表电平状态		
    u16 u16_ST;//状态
    volatile u16 u16_LimStop;
    u16 nowXB;  //做成结构是为了以后做成队列
    u16 setXB;	//有任务的设定下标
    MotorCmdType Task[8];    
    
    
    //设置    
    u16 XB;				//设备地址，和自己下标相同    
    u16 u16_Setings;    //设定
    u16 u16_EachScale_Steps; //例如丝杠导程5mm，如果不细分，1STEP->0.025mm，0.1mm->4STEP   
    u16 u16_ScaleLenth;			//全长，刻度
    u32 u32_EachCycleScale;		//一周长度 刻度
    u16 u16_AccCurrentDirP;		//电流
    u16 u16_RunCurrentDirP;		
    u16 u16_AccCurrentDirM;
    u16 u16_RunCurrentDirM;
    u16 u16_Lock1Current;		//短期（u16_StopLock1ms内）锁定电流
	u16 u16_Lock2Current;		//长期（u16_StopLock1ms后）锁定电流，=0时，释放EN
    
	u16 u16_StopLock1ms;		//设置的锁定时间长度
	u16 u16_StartV0Scale;       //启动速度
	u16 u16_EndVScale;          //停止速度
	s16 s16_Upper_Scale10;	//上限刻度
	s16 s16_Lower_Scale10;  //下限刻度
	
    u16 u16_CutOffMoreSteps;    //关闭多走几步 目前版本无用
	
    //内部变量
	u16 CMD;					//当前执行的命令
	u32 u32_TargetStep;			//内部目标计数
    u32 u32_MotorAllSteps;		//全程，如果非零，则走到停
	s32 s32_EachCycleSteps;		//每周多少步
	s32 s32_V0_Step;            //初速度
	s32 s32_Acc_Step;			//以step单位的加速，即每秒加速多少步，可正负
	s32 s32_Dec_Step;			//以step单位的减速，即每秒减速多少步，仅仅正
	s32 s32_Set_V_Step;			//以step单位的目标速度
	u32 u32_End_V_Step;			//以step单位的停止速度
	u16 u16_StopRec1ms;			//记录的锁定时间
	u16 u16_LockCurrent;		//锁定电流寄存器
	
	s32 s32_Upper_Steps;	//上限刻度
	s32 s32_Lower_Steps;  //下限刻度
    
    u32 u32_StepInt;			//每步周期，以0.25us为单位
    u32 u32_HalfInt;			//半周期
    u32 u32_Acc025us;			//加速计时器
	
	u32 u32_DecelerateStep;		//减速步数
	s16 s16_Need_AccCalc;		//需要计算加速或者减速
    //volatile 
}st_StepMotorDef;
///////////////////////////////////////////////////////////////////////////
//cmd ，速度，位移量，加速度，减速度。
//运行命令：StepMotor_RuningStandy 
//			位移=定时ms
//			速度=0 则按锁定电流2
//			加速度=0，则锁定电流1；否则按运行电流，分正负
//			减速度，决定DIR引脚+-方向

//运行命令：StepMotor_Run，StepMotor_2Zero_Rise，StepMotor_2Zero_Drop
//			位移=0时，无尽模式
//			减速度=0时，不减速等待下一个任务
//
///////////////////////////////////////////////////////////////////////////
//极限定时500s----位移是定时ms
//速度=0 则按锁定电流2
//加速度=0，则锁定电流1；否则按运行电流
//减速的+-，自动由决定方向
#define StepMotor_None				0   //上电流待机
#define StepMotor_Run				1   //运行-释放
#define StepMotor_2Zero_Rise		2   //运行到碰到0点，注意位移量！=0
#define StepMotor_2Zero_Drop		3   //运行到脱离0点，注意位移量！=0
#define StepMotor_CutOff			4   //关闭
#define StepMotor_Release         5   //停-释放
#define StepMotor_Lock            6   //停-锁定
#define StepMotor_SetZero         7   //位置置零
#define StepMotor_SetSclae         8   //置位
#define StepMotor_ClsTasks        9
#define StepMotor_RuningStandy	  65535   //上电流待机

#define StepMotor_Standy		100 //上电流待机 	
#define StepMotorERelease       101 //停-释放
#define StepMotor_E_Stop        102	//紧急停车，没有减速
#define StepMotor_D_Stop        103	//减速停车，按给定的减速度
#define StepMotor_D_Jump		104	//减速停车，走后面的动作，跳步7
#define StepMotor_Modify	    105	//修改当前任务
#define StepMotor_DelMoreTask   106 //删除后继任务
#define StepMotor_InstanceSetSclae      107 //置位

//set
#define StepMotor_Set_ModeCycle     0
#define StepMotor_Set_Polar         1
#define StepMotor_Set_DRZ			2 //DetectRiseZero->SetSTEP=0
#define StepMotor_Set_DDZ			3 //DetectDropZero->SetSTEP=0
#define StepMotor_Set_ForceOff      7      //关闭撞紧
#define StepMotor_Set_NotAleNegative	8	//走到0点必须停
#define StepMotor_Set_NotAleUpperLim	9
#define StepMotor_Set_NotAleLowerLim   10	//如果设置了上下限，那么禁止

#define MOTOR_ST_RUNING 0
#define MOTOR_ST_ACCING 1
#define MOTOR_ST_DECING 2
#define MOTOR_ST_ZERO   3	//碰到0点是高
#define MOTOR_ST_LEVEL_ZERO 4   //零点的电平状态
#define MOTOR_ST_DIR_OFF 5
#define MOTOR_ST_Alert   6  //过热保护
#define MOTOR_ST_TaskFull 7
#define MOTOR_ST_NotAleLim	8	//极限停车！这个状态是为了减少中断的计算


typedef struct
{
	u16 XB;    //设备地址    
    u16 u16_Setings;    //设定
    u16 u16_EachScale_Steps; //例如丝杠导程5mm，如果不细分，1STEP->0.025mm，0.1mm->4STEP   
    u16 u16_ScaleLenth;      //全长，刻度
    u32 u32_EachCycleScale;  //一周长度 刻度
    u16 u16_AccCurrentDirP;
    u16 u16_RunCurrentDirP;
    u16 u16_AccCurrentDirM;
    u16 u16_RunCurrentDirM;
    u16 u16_Lock1Current;
	u16 u16_Lock2Current;
    u16 u16_CutOffMoreSteps;    //关闭多走几步
	u16 u16_StopLock1ms;		//设置的锁定时间长度
	u16 u16_StartV0Scale;       //启动速度
	u16 u16_EndVScale;          //停止速度
	
	s16 s16_Upper_Scale10;	//上限刻度
	s16 s16_Lower_Scale10;  //下限刻度
} st_StepMotorSet;

/////////////////////////////////////////////////////////////////////////////////////////////
void about_StepMotor(st_StepMotorDef *p);
/////////////////////////////////////////////////////////////////////////////////////////////
void StepMotor_NewTask(st_StepMotorDef *p,u16 Cmd,s16 SetV,u32 SetL,u16 a,u16 SetD);
void StepMotor_Init(st_StepMotorDef *p,st_StepMotorSet *s);
void StepMotor_Execute(st_StepMotorDef *p);
//定时器要配置成0.25us
void StepMotorInt(st_StepMotorDef *p);
#endif