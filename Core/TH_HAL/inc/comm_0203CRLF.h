#ifndef _COMM_FREE_0203CRLF_H_
#define _COMM_FREE_0203CRLF_H_
#include "base.h"
#include "Comm.h"

#include "comm_table.h"
#include "CRC_LRC.h"

//** modbus ascii 模式的宏定义
//ui_plc_begin_addr 作为接收串的 H:开始 L:结束
//ui_write_plc_addr 作为发送串的 H:开始 L:结束
//ui_write_mem_addr 
//u16_lenth     作为 HH HL 命令字节
//u16_write_len 作为 LH LL 命令字节

#define CommCMD0203CRLF_RD 0  //读取
#define CommCMD0203CRLF_WD 1  
#define CommCMD0203CRLF_RB 2  //读取
#define CommCMD0203CRLF_WB 3  //写入流量设定值命令 
//

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//         自由协议 校验
//         可以对RTU和ASCII 方式进行校验
//Entry:
//         结构指针
//return:
//     校验成功返回TURE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u8 u8_Comm0203CRLF_is_check_is_right(st_Comm_TypeDef *p);

u16 Comm0203CRLF_master_send(st_Comm_TypeDef *p);

void Comm0203CRLF_reqframe_anlys_master(st_Comm_TypeDef *p);


#endif
