#ifndef _MODBUS_H_
#define _MODBUS_H_
#include "base.h"
#include "Comm.h"

#include "comm_table.h"
#include "CRC_LRC.h"
#include "Convert_ASC_RTU.h"

//** modbus ascii 模式的宏定义
#define MB_HEAD_ASCII        0x3A     //**modbus asc模式的帧头  3a
#define MB_PRE_END_ASCII     0x0D   //**modbus asc模式的帧的前一个尾
#define MB_END_ASCII         0x0A   //**modbus asc模式的尾

//** modbus 功能码的定义
#define MB_READ_COIL         0x01   //read coil
#define MB_READ_DISCRETE     0x02   //read discrete
#define MB_READ_HOLD_REG     0x03   //read hold register
#define MB_READ_INPUT_REG    0x04   //read input register
#define MB_WRITE_SINGLE_COIL 0x05   //write single coil
#define MB_WRITE_SINGLE_REG  0x06   //write single register
#define MB_WRITE_MULTI_COIL  0x0F   //Write multi  coil
#define MB_WRITE_MULTI_REG   0x10   //Write multi  register
#define MB_READ_WRITE_REG    0x17   //read and write register
#define MB_READ_DEVICE_INFO  0x2B   //read device information
#define MB_MEI_TYPE          0x0E   //use for reading device information

#define MB_WRITE_BLOCK_REG   0x20   //写寄存器块 自定义
#define MB_READ_BLOCK_REG    0x24   //read input register
#define MB_R_Reg_W_Block_REG 0x27  //read write

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//         ModBus 校验
//         可以对RTU和ASCII 方式进行校验
//Entry:
//         结构指针
//return:
//     校验成功返回TURE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u8 u8_modbus_check_is_right(st_Comm_TypeDef *p);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//        modbus协议，本机做从，此函数分析主机发过来的命令串，并组成返回串。
//        函数开始时都转换成RTU，到函数末尾再根据参数is_modbus_asc转换成相应的ASCII或RTU
//Entry:
//      unsigned char *puc_rec_buf: 需要分析的串的指针，ASCII模式不包含头和尾巴（即不包含0x3A和0x0D 0x0A）。
//      unsigned char uc_rec_len：传过来的需要分析串的长度，是个数，不是下标。
//     unsigned char *puc_send_buf：组成发送串的指针，是将来返回给发送串的全局变量的
//     unsigned char is_modbus_asc：表征ASCII或RTU模式的变量，0----RTU，1----ASCII
//return:
//     无返回
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rtu_reqframe_anlys_slaver(st_Comm_TypeDef *p);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//        modbus协议，本机做主，此函数根据OP表（结构数组），生成发送串。
//        函数开始时按照RTU处理，到函数末尾再根据参数is_modbus_asc转换成相应的ASCII或RTU
//Entry:
//	    st_Comm_TypeDef *p  : 通讯变量结构指针
//局部变量
//      struct Master_OP *pMaster_OP: 结构指针，传入的是OP表（结构数组）
//     unsigned char puc_send_buf：组成发送串的指针，是将来返回给发送串的全局变量的
//return:
//     ucp_send_count   返回的是发送串的长度，是下标，不是个数，返回0为不发送
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u16 rtu_modbus_master_send(st_Comm_TypeDef *p);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//        modbus协议，本机做主，此函数用来分析（当本机主发了命令串后）从设备的返回串
//        函数开始时都转换成RTU进行解析
//Entry:
//	    st_Comm_TypeDef *p  : 通讯变量结构指针
//局部变量
//      unsigned char puc_rec_buf:  需要分析的串的指针，ASCII模式不包含头和尾巴（即不包含0x3A和0x0D 0x0A）。见调用前的工作
//      unsigned char uc_rec_len：传过来的需要分析串的长度，是个数，不是下标。
//return:
//     无返回
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rtu_reqframe_anlys_master(st_Comm_TypeDef *p);

//解密
void unSECRET(u8 *puc_rec_buf,u16 u16_Begin,u16 u16_rec_len);

#endif
