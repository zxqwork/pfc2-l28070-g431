#ifndef _FREE_CMD4_H_
#define _FREE_CMD4_H_
#include "base.h"
#include "Comm.h"

#include "comm_table.h"
#include "CRC_LRC.h"

//** modbus ascii 模式的宏定义
//ui_plc_begin_addr 作为接收串的 H:开始 L:结束
//ui_write_plc_addr 作为发送串的 H:开始 L:结束
//ui_write_mem_addr 
//u16_lenth     作为 HH HL 命令字节
//u16_write_len 作为 LH LL 命令字节

#define FreeCommCmd_RCFR 0  //读取流量瞬时值命令
#define FreeCommCmd_WSFD 1     //写入流量设定值命令 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//         自由协议 校验
//         可以对RTU和ASCII 方式进行校验
//Entry:
//         结构指针
//return:
//     校验成功返回TURE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u8 u8_FreeComm4_is_check_is_right(st_Comm_TypeDef *p);

u16 FreeComm4_master_send(st_Comm_TypeDef *p);

void FreeComm4_reqframe_anlys_master(st_Comm_TypeDef *p);


#endif
