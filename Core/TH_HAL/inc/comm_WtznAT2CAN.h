#ifndef _COMM_WTZN_AT2CAN_H_
#define _COMM_WTZN_AT2CAN_H_
#include "base.h"
#include "Comm.h"

#include "comm_table.h"

//** modbus ascii 模式的宏定义
//ui_plc_begin_addr 作为接收串的 H:开始 L:结束
//ui_write_plc_addr 作为发送串的 H:开始 L:结束
//ui_write_mem_addr 
//u16_lenth     作为 HH HL 命令字节
//u16_write_len 作为 LH LL 命令字节
#define CommWtznAT_EXP8Data 0
#define CommWtznAT_CG 1
#define CommWtznAT_SET_CAN_FRAMEFORMAT 2
#define CommWtznAT_SET_CAN_BAUD 3
#define CommWtznAT_AT 4

u8 u8_CommWtznAT2CAN_is_check_is_right(st_Comm_TypeDef *p);

u16 CommWtznAT2CAN_master_send(st_Comm_TypeDef *p);

void CommWtznAT2CAN_reqframe_anlys_master(u8 u8isRight , st_Comm_TypeDef *p);


#endif