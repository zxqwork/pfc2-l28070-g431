#ifndef _SYS_VAR_H_
#define _SYS_VAR_H_

#include "base.h"

u16 sys_ABS16(s16 x);
u32 sys_ABS32(s32 x);

extern unsigned long gu32_debug;
extern unsigned int  gu16_debug;
extern unsigned char gu8_debug,gu8_debug1,gu8_debug2;

extern unsigned long gu32_Flag;
#define SysFlag_EepromChange 0
#define SysFlag_Green_Is_Need 1
#define SysFlag_Alarm_Is_Need 2
#define SysFlag_Red_Is_Need 3
#define SysFlag_Step_motor_Is_Need 4

#define isSysFlag(x) (gu32_Flag & BIT32(x))
#define  setSysFlag(x)   (gu32_Flag |=  BIT32(x))
#define  clsSysFlag(x)   (gu32_Flag &=~ BIT32(x))
#define  notSysFlag(x)   (gu32_Flag ^= BIT32(x))
#define  movSysFlag(y,z) ((z) != 0 ? (gu32_Flag |=  BIT32(y)) : (gu32_Flag &=~ BIT32(y)))



/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
//	DA structure definition
//
//	
//
////////////////////////////////////////////////////////////
typedef struct
{
  // 0 毛重，1 净重，2 总重
  u8  u8_src;

  u8  u8_typ;

  u16 u16_zero_ma;
  u16 u16_20mA_ma;
  u16 u16_da_ma;

  u8  u8_scan_count_10ms;

  u16 u16_min_ma,u16_max_ma;

}st_DA_Typedef;

////////////////////////////////////////////////////////////
//	ADC status definition
//
//	
//
////////////////////////////////////////////////////////////
typedef enum
{
  en_ADC_Status_is_have_new			= 0,
  en_ADC_Status_is_have_err			= 7
}enum_ADC_Status;

////////////////////////////////////////////////////////////
//	
//
//	
//
////////////////////////////////////////////////////////////

#ifdef APP_USE_FILTER_32
typedef enum
{
  en_Filter_Status_is_have_new			= 0,
}enum_Filter_BD;

typedef struct
{
  u8  u8_sensor_type;     //传感器类型
  u8  u8_pro_filter_lv;	//预滤波级别
  u8  u8_pro_filter_count;
  s32 s32_pro_total;
  s32 s32_pro_max;
  s32 s32_pro_min;

  u8  u8_filter_lv;	//滤波级别，0=无
  u8  u8_status;
  s32 s32_total;
  s32 s32_filter_result;
  u16 u16_door_val;
  u8  u8_over_door_times;
  u8  u8_over_door_count;
  u8  u8_over_door_dat_use;	//越门的数据，是否参与滤波，1是参与，0是丢弃

}st_Filter_Typedef;
#endif

typedef enum
{
  en_E_1		= 0,
  en_E_2		= 1,
  en_E_5		= 2,
  en_E_10		= 3,
  en_E_20		= 4,
  en_E_50		= 5,
}enum_E;

typedef enum
{
  en_Weight_Status_Zero		= 0,
  en_Weight_Status_Steady		= 1,
  en_Weight_Status_tare		= 2,
  en_Weight_Status_is_have_new= 3,
  en_Weight_Status_new_ctrl	= 4,


}enum_Weight_Status;

typedef struct
{
  u8  u8_dc;
  u8  en_e;	
  s32 s32_each_e;
  s32 s32_e_amp;
  s32 s32_zero_ma;		//标定零点
  s32 s32_full_ma;		//标定满幅，未存储
  s32 s32_empty_ma;		//置零码值，未存储

  u8  u8_weight_door_e;	//重量的智能滤波门限，单位是E

  s32 s32_net_weight;		//净重
  s32 s32_gross_weight;	//毛重
  s32 s32_tare_weight;	//皮重
  s32 s32_hi_lim_weight;	//上限重量

  u8 u8_pow_on_zero_range;//F .S% 0 2 4 10 20 100
  u8 u8_manual_zero_range;//F .S% 0 2 4 10 20 100
  u8 u8_tail_zero_range;	//（e） 0.5 1 1.5 2 2.5 3 3.5 4
  u8 u8_tail_zero_time100ms;//零点的采样
  u8 u8_tail_zero_timecount;

  u8 u8_steady_judge_ens;		////AD 稳定判据，波动小于几个E，开始判断稳定
  u8 u8_steady_judge_tms;		//AD 稳定判据，波动小于几个E，保存多少100ms
  u8 u8_steady_timecount;		//有多少个数连续的，波动小于等于1个分度，则认为稳定
  u8 u8_exit_steady_count;


  s32 s32_old_es;
  s32 s32_now_es;
  s32 s32_total_es_after_steady; //601表中已经不再使用 2012.2.10
  s16 s16_now_es;

  u8 u8_pro_filter_lv;

  u8 u8_status;
  u8 u8_get_weight_count;

  u16 u16_da_ma[2];   //这两个纯粹是为了在标定函数中直接操纵DA,方便DA调试而用

}st_Weight_Typedef;


typedef enum
{
  en_PARA_FLAG_COMM_CHG	      = 0,
  en_PARA_FLAG_ADC_CHG		  = 1,
  en_PARA_FLAG_DA_BD_CHANGE     = 2,
  en_PARA_FLAG_LOAD_DEFAULT     = 3,
  en_PARA_FLAG_PASSWORD		  = 4,
  en_PARA_FLAG_TIME             = 5,
  en_PARA_FLAG_NeedLoadFefault  = 6,
}
enum_Para_Set_Flag;

typedef enum
{
  en_VAR_TYPE_U8  = 0,
  en_VAR_TYPE_U08 = 0,
  en_VAR_TYPE_U16 = 1,
  en_VAR_TYPE_U32 = 2,
  en_VAR_TYPE_ENUM =3,
  en_VAR_TYPE_S8  = 4,
  en_VAR_TYPE_S16 = 5,
  en_VAR_TYPE_S32 = 6,
  en_VAR_TYPE_ARRAY_U8 = 16,
  en_VAR_TYPE_ARRAY_U16 = 17,
  en_VAR_TYPE_LoadDefault =31,
  
  en_Readonly_U8  = 128 + 0,
  en_Readonly_U08 = 128 + 0,
  en_Readonly_U16 = 128 + 1,
  en_Readonly_U32 = 128 + 2,
  en_Readonly_ENUM =128 + 3,
  en_Readonly_S8  = 128 + 4,
  en_Readonly_S16 = 128 + 5,
  en_Readonly_S32 = 128 + 6,
  en_Readonly_ARRAY_U8 = 128 + 16,
  en_Readonly_ARRAY_U16 = 128 + 17,

}enum_VAR_TYPE;


typedef struct
{
  //union //联合表示几个变量公用一个内存位置, 在不同的时间保存不同的数据类型 和不同长度的变量。
  //{     //若要访问结构变量y[1]中联合x的成员i, 可以写成: y[1].x.i;
  //  //若要访问结构变量y[2]中联合x的字符串指针ch的第一个字符可写成: *y[2].x.ch;
  //  unsigned char *uc_p;
  //  unsigned int  *ui_p;	
  //  unsigned long *ul_p;
  //};
  unsigned int  *ui_p;
  unsigned char uc_min;//最小
  unsigned int  ui_max;//最大
  unsigned char uc_dp;//小数点个数
  unsigned char uc_type;//参数值的数据类型：字，字节...
  unsigned char uc_save_level;//参数可见的级别设置
  unsigned int  ui_eeprom_add;//参数存的EEPROM地址
  unsigned int  ui_default;//默认值
  unsigned char __flash *uc_tip_p;//提示字符串，LED用，三位或四位，不同的应用不同
  unsigned int  __flash *ui_menu_p;//枚举表，比如4800，9600
  unsigned char uc_user_flag; //该参数的标志，比如，通讯参数，或者标定参数，或者恢复默认值等
  unsigned char uc_max_pos;
  unsigned int ui_set_step;//参数项
}st_Para_Var_TypeDef;

extern __flash unsigned int ALL_VAR_XB_BE_SET;


///////////////////////////////////////////////////////////////////
//  参数设置结构
//
///////////////////////////////////////////////////////////////////
//
//st_Para_Set_Typedef.u8_status 的枚举
typedef enum
{

  en_PARA_STATUS_IS_SETTING       = 0,
  en_PARA_STATUS_IS_SET_0         = 1,
  en_PARA_STATUS_IS_SCALE			= 2,
  en_PARA_STATUS_IS_SHELL_SET		= 3,
  en_PARA_STATUS_NEED_DISP_CLEAR  = 4,
  en_PARA_STATUS_CHG_PARA_VAL     = 5,
  en_PARA_STATUS_IS_ENABLE_CHANGE = 6,

}enum_Para_Set_Status;

#define SET_POS_SET_FLASH		0x0d
#define SET_POS_ALL_FLASH		0x0e    //u8_set_pos被设定项全闪，用于枚举
#define SET_POS_NONE_FLASH		0x0f    //u8_set_pos设定项目间切换则不闪

//-->参数表变量结构，注意，用到参数设定和存储的话，要在APP_VAR中声明
//此变量，并使用EEPROM库，才能使用本库

typedef enum
{
  en_PARA_SET_IS_ENUM			= 0,
  en_PARA_STRING_IS_TIP		= 1,
  en_PARA_STRING_IS_ALL		= 2,

}enum_Para_Set_String;

typedef struct
{
  u8  u8_status;
  u8  u8_flag;
  u16 u16_set_step;
  u16 u16_show_step;
  u16 u16_init_step;
  u8  u8_sys_lock;
  u8  u8_set_pos;		//设置的位
  u8  u8_max_pos;
  u8  u8_dp;
  u8  u8_now_save_level;	//当前安全级别，要存入EEPROM的
  u32 u32_be_set;
  u16 u16_max;
  u8  u8_min;
  u8  u8_type;

  //这几个是LED设定用的
  u8  u8_is_string;			//显示字符串的位置
  u16 __flash *u16_menu_p;
  u8  __flash *u8_tip_p;		//提示字符串

  u8  u8_shell_menu_xb;		//SET 0,1,2... 有多少，是下标

  u8  u8_last_key_press_1s;

  u8 u8_sys_null;

}st_Para_Set_Typedef;   //控制参数设定函数的行为

///////////////////////////////////////////////////////////////////
//和键盘有关的规格化
//对于参数设定函数来说，控制数据类型st_Para_Set_Typedef的动作
//
///////////////////////////////////////////////////////////////////
typedef enum
{
  en_Standard_Key_SET			= 0x0100,
  en_Standard_Key_ENTER		= 0x0200,	
  en_Standard_Key_SAVE		= 0x0300,
  en_Standard_Key_ESC 		= 0x0400,
  en_Standard_Key_RISE		= 0x0500,
  en_Standard_Key_DOWN		= 0x0600,
  en_Standard_Key_LEFT		= 0x0700,
  en_Standard_Key_RIGHT		= 0x0800,

  en_Standard_Key_INIT        = 0x0900,
  en_Standard_Key_QUICK_RISE	= 0x0a00,
  en_Standard_Key_QUICK_DOWN	= 0x0b00,

  en_Standard_Key_Run         = 0x0c00,
  en_Standard_Key_Stop        = 0x0d00,
  en_Standard_Key_Pause       = 0x0e00,


  en_Standard_Key_SHIFT		= 0x2000,
  en_Standard_Key_Coder_INC	= 0x4000,
  en_Standard_Key_Coder_DEC	= 0x8000,
  //注意，有两个是旋转编码器，占用2个位，最最高位
  //所以，系统标准键值可以有0x1f 个
  //低字节是应该按键，由应该系统定义
}enum_Standard_Key;

///////////////////////////////////////////////////////////////////
//键盘扫描的控制结构
//
//
///////////////////////////////////////////////////////////////////
typedef enum
{
  en_ScanKeyMsg_is_have_key = 0,
  en_ScanKeyMsg_is_push_key = 1,
}enum_Scan_Key_Msg;

typedef struct
{
  u8  u8_msg;
  u16 u16_push_bc_time;
  u8  u8_every_add_time;
  u8  u8_add_key_count;
  u8  u8_scan_time1ms;
  u8  u8_io_ma;
  u16 u16_key_code;
  u8  u8_delay_time;

  u8  u8_old_coder_state;
  u8  u8_coder_right_is_forward;

}st_Scan_Key_Typedef;



/*
以二进制码串行输出，
波特率为 600。每一桢数据有 11 个位，1 个起始位（0）、8 个数 据位（低位在前）、1 个标志位、1 个停止位（1）。
2、 每隔 100ms 发送一组数据，每组数据包括 3 桢数据，其意义如下：
第一桢数据：标志位为 0；
X：d0、d1、d2 为小数点位置（0－3）；
Y：d3－为重量符号（1－负；0－正）；
d4－为毛/净重（1－净重；0－毛重）；
G17、G16：二进制数据； 第二桢数据：标志位为 0；
G15 ～ G8：二进制数据； 第三桢数据：标志位为 1；
G7  ～ G0： 二进制数据；
G0  ～ G17：由低到高构成重量的 18 位二进制码。
*/
typedef struct
{
  u8 u8_use_YHL;   //大屏幕，0是不用，1-8是输出的口位，采用上海耀华的YHL显示屏
  u8 u8_100ms_count;
  u8 u8_bit_count;
  u32 u32_be_show;
  u8  u8_dp;
  u8  u8_pol;

}st_YHL_SCREEN_Typedef;
#endif
