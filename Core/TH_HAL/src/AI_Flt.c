
#define MCU_TYPE_AVR   
#define IAR_Compiler

#include "lib_includes.h"

#include "AI_Flt.h"

unsigned long Ai_GET_New_Total(s32 s32_total,u8 u8_filter_lv,s32  s32_mid_now)
{
 return(((s32_total * (s32)((1 << u8_filter_lv) - 1)) / (s32)(1 << u8_filter_lv)) + s32_mid_now);
 //f->s32_total  = ((f->s32_total * (s32)((1 << f->u8_filter_lv) - 1)) >> f->u8_filter_lv) + s32_mid_now;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AI_filter(st_Filter_Typedef *f,s32 s32_now)
{
    s32 s32_mid_now;
    unsigned char u8_is_pro_filter_over = 0;
    
	if(f->u8_pro_filter_lv)
	{
		f->s32_pro_total += s32_now;
		if(s32_now > f->s32_pro_max){f->s32_pro_max = s32_now;} 
		if(s32_now < f->s32_pro_min){f->s32_pro_min = s32_now;} 
		
		if(++(f->u8_pro_filter_count) >= ((1 << f->u8_pro_filter_lv) + 2))
		{
			s32_mid_now = (f->s32_pro_total - f->s32_pro_max - f->s32_pro_min) /(s32)(1 << f->u8_pro_filter_lv);
			f->s32_pro_total = 0;
			f->s32_pro_max	= 0x80000000;
			f->s32_pro_min	= 0x7fffffff;
			f->u8_pro_filter_count = 0;
            
			u8_is_pro_filter_over = TRUE;
		}
	}
	else	//不需要预滤波
	{
		s32_mid_now = s32_now;
		u8_is_pro_filter_over = TRUE;
	}
	
	//预滤波完成，下面开始队列权值滤波
	if(u8_is_pro_filter_over)
    { 
        if(f->u8_over_door_times)		//启用智能门限滤波功能么
        {
            if(ABS(s32_mid_now - f->s32_filter_result) > (s32)f->u16_door_val)
            {
				//要不全部重新填写队列(累计),要不看要不要越界数据滤波，如果不要，则该数据抛弃
				if(++f->u8_over_door_count > f->u8_over_door_times)
                {
                    f->s32_total = s32_mid_now * (s32)(1 << f->u8_filter_lv);
                    f->u8_over_door_count = 0;
                }
				else if(f->u8_over_door_dat_use)
                {
                    f->s32_total = Ai_GET_New_Total(f->s32_total,f->u8_filter_lv,s32_mid_now);
                }
        	}
			else
            {
				f->u8_over_door_count = 0;
				f->s32_total = Ai_GET_New_Total(f->s32_total,f->u8_filter_lv,s32_mid_now);
            }
        }	
		else
        {
			f->s32_total = Ai_GET_New_Total(f->s32_total,f->u8_filter_lv,s32_mid_now);
        }
        
        
        f->s32_filter_result = f->s32_total / (s32)(1 << f->u8_filter_lv);
        
        f->u8_status |= BIT(en_Filter_Status_is_have_new);
        
        
    }
    
}

