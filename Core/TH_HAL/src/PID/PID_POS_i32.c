#include "PID_POS_i32.h"

void PID_POS_I32(PID_TypeDef *pid,long i32_set,long i32_now)
{
    long err, i32;
    err = i32_now - i32_set;   //计算电压误差量，实际电压高了，输出量增加，慢速环
    if(pid->u8_st & BIT(PID_ST_DIR)){err = -err;}
    
    pid->Integral +=  (err * pid->I) ;    //积分量=积分量+KI*误差量
    //
    if     (pid->Integral < pid->InteMin){pid->Integral = pid->InteMin;}//积分量限制，积分量最小限制
    else if(pid->Integral > pid->InteMax){pid->Integral = pid->InteMax;}//积分量限制，积分量最大值限制，10A数字量对应Q15
    
    //输出=积分量 + KP*误差量
    i32 = (pid->Integral + err *  pid->P) >> pid->u8_shift;
    if     (i32 < pid->u16_OutMin){i32 = pid->u16_OutMin;}  //输出最小限制  
    else if(i32 > pid->u16_OutMax){i32 = pid->u16_OutMax;}  
    //其输出叠加在电流环的电流参考值上面，通过降低电流环参考值，降低输出电流的方式从而限制输出电压      
    pid->u16_Output = i32; 

}
void PID_Init(PID_TypeDef *pid)
{
    pid->Integral = 0;
    pid->u16_Output = pid->u16_OutMin;
}