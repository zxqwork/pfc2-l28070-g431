#include "Base.h"
#include "PluseSensor.h"
//改2处，1. if(pf->u16_EventCnt == 0)内部禁止计算  2.找到第一个脉冲，赋值了Event_t
/////////////////////////////////////////////////////////////////////////////
//   流量
//   输入：当前时刻，当前电平状态
/////////////////////////////////////////////////////////////////////////////
void FlowSensor(PulseFlowSensorType *pf,u16 u16_t,u8 u8_NowLevel)
{
    
    if(pf->Flag & BIT(PulseFlowSensor_Measuring))//测量中
    {
        if(pf->u8_RecLevel != u8_NowLevel)
        {
            pf->u8_RecLevel = u8_NowLevel;
            pf->u16_Event_t = u16_t;
            pf->u16_EventCnt++;            
        }
        
        if((u16)(u16_t - pf->u16_Begin_t) >= pf->u16_Door_t)
        {
            if(pf->u16_EventCnt == 0)
            {
                pf->u16_Flow = 0;
                pf->Flag &=~ (BIT(PulseFlowSensor_Measuring) | BIT(PulseFlowSensor_CanCalc));
            }
            else
            {
                //每秒，N个脉冲 N / 1S * 60S / 1080 = L/min 60s->60000ms / 1080= 55.55 放大100倍 5555
                pf->u16_dt = (u16)(pf->u16_Event_t - pf->u16_Begin_t);
                pf->u16_dCnt = pf->u16_EventCnt;
                pf->u16_EventCnt = 0;
                //pf->u16_Flow = (u32)pf->u16_EventCnt * (u32)pf->u16_Rate / (u32)u16_tmp; //减少中断中停留的时间
                pf->Flag |= BIT(PulseFlowSensor_CanCalc);
                pf->u16_Begin_t = pf->u16_Event_t;//首脉冲的起点就是最后脉冲的时刻
            }            
        }
    }
    else //找第一个脉冲
    {
        if(pf->u8_RecLevel != u8_NowLevel)
        {
             pf->u8_RecLevel = u8_NowLevel;
             pf->u16_Begin_t = pf->u16_Event_t = u16_t;
             pf->u16_EventCnt = 0;
             pf->Flag |= BIT(PulseFlowSensor_Measuring);
        }
    }
}

// 初始化系数，
//输入：每单位多少事件  输出:60秒的物理量参数，放大100倍
//事件可以是沿，也可以是脉冲
//例：每升1080个脉冲，换成L/Min, 60s->60000ms     / 1080= 55.55 放大100倍 5555，采用周期2秒 (&PF,1080,6000000, 2000)
//例：每升1080个脉冲，换成L/Min, 60s->6000000百us / 1080= 555.5 放大100倍 55555,采用周期2秒 (&PF,1080,60000000,20000)

void initEvent_PerMin(PulseFlowSensorType *pf,u16 u16_PlusePreL,u32 minT,u16 u16_DoorT)
{
    pf->u32_Rate = (u32)minT / (u32)u16_PlusePreL;
    pf->u16_Door_t = u16_DoorT;
}

//主程序循环中，根据标志，  计算流量  ，为了减少中断时间占用
void FlowSensor_Calc(PulseFlowSensorType *pf)
{
    pf->u16_Flow = ((u32)pf->u16_dCnt * (u32)pf->u32_Rate) / (u32)pf->u16_dt; 
    pf->Flag &=~BIT(PulseFlowSensor_CanCalc);
    pf->Flag ^= BIT(PulseFlowSensor_NewData);
}
