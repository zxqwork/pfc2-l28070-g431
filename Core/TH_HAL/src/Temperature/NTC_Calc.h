#ifndef _NTC_CALC_
#define _NTC_CALC_

#include "base.h"

//r25:常温对应的阻值，单位可以是千欧，比如10=10k 
//Tt和r25单位保持一致
//adj001C温度微调量，0.01度
float NTC3950_Calc(float Rt, u16 r25, s16 adj001C);

//输出8位表达0.5精度的温度  (输入16位精度0.1的整数)
//  因为基本都是正值，为读取方便，从0-220表达0-110度
//  221-254 表达-0.5 -> -17度 
//  255 为非法数据，开路或者短路
u8 getU8ZipTemp05c(s16 s16t01);

//获得16位的温度，精度0.5 
//0-220 ->  0.0 :  110.0
//221-255  -0.5 :  -17.5
s16 getU16Temp05cFromZip(u8 u8t05);
#endif