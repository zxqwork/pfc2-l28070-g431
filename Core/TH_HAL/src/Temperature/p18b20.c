/*
有别于18b20.c, 本函数全部采用指针调用，可以同时接256个18b20,异步操作
依赖50us定时器
发现了51一个大坑，读TH TL的顺序由于进位会有错误！所以改为了8位变量
*/
#include "base.h"
#include "p18b20.h"
#include "bsp.h"

#define SKIP_ROM        0XCC
#define START_CONVERT   0X44
#define WRITE_18B20REG  0X4E 
#define SAVE18B20       0X48 //掉电存储 
#define READ_TEMPRATURE 0XBE

/*BSP中必须实现
void POWER_ON(void);
void SET_PORT_18B20_OUT(u8 XB);
void SET_PORT_18B20_IN(u8 XB);
void CLS_PIN_18B20(u8 XB);
void SET_PIN_18B20(u8 XB);
u8 INP_PIN_18B20(u8 XB);
#define TCNT_us               TIM3->CNT

SET_PORT_18B20_POWEROFF(u8 XB);
SET_PORT_18B20_POWERON(u8 XB);
*/

u8 mpu16_10ms; 

unsigned char CRC8_Byte[256] = {
    0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83,
    0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41, 
    0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e,
    0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc, 
    0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0,
    0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62, 
    0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d,
    0x7c, 0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
    0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5, 
    0x84, 0xda, 0x38, 0x66, 0xe5, 0xbb, 0x59, 0x07,
    0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58,
    0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a,
    0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6, 
    0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24, 
    0xf8, 0xa6, 0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b,
    0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9, 
    0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f, 
    0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
    0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92,
    0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50, 
    0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c, 
    0x6d, 0x33, 0xd1, 0x8f, 0x0c, 0x52, 0xb0, 0xee, 
    0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1, 
    0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73, 
    0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49,
    0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
    0x57, 0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4,
    0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16, 
    0xe9, 0xb7, 0x55, 0x0b, 0x88, 0xd6, 0x34, 0x6a, 
    0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8, 
    0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9, 0xf7,
    0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35,
};
////////////////////////////////////////////////////////////////////////////
void Jump_BigStep(Type18b20 *S);
void Jump_SmlStep(Type18b20 *S);
void read_18b20(Type18b20 *S);
void write_byte(Type18b20 *S);
void read_byte(Type18b20 *S);
void reset_18b20(Type18b20 *S);
void start_18b20(Type18b20 *S);
void write_18b20REG(Type18b20 *S);
void save_18b20(Type18b20 *S);
////////////////////////////////////////////////////////////////////////////// 
//函数功能：主循环异步
//////////////////////////////////////////////////////////////////////////////
void about_18b20(Type18b20 *S)
{
	

    mpu16_10ms = st_sys_timer.u8_count_10ms;
	
    switch(S->Big.u8_step)
	{                
        case TH18B20_READ_REST4Start:
		{
			reset_18b20(S);
			if(S->Sml.u8_step >= 100)
			{
					Jump_BigStep(S);				
			}
		}break;
		
        case TH18B20_RESTART_CONVERT:
		{
			if(S->mu8_No_18b20 == 0)
            {
                start_18b20(S);
                if(S->Sml.u8_step >= 100){Jump_BigStep(S);}
            }
            else
            {
				if((u8)(mpu16_10ms - S->Big.u8_us) < 40) //断电400ms
				{
					SET_PORT_18B20_POWEROFF(S->XB);
				}
				else if((u8)(mpu16_10ms - S->Big.u8_us) < 80) //上电400ms
				{SET_PORT_18B20_POWERON(S->XB);}
				else
				{
					SET_PORT_18B20_POWERON(S->XB);
					Jump_BigStep(S);
					S->Big.u8_step = TH18B20_WAIT_FOR_NEXT_CONVERT;
				}
            }            
		}break;
		
        case TH18B20_WAIT_FOR_READ:
		{if((u8)(mpu16_10ms - S->Big.u8_us) > 75){Jump_BigStep(S);}}break;
		
        case TH18B20_READ_REST4Read:
		{
			reset_18b20(S);
			if(S->Sml.u8_step >= 100){Jump_BigStep(S);}
		}break;
        
        case TH18B20_READ_TEMP:
        {
            if(S->mu8_No_18b20 == 0)
            {
                read_18b20(S);
                if(S->Sml.u8_step >= 100){S->u8_18b20_not_read_count = 0;Jump_BigStep(S);}                
            }
            else
            {
                if(++S->u8_18b20_not_read_count >= S->u8_ERROR_MAX)
                {S->u8_18b20_error = CANNOT_READ_18B20;}// 当连续不能读18B20温度的次数大于此值时，18B20报错                
                Jump_BigStep(S);
            }            
        }break;
        
        case TH18B20_WAIT_FOR_NEXT_CONVERT:
		{
            if((u8)(mpu16_10ms - S->Big.u8_us) > 10)
            {
                if(S->u8_set & BIT(C18B20_WriteADJ))
                {
                    S->u8_set &=~ BIT(C18B20_WriteADJ);
                    Jump_BigStep(S);S->Big.u8_step = TH18B20_REST4WRITE_ADJ;
                }
                else
                {Jump_BigStep(S);S->Big.u8_step = 0;}
            }
        }break;
        
        case TH18B20_REST4WRITE_ADJ:
        {
            reset_18b20(S);
			if(S->Sml.u8_step >= 100){Jump_BigStep(S);}
            S->Big.u8_us = TCNT_us;
        }break;
        
        case TH18B20_WRITE_ADJ: 
        {
            write_18b20REG(S);
            if(S->Sml.u8_step >= 100){Jump_BigStep(S);}
        }break;
        
        case TH18B20_REST4SAVE:
        {
            reset_18b20(S);
			if(S->Sml.u8_step >= 100){Jump_BigStep(S);}
            S->Big.u8_us = TCNT_us;
        }break;
        
        case TH18B20_WRITE_SAVE:
        {
            save_18b20(S);
            if(S->Sml.u8_step >= 100){Jump_BigStep(S);S->Big.u8_step = 0;}
        }break;
        
        default:{S->Big.u8_step = 0;}	
	}//sw
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：初始化变量
//////////////////////////////////////////////////////////////////////////////
void init_18b20(Type18b20 *S)
{
	  S->u8_18b20_error = 0;
    S->u8_Count = 0;
    S->Big.u8_us = mpu16_10ms;
    S->Sml.u8_us = TCNT_us;
    S->Big.u8_step = S->Sml.u8_step = S->Tiny.u8_step = 0;    
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：18B20复位，发送开始温度转换命令，18B20开始温度转换
//////////////////////////////////////////////////////////////////////////////
void start_18b20(Type18b20 *S)
{
    switch(S->Sml.u8_step)
    {
        case 0:{S->Tiny.u8_val = SKIP_ROM;        write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //跳过ROM
        case 1:{S->Tiny.u8_val = START_CONVERT;   write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //发送读温度命令
        default:{S->Sml.u8_step = 100;}break;
    }
}
//////////////////////////////////////////////////////////////////////////////
//函数功能: 写校准数据
//////////////////////////////////////////////////////////////////////////////
void write_18b20REG(Type18b20 *S)
{
    switch(S->Sml.u8_step)
    {
        case 0:{S->Tiny.u8_val = SKIP_ROM;        write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //跳过ROM
        case 1:{S->Tiny.u8_val = WRITE_18B20REG;  write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //写寄存器 TH TL
        case 2:{S->Tiny.u8_val = S->u8_adj;       write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //写TH
        case 3:{S->Tiny.u8_val = S->u8_adj;       write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //写TL
        default:{S->Sml.u8_step = 100;}break;
    }
}
//////////////////////////////////////////////////////////////////////////////
//函数功能：掉电保存命令
//////////////////////////////////////////////////////////////////////////////
void save_18b20(Type18b20 *S)
{
    switch(S->Sml.u8_step)
    {
        case 0:{S->Tiny.u8_val = SKIP_ROM;        write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //跳过ROM
        case 1:{S->Tiny.u8_val = SAVE18B20;       write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //
        default:{S->Sml.u8_step = 100;}break;
    }
}
//////////////////////////////////////////////////////////////////////////////
//函数功能：读18B20温度值，并且计算出真正的温度值
//注意：此函数执行前，即读温度值之前，应该等待大约2S时间，等待18B20转换完成
//返回的为真实值的10倍，即25.3度返回253
//////////////////////////////////////////////////////////////////////////////
void read_18b20(Type18b20 *S)
{
    s32 l_calc;
    s16 i_calc;
    s8 s8_adj;
    
    switch(S->Sml.u8_step)
    {
        case 0: {S->Tiny.u8_val = SKIP_ROM;        write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //跳过ROM}
        case 1: {S->Tiny.u8_val = READ_TEMPRATURE; write_byte(S);   if(S->Tiny.u8_step >= 8){Jump_SmlStep(S);}}break;      //发送读温度命令
        case 2: {read_byte(S);   if(S->Tiny.u8_step >= 8){S->u8_crc = CRC8_Byte[S->Tiny.u8_val];           S->uc_read_temp_reg_lo_byte = S->Tiny.u8_val; Jump_SmlStep(S);}}break;
        case 3: {read_byte(S);   if(S->Tiny.u8_step >= 8){S->u8_crc = CRC8_Byte[S->u8_crc ^ S->Tiny.u8_val];  S->uc_read_temp_reg_hi_byte = S->Tiny.u8_val; Jump_SmlStep(S);}}break;
        case 4: {read_byte(S);   if(S->Tiny.u8_step >= 8){S->u8_crc = CRC8_Byte[S->u8_crc ^ S->Tiny.u8_val];  S->u8_TH = S->Tiny.u8_val; Jump_SmlStep(S);}}break;//TH
        case 5: {read_byte(S);   if(S->Tiny.u8_step >= 8){S->u8_crc = CRC8_Byte[S->u8_crc ^ S->Tiny.u8_val];  S->u8_TL = S->Tiny.u8_val; Jump_SmlStep(S);}}break;//TL
        case 6: {read_byte(S);   if(S->Tiny.u8_step >= 8){S->u8_crc = CRC8_Byte[S->u8_crc ^ S->Tiny.u8_val];  Jump_SmlStep(S);}}break;//config
        case 7: {read_byte(S);   if(S->Tiny.u8_step >= 8){S->u8_crc = CRC8_Byte[S->u8_crc ^ S->Tiny.u8_val];  Jump_SmlStep(S);}}break;//reserved
        case 8: {read_byte(S);   if(S->Tiny.u8_step >= 8){S->u8_crc = CRC8_Byte[S->u8_crc ^ S->Tiny.u8_val];  Jump_SmlStep(S);}}break;//reserved
        case 9: {read_byte(S);   if(S->Tiny.u8_step >= 8){S->u8_crc = CRC8_Byte[S->u8_crc ^ S->Tiny.u8_val];  Jump_SmlStep(S);}}break;//reserved
        case 10:
        {
            read_byte(S);
            if(S->Tiny.u8_step >= 8)
            {
                //CRC效验
                if(S->u8_crc == S->Tiny.u8_val)  //自己计算的CRC与读出的CRC值进行比较。
                {
                    //无符号数凑成有符号数
                    i_calc = S->uc_read_temp_reg_hi_byte;
                    i_calc = i_calc << 8;
                    i_calc = (i_calc & 0xff00) | S->uc_read_temp_reg_lo_byte;
                    //有符号数扩大类型
                    l_calc = i_calc;
                    l_calc *= 625;
                    
                    l_calc /= 100;//证明可以测负温度。只是没有四舍五入而已 0.01度
                    
                    S->s16_Original001 = i_calc = l_calc;
                    
                    S->u8_18b20_crc_error_count = 0;
                    if(i_calc == 8500){S->u8_18b20_error = 85;}
                    else
                    {
                        if((S->u8_TH != S->u8_TL) || ((s8)S->u8_TH > 111) || ((s8)S->u8_TH < -111)){s8_adj = 0;}
                        else{s8_adj = S->u8_TH;}
                        
                        S->s16_adj = (s16)s8_adj;
                        
                        i_calc = (i_calc - (s16)s8_adj); //修正过的0.01度
                        if(S->u8_set & BIT(C18B20_SET_is01)){i_calc /= 10;}
                        
                        S->u8_18b20_error = 0;
                        S->s16_Temperature = i_calc;
                        
                        S->u8_Count++;
                    }//读到正确的数时，清除18B20错误标志
                }
                else
                {
                    if(++S->u8_18b20_crc_error_count >= S->u8_ERROR_MAX)
                    {S->u8_18b20_error = CRC_ERROR_18B20;}// 当连续CRC校验错误的次数大于此值时，18B20报错                			
                }   
                
                Jump_SmlStep(S);
                S->Sml.u8_step = 100;
            }
        }break;
        default:{S->Sml.u8_step = 100;}break;
    }//sw
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：18B20复位
//////////////////////////////////////////////////////////////////////////////
void reset_18b20(Type18b20 *S)
{
    
    switch(S->Sml.u8_step)
    {
        case 0:
        {
            SET_PORT_18B20_OUT(S->XB); //将数据总线PE3设置为输出口  
            CLS_PIN_18B20(S->XB);//拉低数据总线PE3，开始复位
            Jump_SmlStep(S);
        }break;
		
		case 1:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100){Jump_SmlStep(S);}
        }break;
		
		case 2:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100){Jump_SmlStep(S);}
        }break;
		
		case 3:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100){Jump_SmlStep(S);}
        }break;
		
		case 4:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100){Jump_SmlStep(S);}
        }break;
		
		case 5:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100)//至少480us，这里至少500us
            {
                SET_PORT_18B20_IN(S->XB);
                SET_PIN_18B20(S->XB);
                Jump_SmlStep(S);
				S->u8_50usCnt = 2;//由50us定时器--，获得50-100us的延时,中断里仍弱上拉
            }
        }break;//mu8_isHave18b20
        
        case 6:
        {
			if(S->u8_50usCnt == 0)//从15us以后可读取，18b20会保持240低电平
			{
				S->mu8_No_18b20 = S->u8_ReadInInt; //中断里读取了INP_PIN_18B20(S->XB);
				Jump_SmlStep(S);
			}
        }break;
        
        case 7:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100){Jump_SmlStep(S);}
        }break;
		
		case 8:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100){Jump_SmlStep(S);}
        }break;
		
		case 9:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100){Jump_SmlStep(S);}
        }break;
		
		case 10:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100){Jump_SmlStep(S);}
        }break;
		
		case 11:
        {
            if((u8)(TCNT_us - S->Sml.u8_us) >= 100){Jump_SmlStep(S);S->Sml.u8_step = 100;}
        }break;
        
        default:{S->Sml.u8_step = 100;}break;
    }
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：向18B20写单个位
//////////////////////////////////////////////////////////////////////////////
void write_bit(Type18b20 *S,u8 bit_value)
{
    _CLI();
    SET_PORT_18B20_OUT(S->XB); //将数据总线PE3设置为输出口
    CLS_PIN_18B20(S->XB);//拉低 以开始一个写时序
    
    //6us//做延时，在时序图上，如果位为高，需要这个延时
    S->Tiny.u8_us = TCNT_us;while((u8)(TCNT_us - S->Tiny.u8_us) < 6){FeedDog();}
    
    if(bit_value)//在下次操作前已经延时了，实现了加速
    {
		SET_PIN_18B20(S->XB);
	    SET_PORT_18B20_IN(S->XB);    //释放PF0总线，将数据总线PF0设置为输入口
		S->u8_50usCnt = 0;
	}//如果写“1”，则将总线置高 ,然后就可以出去了
	else
	{S->u8_50usCnt = 2;} //继续拉低，利用50us中断，保持继续低50-100us，要求不可超过120us，这样做最长106us
	
	//如果没有50us定时器，则必须延时
    //else//虽然在此延时，但也没耽误时间，因为并没有改变u8_us
    //{ S->Tiny.u8_us = TCNT_us;while((u8)(TCNT_us - S->Tiny.u8_us) < 60){FeedDog();}}

    _SEI(); 
    
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：向18B20写一个字节数据
//////////////////////////////////////////////////////////////////////////////
void write_byte(Type18b20 *S)
{    
    if(((u8)(TCNT_us - S->Tiny.u8_us) > 60) && (S->u8_50usCnt == 0))
    {
        write_bit(S,(S->Tiny.u8_val >> S->Tiny.u8_step) & 0x01);//向总线写该位
        S->Tiny.u8_step++;
    }
}	

//////////////////////////////////////////////////////////////////////////////
//函数功能：读18B20一个位
//////////////////////////////////////////////////////////////////////////////
u8 read_bit(Type18b20 *S)
{
    u8 uc_temp;
    
    _CLI(); 
    SET_PORT_18B20_OUT(S->XB); //将数据总线PE3设置为输出口
    
    CLS_PIN_18B20(S->XB);//拉低 以开始一个写时序
    
    //2us延时以后再释放总线
    S->Tiny.u8_us = TCNT_us;
    while((u8)(TCNT_us - S->Tiny.u8_us) < 2){FeedDog();}
    
    
    SET_PORT_18B20_IN(S->XB);//释放PF0总线，将数据总线PF0设置为输入口
    SET_PIN_18B20(S->XB);
    
    //延时后再读数据，延时时间现在是12us，至此共延时2+10=12us
    S->Tiny.u8_us = TCNT_us;
    while((u8)(TCNT_us -  S->Tiny.u8_us) < 10){FeedDog();}
    
    uc_temp = INP_PIN_18B20(S->XB);
    S->Tiny.u8_us = TCNT_us;
    _SEI();
    //延时60us，等待总线恢复    至此共延时2+10+60=72us
    //while((u8)(TCNT_us -  S->Tiny.u8_us) < 60){FeedDog();}
    //不延时了，作为循环的，进入的时候判断
    return(uc_temp);
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：读18B20一个字节数据
//////////////////////////////////////////////////////////////////////////////
void read_byte(Type18b20 *S)
{    
    if(S->Tiny.u8_step == 0){S->Tiny.u8_val = 0;}
    
    if((u8)(TCNT_us - S->Tiny.u8_us) > 60)
    {
        if(read_bit(S)){S->Tiny.u8_val |= BIT(S->Tiny.u8_step);}//读一字节数据，一个读时序中读一位，并做移位处理
        S->Tiny.u8_step++;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void Jump_BigStep(Type18b20 *S)
{
    S->Big.u8_step++;
    S->Sml.u8_step = 0;
    S->Big.u8_us = mpu16_10ms;//仅仅在about函数中用过，代表MS
    S->Tiny.u8_us = S->Sml.u8_us = TCNT_us;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
void Jump_SmlStep(Type18b20 *S)
{
    S->Sml.u8_us = TCNT_us; //S->Sml.u8_us仅仅在reset函数中用过
    S->Sml.u8_step++;
    
    S->Tiny.u8_step = 0;//step清零，但是时间并不清零，因为一开始就要用
}

void Write18b20TempAdj(Type18b20 *S,s16 s16_adj)
{
    s8 s8_tmp;    
    if((s16_adj >= -111) && (s16_adj <= 111))
    {
        s8_tmp = (s8)s16_adj;
        S->u8_adj = (u8)s8_tmp;
        S->u8_set |= BIT(C18B20_WriteADJ);
    }
}

void Calibration18b20atZero(Type18b20 *S)
{
    s8 s8_tmp;
    if((S->s16_Original001 >= -111) && (S->s16_Original001 <= 111))
    {
        s8_tmp = (s8)S->s16_Original001;
        S->u8_adj = (u8)s8_tmp;
        S->u8_set |= BIT(C18B20_WriteADJ);
    }
}


