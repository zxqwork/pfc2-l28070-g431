#include "base.h"
#include "sys_var.h" //?
#include "TH_Timer.h"
#include "18b20.h"
#include "bsp.h"
#include "CRC_LRC.h"

#define SKIP_ROM        0XCC
#define START_CONVERT   0X44
#define READ_TEMPRATURE 0XBE

#define TH18B20_READ_REST4Start             0
#define TH18B20_RESTART_CONVERT             1
#define TH18B20_WAIT_FOR_READ               2
#define TH18B20_READ_REST4Read              3
#define TH18B20_READ_TEMP                   4
#define TH18B20_WAIT_FOR_NEXT_CONVERT       5
////////////////////////////////////////////////////////////////////////////
void Jump_BigStep(void);
////////////////////////////////////////////////////////////////////////////
void Jump_SmlStep(void);
////////////////////////////////////////////////////////////////////////////
void read_18b20(void);
////////////////////////////////////////////////////////////////////////////
typedef struct
{
    u16 u16_us;
    u16 u16_step;
    u8  u8_val;
}Type18b20Step;
Type18b20Step Big,Sml,Tiny;

u8  mu8_No_18b20 = 0;
u8  mu8_18b20_not_read_count = 0;

u8  mu8_ERROR_MAX = 5;       //错误报警上限

s16 *ps16_Temperature;
u16 *pu16_18b20_error;

void write_byte(void);
void read_byte(void);
void reset_18b20(void);
void start_18b20(void);

__flash u8 crc_table[256]={
	0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
    157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
    35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
    190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
    70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
    219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
    101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
    248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
    140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
    17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
    175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
    50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
    202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
    87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
    233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
    116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53};

////////////////////////////////////////////////////////////////////////////// 
//函数功能：主循环异步
//////////////////////////////////////////////////////////////////////////////
void about_18b20(void)
{
    switch(Big.u16_step)
	{                
        case TH18B20_READ_REST4Start:
		{
			reset_18b20();
			if(Sml.u16_step >= 100){Jump_BigStep();}
            Big.u16_us = TCNT_us;
		}break;
		
        case TH18B20_RESTART_CONVERT:
		{
			if(mu8_No_18b20 == 0)
            {
                start_18b20();
                if(Sml.u16_step >= 100){Jump_BigStep();}
            }
            else
            {
                Jump_BigStep();
                Big.u16_step = TH18B20_WAIT_FOR_NEXT_CONVERT;
            }            
		}break;
		
        case TH18B20_WAIT_FOR_READ:
		{if((u16)(st_sys_timer.u16_count_1ms - Big.u16_us) > 2000){Jump_BigStep();}}break;
		
        case TH18B20_READ_REST4Read:
		{
			reset_18b20();
			if(Sml.u16_step >= 100){Jump_BigStep();}
		}break;
        
        case TH18B20_READ_TEMP:
        {
            if(mu8_No_18b20 == 0)
            {
                read_18b20();
                if(Sml.u16_step >= 100){mu8_18b20_not_read_count = 0;Jump_BigStep();}                
            }
            else
            {
                if(++mu8_18b20_not_read_count >= mu8_ERROR_MAX)
                {*pu16_18b20_error = CANNOT_READ_18B20;}// 当连续不能读18B20温度的次数大于此值时，18B20报错                
                Jump_BigStep();
            }            
        }break;
        
        case TH18B20_WAIT_FOR_NEXT_CONVERT:
		{if((u16)(st_sys_timer.u16_count_1ms - Big.u16_us) > 1000){Jump_BigStep();Big.u16_step = 0;}}break;
        
        default:{Big.u16_step = 0;}	
	}//sw
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：初始化变量
//////////////////////////////////////////////////////////////////////////////
void init_18b20(s16 *s16_Temperature_X_10,u16 *u16_18b20_error,u8 ErrorMax)
{
    mu8_ERROR_MAX = ErrorMax;
    pu16_18b20_error = u16_18b20_error;
    ps16_Temperature = s16_Temperature_X_10;
    Big.u16_us = st_sys_timer.u16_count_1ms;
    Sml.u16_us = TCNT_us;
    Big.u16_step = Sml.u16_step = Tiny.u16_step = 0;    
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：18B20复位，发送开始温度转换命令，18B20开始温度转换
//////////////////////////////////////////////////////////////////////////////
void start_18b20(void)
{
    switch(Sml.u16_step)
    {
        case 0:{Tiny.u8_val = SKIP_ROM;        write_byte();   if(Tiny.u16_step >= 8){Jump_SmlStep();}}break;      //跳过ROM
        case 1:{Tiny.u8_val = START_CONVERT;   write_byte();   if(Tiny.u16_step >= 8){Jump_SmlStep();}}break;      //发送读温度命令
        default:{Sml.u16_step = 100;}break;
    }
}
//////////////////////////////////////////////////////////////////////////////
//函数功能：读18B20温度值，并且计算出真正的温度值
//注意：此函数执行前，即读温度值之前，应该等待大约2S时间，等待18B20转换完成
//返回的为真实值的10倍，即25.3度返回253
//////////////////////////////////////////////////////////////////////////////
void read_18b20(void)
{
    s32 l_calc;
    s16 i_calc;
    
    static u8 uc_read_temp_reg_lo_byte;
    static u8 uc_read_temp_reg_hi_byte;
    
    static u8  u8_crc;
    static u8 su8_18b20_crc_error_count = 0;
    
    switch(Sml.u16_step)
    {
        case 0: {Tiny.u8_val = SKIP_ROM;        write_byte();   if(Tiny.u16_step >= 8){Jump_SmlStep();}}break;      //跳过ROM}
        case 1: {Tiny.u8_val = READ_TEMPRATURE; write_byte();   if(Tiny.u16_step >= 8){Jump_SmlStep();}}break;      //发送读温度命令
        case 2: {read_byte();   if(Tiny.u16_step >= 8){u8_crc = CRC8_Byte(Tiny.u8_val);           uc_read_temp_reg_lo_byte = Tiny.u8_val; Jump_SmlStep();}}break;
        case 3: {read_byte();   if(Tiny.u16_step >= 8){u8_crc = CRC8_Byte(u8_crc ^ Tiny.u8_val);  uc_read_temp_reg_hi_byte = Tiny.u8_val; Jump_SmlStep();}}break;
        case 4: {read_byte();   if(Tiny.u16_step >= 8){u8_crc = CRC8_Byte(u8_crc ^ Tiny.u8_val);  Jump_SmlStep();}}break;//TH
        case 5: {read_byte();   if(Tiny.u16_step >= 8){u8_crc = CRC8_Byte(u8_crc ^ Tiny.u8_val);  Jump_SmlStep();}}break;//TL
        case 6: {read_byte();   if(Tiny.u16_step >= 8){u8_crc = CRC8_Byte(u8_crc ^ Tiny.u8_val);  Jump_SmlStep();}}break;//config
        case 7: {read_byte();   if(Tiny.u16_step >= 8){u8_crc = CRC8_Byte(u8_crc ^ Tiny.u8_val);  Jump_SmlStep();}}break;//reserved
        case 8: {read_byte();   if(Tiny.u16_step >= 8){u8_crc = CRC8_Byte(u8_crc ^ Tiny.u8_val);  Jump_SmlStep();}}break;//reserved
        case 9: {read_byte();   if(Tiny.u16_step >= 8){u8_crc = CRC8_Byte(u8_crc ^ Tiny.u8_val);  Jump_SmlStep();}}break;//reserved
        case 10:
        {
            read_byte();
            if(Tiny.u16_step >= 8)
            {
                //CRC效验
                if(u8_crc == Tiny.u8_val)  //自己计算的CRC与读出的CRC值进行比较。
                {
                    //无符号数凑成有符号数
                    i_calc = uc_read_temp_reg_hi_byte;
                    i_calc = i_calc << 8;
                    i_calc = (i_calc & 0xff00) | uc_read_temp_reg_lo_byte;
                    //有符号数扩大类型
                    l_calc = i_calc;
                    l_calc *= 625;
                    
                    l_calc /= 1000;//证明可以测负温度。只是没有四舍五入而已
                    i_calc = l_calc;
                    
                    su8_18b20_crc_error_count = 0;
                    if(i_calc == 850){*pu16_18b20_error = TEMP_ERROR_18B20;}
                    else{*pu16_18b20_error = 0;*ps16_Temperature = i_calc;}//读到正确的数时，清除18B20错误标志
                }
                else
                {
                    if(++su8_18b20_crc_error_count >= mu8_ERROR_MAX)
                    {*pu16_18b20_error = CRC_ERROR_18B20;}// 当连续CRC校验错误的次数大于此值时，18B20报错                			
                }            
                
                Jump_SmlStep();
                Sml.u16_step = 100;
            }
        }break;
        default:{Sml.u16_step = 100;}break;
    }//sw
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：18B20复位
//////////////////////////////////////////////////////////////////////////////
void reset_18b20(void)
{
    switch(Sml.u16_step)
    {
        case 0:
        {
            SET_PORT_18B20_OUT; //将数据总线PE3设置为输出口  
            CLS_PIN_18B20;//拉低数据总线PE3，开始复位
            Jump_SmlStep();
        }break;
        
        case 1:
        {
            if((u16)(TCNT_us - Sml.u16_us) >= 510)
            {
                SET_PORT_18B20_IN;
                SET_PIN_18B20;
                Jump_SmlStep();
            }
        }break;//mu8_isHave18b20
        
        case 2:
        {
            while((u16)(TCNT_us - Sml.u16_us) <= 66){FeedDog();}
            mu8_No_18b20 = INP_PIN_18B20;
            Jump_SmlStep();
        }break;
        
        case 3:
        {
            if((u16)(TCNT_us - Sml.u16_us) >= 510){Jump_SmlStep();Sml.u16_step = 100;}
        }break;
        
        default:{Sml.u16_step = 100;}break;
    }
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：向18B20写单个位
//////////////////////////////////////////////////////////////////////////////
void write_bit(u8 bit_value)
{
    _CLI();
    SET_PORT_18B20_OUT; //将数据总线PE3设置为输出口
    CLS_PIN_18B20;//拉低 以开始一个写时序
    
    //6us//做延时，在时序图上，如果位为高，需要这个延时
    Tiny.u16_us = TCNT_us;while((u16)(TCNT_us - Tiny.u16_us) < 6){FeedDog();}
    
    if(bit_value)//在下次操作前已经延时了，实现了加速
    {SET_PIN_18B20;}//如果写“1”，则将总线置高 ,然后就可以出去了
    else//虽然在此延时，但也没耽误时间，因为并没有改变u16_us
    { Tiny.u16_us = TCNT_us;while((u16)(TCNT_us - Tiny.u16_us) < 60){FeedDog();}}
    
    SET_PORT_18B20_IN;    //释放PF0总线，将数据总线PF0设置为输入口
    SET_PIN_18B20;
    _SEI(); 
    
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：向18B20写一个字节数据
//////////////////////////////////////////////////////////////////////////////
void write_byte(void)
{    
    if((u16)(TCNT_us - Tiny.u16_us) > 60)
    {
        write_bit((Tiny.u8_val >> Tiny.u16_step) & 0x01);//向总线写该位
        Tiny.u16_step++;
    }
}	

//////////////////////////////////////////////////////////////////////////////
//函数功能：读18B20一个位
//////////////////////////////////////////////////////////////////////////////
u8 read_bit(void)
{
    u8 uc_temp;
    
    _CLI(); 
    SET_PORT_18B20_OUT; //将数据总线PE3设置为输出口
    
    CLS_PIN_18B20;//拉低 以开始一个写时序
    
    //2us延时以后再释放总线
    Tiny.u16_us = TCNT_us;
    while((u16)(TCNT_us - Tiny.u16_us) < 2){FeedDog();}
    
    
    SET_PORT_18B20_IN;//释放PF0总线，将数据总线PF0设置为输入口
    SET_PIN_18B20;
    
    //延时后再读数据，延时时间现在是12us，至此共延时2+10=12us
    Tiny.u16_us = TCNT_us;
    while((u16)(TCNT_us -  Tiny.u16_us) < 10){FeedDog();}
    
    uc_temp = INP_PIN_18B20;
    Tiny.u16_us = TCNT_us;
    _SEI();
    //延时60us，等待总线恢复    至此共延时2+10+60=72us
    //while((u16)(TCNT_us -  Tiny.u16_us) < 60){FeedDog();}
    //不延时了，作为循环的，进入的时候判断
    return(uc_temp);
}

//////////////////////////////////////////////////////////////////////////////
//函数功能：读18B20一个字节数据
//////////////////////////////////////////////////////////////////////////////
void read_byte(void)
{    
    if(Tiny.u16_step == 0){Tiny.u8_val = 0;}
    
    if((u16)(TCNT_us - Tiny.u16_us) > 60)
    {
        if(read_bit()){Tiny.u8_val |= BIT(Tiny.u16_step);}//读一字节数据，一个读时序中读一位，并做移位处理
        Tiny.u16_step++;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void Jump_BigStep(void)
{
    Big.u16_step++;
    Sml.u16_step = 0;
    Big.u16_us = st_sys_timer.u16_count_1ms;//仅仅在about函数中用过，代表MS
    Tiny.u16_us = Sml.u16_us = TCNT_us;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
void Jump_SmlStep(void)
{
    Sml.u16_us = TCNT_us; //Sml.u16_us仅仅在reset函数中用过
    Sml.u16_step++;
    
    Tiny.u16_step = 0;//step清零，但是时间并不清零，因为一开始就要用
}