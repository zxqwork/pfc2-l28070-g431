#include "RollFltWithSuspect.h"
#include "base.h"
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u16 is_RollFlt_Believable(stRollFltWithSuspect *R,u32 u32_now)
{
    u32 u32Total,j; //暂时不考虑溢出问题
    u16 i,BelieveCnt;
    u16 RetBelievable;
    
    //指针和队列
    R->p++;         //当前指针
    R->p &= 0x1f;   //32内循环
    R->Arry[R->p] = u32_now;        //放队列
    R->pTail = (R->p + 1) & 0x1f; //最尾指针
    
    //是否置信
    if(R->IntialStage){R->IntialStage--;R->SuspectBit = 0;}
    if((Unsinged_ABS(R->u32_Avg,u32_now) < R->u16_SuspectDoor) || R->IntialStage)//不超过怀疑门限，或者刚刚工作
    {
        RetBelievable = 0xffff;   //可信
        R->SuspectBit &=~ BIT((u32)R->p);
    }
    else
    { 
        RetBelievable = 0;   //不可信
        R->SuspectBit |= BIT((u32)R->p);
        
    }   
    //求可信的和
    u32Total = 0;BelieveCnt = 0;
    for(i = 0;i < 32 ;i++)
    {
        if((R->SuspectBit & BIT((u32)i)) == 0)
        {
            u32Total += R->Arry[i];
            BelieveCnt++;//可信计数器
        }
    }
    
    //可信的数<16个了 20ms一个都300ms了
    if(BelieveCnt <= 16)
    {
        //计算最近的8个数
        u32Total = 0;
        if(R->p){j = R->p - 1;}else{j = 0x1f;}
        for(i = 0;i < 8 ;i++)
        {u32Total += R->Arry[j]; if(j){j--;}else{j = 0x1f;}}
        
        //当前值和近8个不离谱
        j = u32Total >> 3;        
        if(Unsinged_ABS(u32_now,j) < (R->u16_SuspectDoor << 1))//置信
        {
            //重填队列，全部置信
            for(i = 0;i < 32 ;i++){R->Arry[i] = u32_now;}
            RetBelievable = 0xffff;   
            R->SuspectBit = 0;            
            R->u32_Avg = u32_now;  
        }
        else if(BelieveCnt)//只要还有可信的数
        {
            R->u32_Avg = u32Total / BelieveCnt;
        }
    }
    else//可信的数>16个
    {
        R->u32_Avg = u32Total / BelieveCnt;
    }
    R->BelieveCnt = BelieveCnt;
    return(RetBelievable);
}