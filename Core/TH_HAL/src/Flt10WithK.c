#include "base.h"
#include "Flt10WithK.h"

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
void Calc_GetPhyArrayAvg(PhysicsParaType *Phy,u16 u16_val,u8 u8_t1ms)
{
    u8 j;
    u32 u32_tmp;
    
    u32_tmp = 0; //平均值计算的累积
    
    for(j = 18; j > 0; j--)
    {
        Phy->u16_ValArray[j] = Phy->u16_ValArray[j - 1];
        Phy->u8_T_msArray[j] = Phy->u8_T_msArray[j - 1];
    }
    for(j = 8; j > 0; j--)
    {
        u32_tmp += (u32)Phy->u16_ValArray[j];  //加了8个
    }
    Phy->u16_ValArray[0] = Phy->u16_val; //目前Phy->u16_val还是上次的值
    u32_tmp += (u32)Phy->u16_val;        //加第9个
    
    Phy->u16_val = u16_val;              //Phy->u16_val更新成当前值了
    u32_tmp += (u32)u16_val;             //加第10个
    u32_tmp += 5;  //ver10019
    u32_tmp /= 10;
    Phy->u16_avg = (u16)u32_tmp;
    
    Phy->u8_T_msArray[0] = (u8)(u8_t1ms - Phy->u8_old_T_ms);//这次的周期
    Phy->u8_old_T_ms = u8_t1ms;	
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//目前这是每2秒的斜率
void Calc_GetCtrlPhyK(PhysicsParaType *Phy,u16 u16_amp)
{
    u8  j;
    u16 u16_tmp;
    s32 s32_tmp;
    
    u16_tmp = 0;	   //时差的累计值
    for(j = 0; j <= 18; j++)
    {
        u16_tmp += (u16)Phy->u8_T_msArray[j];  //19个间隔
    }
    //u16_tmp是20个采样点的累积间隔，ms
    
    s32_tmp  = Phy->u16_val;
    s32_tmp -= (s32)Phy->u16_ValArray[18];
    s32_tmp *= (s32)u16_amp;
    s32_tmp = (s32_tmp * 2000) / (s32)u16_tmp;
    
    if     (s32_tmp > 32767) {Phy->s16_NowK = 32767;}
    else if(s32_tmp < -32768){Phy->s16_NowK = -32768;}
    else{Phy->s16_NowK = (s16)s32_tmp;}
    
    //amp = 10 放大了10倍
    //比如压力，压力单位是0.01MPa，可能量程150.00MPa,
    //K单位 每秒0.001MPa,s16范围:+-3.768Ma/s
    
    //比如功率，功率0.001kw，可能的量程50.000kw
    //斜率是每秒0.1W s16的范围是 +-3.2768kw/s
    //
}
