#include "TH_Timer.h"
/////////////////////////////////////////////////////////////////////
//定义一个系统时钟变量:
/////////////////////////////////////////////////////////////////////
st_Timer_TypeDef st_sys_timer;
/////////////////////////////////////////////////////////////////////
//
//内部定时器1ms计时函数，请在中断中调用
//
void DRV_Timer1ms(void)
{
  
  static u8 suc_for_5ms = 5;
  static u8 suc_for_10ms = 10;
  static u8 suc_for_50ms = 5;
  static u8 suc_for_1s = 10;
  static u8 suc_for_1m = 60;  //////////邢
  st_sys_timer.u8_count_1ms++;
  st_sys_timer.u16_count_1ms++;
  if(st_sys_timer.u8_count_1ms & 1){st_sys_timer.u16_msg_2ms = 0xffff;}
  if(--suc_for_5ms == 0){st_sys_timer.u8_count_5ms++;suc_for_5ms = 5;}
  
  st_sys_timer.u16_msg_1ms = 0xffff;
  if(--suc_for_10ms == 0)
  {
    suc_for_10ms = 10;
    st_sys_timer.u8_count_10ms++;
    st_sys_timer.u16_msg_10ms = 0xffff;
    
    if(--suc_for_50ms == 0)
    {
      suc_for_50ms = 5;
      st_sys_timer.u8_count_50ms++;
      st_sys_timer.u16_msg_50ms = 0xffff;
      if((st_sys_timer.u8_count_50ms & 1) == 0)
      {
        st_sys_timer.u8_count_100ms++;
        st_sys_timer.u16_msg_100ms = 0xffff;
	
        if(--suc_for_1s == 0)
        {
          suc_for_1s = 10;
          st_sys_timer.u8_count_1s++;
          st_sys_timer.u16_msg_1s = 0xffff;
          if(--suc_for_1m ==0) //一下邢文姬新加
          {
            suc_for_1m = 60;//
            st_sys_timer.u8_count_1m++;//
            st_sys_timer.u16_msg_1m = 0xffff;//
          }
        }
        
      }
    }
  }
}





