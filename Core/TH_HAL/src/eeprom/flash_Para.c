#include "base.h"
#include "stm32g4xx_hal.h"
#include "stm32g4xx_hal_flash.h"
#include "stm32_hal_legacy.h"

#include "sys_var.h"
#include "app_conf.h"
#include "Comm.h"
#include "flash_Para.h"
#include "BSP.h"
#include "rtc.h"


u16 eepNeedSaveMsg = 0;  //类变量 这个变量的每一个位，代表一页，每页2k，最多允许1024个字，因为在通讯表里，地址以1024为单位分段
u16 batNeedSaveMsg = 0;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void SetNeedSave(u16 u16_Add)
{
    if(u16_Add < AddBKP)
    {
        u16_Add = u16_Add >> 10;
        eepNeedSaveMsg |= BIT(u16_Add);
    }
    else
    {batNeedSaveMsg = 1;}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void init_read_from_CommVar_Table(st_CommVar_bigTable_Typedef const *v,u16 u16_start_xb, u16 u16_end_xb,u32 u32FlashParaBeginAdd)
{
    u16 i;
    u16 u16_tmp,u16_deep;
    u16 *uip;
    u16 const *u16_Const;
    u16  u16_Offset;
    s16 s16_tmp;
    
    eepNeedSaveMsg = 0;
    
    for(i = u16_start_xb;i <= u16_end_xb;i++)
    {
        if((v + i)->ui_eep_addr)
        {
            u8 u8_data_type = ((v + i)->uc_data_type) & 0x7f;//不管是不是readonly, 那是通讯的要求,存储必须允许
            if((v + i)->ui_eep_addr < AddBKP) //10页允许 
            {
                //1000为单位
                u16_tmp = (v + i)->ui_eep_addr / 1000; //页数, 比如通讯表里写的是3023，则是第三页
                u16_Const = (u16 const *)(u16_tmp * FLASH_PARA_PAGE_SIZE + u32FlashParaBeginAdd);
                u16_Offset = u16_tmp * 1000;    //比如通讯表里写的是3023，则要减去3000，地址是23
                
                if(u8_data_type == en_VAR_TYPE_U16) //字，
                {
                    //读字的方法和写字的顺序有关，本程序先写低字节
                    *(v + i)->ui_p = u16_Const[(v + i)->ui_eep_addr - u16_Offset];
                    if(*(v + i)->ui_p > (v + i)->ui_max){*(v + i)->ui_p = (v + i)->ui_default;}
                    if(*(v + i)->ui_p < (v + i)->ui_min){*(v + i)->ui_p = (v + i)->ui_default;}
                }
                else if(u8_data_type == en_VAR_TYPE_S16) //字，
                {
                    *(v + i)->ui_p = u16_Const[(v + i)->ui_eep_addr - u16_Offset];
                    s16_tmp = (s16)(*(v + i)->ui_p);
                    if(s16_tmp > (s16)(v + i)->ui_max){*(v + i)->ui_p = (v + i)->ui_default;}
                    if(s16_tmp < -(s16)((v + i)->ui_min)){*(v + i)->ui_p = (v + i)->ui_default;} 
                }
                else if(u8_data_type == en_VAR_TYPE_ARRAY_U16) //有队列有深度，min=深度下标
                {
                    uip = (v + i)->ui_p;
                    for(u16_deep = 0;u16_deep <= (v + i)->ui_min;u16_deep++)
                    {
                        u16_tmp = u16_Const[(v + i)->ui_eep_addr -  u16_Offset + u16_deep];
                        if(u16_tmp > (v + i)->ui_max){*uip = (v + i)->ui_default;}
                        else{*uip = u16_tmp;}
                        
                        uip++;
                    }
                }
            }
            else  //大于等于的存入bkp寄存器,注意在通讯大表中，用BKP的宏定义地址
            {
                *(v + i)->ui_p = HAL_RTCEx_BKUPRead(&hrtc,(v + i)->ui_eep_addr - AddBKP);
                //*(v + i)->ui_p = BKP_ReadBackupRegister((v + i)->ui_eep_addr - AddBKP); // 地址从 1 起
                if(u8_data_type == en_VAR_TYPE_S16)
                {
                    s16_tmp = (s16)(*(v + i)->ui_p);
                    if(s16_tmp > (s16)(v + i)->ui_max){*(v + i)->ui_p = (v + i)->ui_default;}
                    if(s16_tmp < -(s16)((v + i)->ui_min)){*(v + i)->ui_p = (v + i)->ui_default;}
                }
                else
                {
                    if(*(v + i)->ui_p > (v + i)->ui_max){*(v + i)->ui_p = (v + i)->ui_default;}
                    if(*(v + i)->ui_p < (v + i)->ui_min){*(v + i)->ui_p = (v + i)->ui_default;}
                }
            }
        }
    } // end(for)
}

//--------------------------------------------------------------------------------------------------------------------------------------------------
void WritePara2BKP(st_CommVar_bigTable_Typedef const *v, u16 u16_xb_start,u16 u16_xb_end)
{
    u16 i;
    
    for(i = u16_xb_start;i <= u16_xb_end;i++)
    {
        u8 u8_data_type = ((v + i)->uc_data_type) & 0x7f;//不管是不是readonly, 那是通讯的要求,存储必须允许
        _WDR;
        if((v + i)->ui_eep_addr) //大部分是0
        {
            if((v + i)->ui_eep_addr >= AddBKP) //
            {
                if((u8_data_type == en_VAR_TYPE_U16) || (u8_data_type == en_VAR_TYPE_S16)) //字，
                {
                    HAL_RTCEx_BKUPWrite(&hrtc,((v + i)->ui_eep_addr - AddBKP), *(v + i)->ui_p);// 地址从 1 起
                }
            }
        }
    } // end(for)
    batNeedSaveMsg = 0;//回收消息
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
void FLASH_ErasePage(u32 u32_PageAdd)
{
    
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
void WritePara2Rom(st_CommVar_bigTable_Typedef const *v,u32 u32_FlashBeginAdd, u16 u16_xb)
{
    u16 i;
    u16 u16_deep;
    u16 u16_page;
    u16 PageRangeBegin,PagePangeEnd;
    u32 u32_PageAdd,u32_NowAdd;
    
    if(eepNeedSaveMsg == 0){return;}
    for(u16_page = 0;u16_page < 10;u16_page++)
    {
        if(eepNeedSaveMsg & BIT(u16_page))
        {
            eepNeedSaveMsg &=~ BIT(u16_page); //随手回收消息
            
            PageRangeBegin = u16_page * 1000;       //参数的页范围            
            PagePangeEnd = PageRangeBegin + 1000;
            u32_PageAdd = u32_FlashBeginAdd + (u32)(u16_page * FLASH_PARA_PAGE_SIZE);   //本页起始地址 2048
            
            HAL_FLASH_Unlock(); 
            bspFLASH_ClearFlag();//不加这句话在优化时好像会出问题
            
            FLASH_EraseInitTypeDef eraseInitStruct;
            eraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
            eraseInitStruct.Page = u32_PageAdd;/* 指定要擦除的页地址 */;
            eraseInitStruct.NbPages = 1; // 擦除的页数量 
            uint32_t pageError = 0;
            HAL_FLASHEx_Erase(&eraseInitStruct, &pageError);
            
            u32 byteAddCnt = 2;
            /*
#ifdef FLASH_TYPEPROGRAM_HALFWORD 
            byteAddCnt = 2;
#else
#ifdef FLASH_TYPEPROGRAM_DOUBLEWORD
            byteAddCnt = 8;
#endif
#endif
*/
            
            for(i = 0;i <= u16_xb;i++)
            {
                u8 u8_data_type = ((v + i)->uc_data_type) & 0x7f;   //不管是不是readonly, 那是通讯的要求,存储必须允许
                _WDR;
                if(((v + i)->ui_eep_addr) && ((v + i)->ui_eep_addr >= PageRangeBegin) && ((v + i)->ui_eep_addr < PagePangeEnd)) //这一页的参数
                {    
                    if((u8_data_type == en_VAR_TYPE_U16) || (u8_data_type == en_VAR_TYPE_S16)) //字，
                    {
                        u32_NowAdd = u32_PageAdd + (u32)((v + i)->ui_eep_addr - PageRangeBegin) * byteAddCnt;//2
                        //HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, u32_NowAdd, *(v + i)->ui_p);//没办法，只能写64位
                        SET_BIT(FLASH->CR, FLASH_CR_PG);
                        *(uint32_t *)u32_NowAdd = *(v + i)->ui_p;
                    }
                    else if(u8_data_type == en_VAR_TYPE_ARRAY_U16) //有队列有深度，min=深度下标
                    {
                        u32_NowAdd = u32_PageAdd + (v + i)->ui_eep_addr - PageRangeBegin;
                        for(u16_deep = 0;u16_deep <= (v + i)->ui_min;u16_deep++)
                        {
                            //HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, u32_NowAdd, *((v + i)->ui_p + u16_deep));//(u32_PageAdd + (((v + i)->ui_eep_addr - PageRangeBegin) + u16_deep) * 2, *((v + i)->ui_p + u16_deep));
                            SET_BIT(FLASH->CR, FLASH_CR_PG);
                            *(uint32_t *)u32_NowAdd = *(v + i)->ui_p;
                            u32_NowAdd += byteAddCnt; //原来是2，G474不支持16位写，只支持64位
                        }
                    }
                }
            } // end(for)
            HAL_FLASH_Lock();
        }//eepNeedSave
    }  
}

//////////////////////////////////////////////////////////////////////////////////////////////
void RecoveryDefaultA_B(st_CommVar_bigTable_Typedef const *v,u16 u16_BeginAdd,u16 u16_EndAdd)
{
    u16 i;
    
    for(i = u16_BeginAdd;i <= u16_EndAdd;i++)
    {            
        if((v + i)->uc_data_type < en_VAR_TYPE_ARRAY_U8) 
        {
            if(*(v + i)->ui_p != (v + i)->ui_default)
            {
                *(v + i)->ui_p = (v + i)->ui_default;
                if((v + i)->ui_eep_addr){SetNeedSave((v + i)->ui_eep_addr);}
            }
        }        
    }
}

//--------------------------------------------------------------------------------------------------------------------------------------------------
