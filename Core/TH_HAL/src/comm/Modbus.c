
#include "Modbus.h"
#include "bsp.h" //for test
/* 2019-09-21 新增宏
MODBUS_RTU_ONLY ， MODBUS_SLV_ONLY ，MODBUS_NoSECRET 用于简短程序长度
2021-12-31 新增宏
MODBUS_TCP
MODBUS_COIL 取代 MODBUS_NO_COIL
MODBUS_MSTER_ONLY
MODBUS_17_ONLY
*/
#ifndef MODBUS_RTU_ONLY
static u8 uc_temp_buf[ASC2RTU_TMP_BUF];  //为了节约内存，如果仅仅有RTU通讯，则不需要转换为ASC了，那么这个临时的数组就没有必要了
#endif
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//加密
void doSECRET(u8 *puc_send_buf,u16 u16_Begin,u16 u16_send_count)
{
#ifndef MODBUS_NoSECRET
    u8 uc_l,uc_h;
    u16 ui_mid;
    uc_l = *(puc_send_buf + u16_send_count - 1);
    uc_h = *(puc_send_buf + u16_send_count);
    for(ui_mid = u16_Begin; ui_mid <= u16_send_count - 2; ui_mid++)
    {
        uc_l++;
        *(puc_send_buf + ui_mid) = *(puc_send_buf + ui_mid) ^ uc_h ^ uc_l;
    }
#endif
}
//解密
void unSECRET(u8 *puc_rec_buf,u16 u16_Begin,u16 u16_rec_len)
{
#ifndef MODBUS_NoSECRET
    u8 uc_l;
    u8 uc_h;
    u16 u16_mid;
	
	  if(u16_rec_len >= 4)
		{
			uc_l = puc_rec_buf[u16_rec_len - 2];//报文加密为后两个校验码
			uc_h = puc_rec_buf[u16_rec_len - 1];
			for(u16_mid = u16_Begin; u16_mid <= u16_rec_len - 3; u16_mid++)
			{
					uc_l++;
					*(puc_rec_buf + u16_mid) = *(puc_rec_buf + u16_mid) ^ uc_h ^ uc_l;//先将报文转换成原码，然后根据校验类型校验（CRC OR THCRC）
			}
	  }
#endif
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//是否合法modbus命令
u8 u8_isModCmd(u8 u8_Cmd)
{
    if((u8_Cmd == MB_WRITE_MULTI_REG) || (u8_Cmd == MB_READ_WRITE_REG) || (u8_Cmd <= MB_WRITE_SINGLE_REG) || (u8_Cmd == MB_WRITE_MULTI_COIL) || (u8_Cmd == MB_MEI_TYPE))
    {
        return(TRUE);
    }
    else
    {
        return(FALSE);
    }
    
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//         ModBus 校验
//         可以对RTU和ASCII 方式进行校验
//Entry:
//         结构指针
//return:
//     校验成功返回TURE
// //解密在check_is_right外面完成了
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u8 u8_modbus_check_is_right(st_Comm_TypeDef *p)
{
    u16 u16_rec_len;
    u8 *puc_rec_buf;
    u8 uc_ret = 0;
    u8 uc_mid;
    
    puc_rec_buf = p->u8_RecBufP;
    
    if((p->u16_rec_p >= 4) && (p->u16_rec_p <= p->COMM_ARRAY_Rec_XB))
    {
        
#ifndef MODBUS_RTU_ONLY
        
        if(p->uc_is_asc) //modbus为asc 模式
        {
            p->u16_rec_p  -= 2;
            u16_rec_len = p->u16_rec_p;
            
            uc_mid = MAKE_HEX_TO_DEC(*(puc_rec_buf + u16_rec_len - 2),*(puc_rec_buf + u16_rec_len - 1));
            u16_rec_len	 =	convert_ascii_to_rtu(puc_rec_buf + 1,u16_rec_len - 3,puc_rec_buf); //这个和以前不一样
            if(uc_mid == lrc(puc_rec_buf,u16_rec_len))
            {
                uc_ret = puc_rec_buf[0];
            }
        }
        else             //modbus为rtu模式
#endif //~MODBUS_RTU_ONLY
        {
            u16_rec_len = p->u16_rec_p;
            
            if((p->uc_master_slaver_status & BIT(en_Comm_SLAVER_REC_EN)) || (p->uc_master_slaver_status & BIT(en_Comm_MONITOR_REC_EN)))
            {                 
                if(CRC16(puc_rec_buf,u16_rec_len - 2,(p->uc_master_slaver_status & BIT(en_Comm_TH_CRC)) | (p->uc_ThCRC)) == (((unsigned int)puc_rec_buf[u16_rec_len - 1] << 8) | (puc_rec_buf[u16_rec_len - 2])))//效验
                {
                    uc_ret = puc_rec_buf[0];
                }
            }
            else
            {
                if(CRC16(puc_rec_buf,u16_rec_len - 2,(p->st_Master_OP[p->uc_Master_OP_P].uc_status & BIT(en_CommOP_TH_CRC)) | (p->uc_ThCRC))  == (((unsigned int)puc_rec_buf[u16_rec_len - 1] << 8) | (puc_rec_buf[u16_rec_len - 2])))//效验
                {
                    uc_ret = puc_rec_buf[0];
                }
            }
        }
    }
    
    return(uc_ret);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//         ModBus RTU模式，获取本机内部的若干位的状态，并组成一个字节数据
//         RTU读位，返回若干位组成的字节，输入位的首地址、位的数量（1-8）
//         从最低位添入，未读的位为0
//Entry:
//      u16 ui_start_address: 指向开始地址。
//      u8 uc_bits ：       要读的位数。
//return:
//      返回的是若干位组成字节以后的字节的值。
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u8 rtu_read_bits(u16 ui_start_address,u8 uc_bits, u8 u8_port_num)
{
    u8 uc_ret= 0;
    u8 uc_i;
    for(uc_i=0; uc_i<uc_bits; uc_i++)
    {
        if(uc_RTU_get_bit(ui_start_address + uc_i, u8_port_num)) {
            uc_ret |= BIT(uc_i);
        }
    }
    return(uc_ret);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//       以下是自己发出了读PLC内部位的命令01，PLC返回了数据，填写入自己的内存
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//         ModBus RTU模式，操作（写）本机内部的若干位
//Entry:
//      u16 ui_bit_address:   指向开始地址。
//      u8 uc_bit_len ：    要操作（写）的位数。
//      u8 *dat：           表征若干要操作的位状态的字节数据的首地址
//return:
//      无返回
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void write_bit_by_plc(u16 ui_bit_address,u8 uc_bit_len,u8 *dat, u8 uc_port_num)
{
    u8 uc_i;
    
    for(uc_i = 0; uc_i<uc_bit_len; uc_i++)
    {
        uc_RTU_write_bit(ui_bit_address+uc_i,*(dat+uc_i/8) & BIT(uc_i%8), uc_port_num);
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//        以下是自己发出了读PLC内部变量的命令03，PLC返回了数据，填写入自己的内存
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//         ModBus RTU模式，操作（写）本机内部的字
//Entry:
//      u16 ui_word_address:   指向开始地址。
//      u8 uc_word_len ：    要操作（写）的字数量。
//      u8 *dat：           表征若干要操作的字数据的首地址
//return:
//      无返回
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void write_word_by_plc(u16 ui_word_address,u16 u16_word_len,u8 *dat,u8 u8_port_num)
{
    u16 u16_i;
    u16 ui_mid;
    
    for(u16_i = 0; u16_i < u16_word_len; u16_i++)
    {
        ui_mid = *dat;
        dat++;
        ui_mid<<=8;
        ui_mid |= *dat;
        dat++;
        
        Rtu_write_singleReg(ui_word_address + u16_i,ui_mid, u8_port_num);
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//        modbus协议，本机做从，此函数分析主机发过来的命令串，并组成返回串。
//        函数开始时都转换成RTU，到函数末尾再根据参数is_modbus_asc转换成相应的ASCII或RTU
//Entry:
//      u8 *puc_rec_buf: 需要分析的串的指针，ASCII模式不包含头和尾巴（即不包含0x3A和0x0D 0x0A）。
//      u8 uc_rec_len：传过来的需要分析串的长度，是个数，不是下标。
//     u8 *puc_send_buf：组成发送串的指针，是将来返回给发送串的全局变量的
//     u8 is_modbus_asc：表征ASCII或RTU模式的变量，0----RTU，1----ASCII
//return:
//     无返回
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rtu_reqframe_anlys_slaver(st_Comm_TypeDef *p)
{
#ifndef MODBUS_MSTER_ONLY //没定义仅主模式
    u16 u16_rec_count,u16_send_count;
    u16 ui_len_limit,u16_word; //把原来的字符型修改为整型，原因是读位的长度限制按照以前的计算方法已经超出255,导致读位时会出错，2008-12-16  LBQ
    u16 ui_address,ui_mid;
    u16 u16_SendCnt,u16_RecCnt;
    u8 *puc_rec_buf,*puc_send_buf;
    u8 uc_is_excep = 0,uc_excep_code = 0, uc_h,uc_l;
    
    puc_rec_buf = p->u8_RecBufP;
    puc_send_buf = p->u8_SendBufP;
    u16_rec_count  = 0;
    u16_send_count = 0;
	
#ifdef MODBUS_TCP
    if(p->uc_status & BIT(en_Comm_Status_is_tmpTCP))
    {
        *(puc_send_buf + u16_send_count++) = *(puc_rec_buf + u16_rec_count++);//来什么回什么
        *(puc_send_buf + u16_send_count++) = *(puc_rec_buf + u16_rec_count++);
        *(puc_send_buf + u16_send_count++) = 0;
        *(puc_send_buf + u16_send_count++) = 0;
        //注意此处空2个
        u16_rec_count  = 6;
        u16_send_count = 6;
    }
#endif //~MODBUS_TCP
    *(puc_send_buf + u16_send_count++) = *(puc_rec_buf + u16_rec_count++);//地址
    *(puc_send_buf + u16_send_count++) = *(puc_rec_buf + u16_rec_count++);//功能码
    
    uc_h = *(puc_rec_buf + u16_rec_count++);//开始地址
    uc_l = *(puc_rec_buf + u16_rec_count++);
    ui_address = ((u16)uc_h<<8) | uc_l;
    
    switch(puc_rec_buf[1])
    {
        
#ifdef MODBUS_COIL
        
        case MB_READ_COIL: //ModbusC_ReadDiscrete  01
        case MB_READ_DISCRETE: //read discrete 02
        {
            if((p->uc_master_slaver_status & BIT(en_Comm_SLAVER_SEND_EN)) == 0){break;}
            uc_h = *(puc_rec_buf + u16_rec_count++);//获得读位的数量
            uc_l = *(puc_rec_buf + u16_rec_count++);
            ui_mid = MAKE_UINT(uc_h,uc_l);
            ui_len_limit = p->uc_is_asc ?(((p->COMM_ARRAY_SendXB - 8)/4)*16):(((p->COMM_ARRAY_SendXB - 6)/2)*16);//SEND_BIT_NUM_MAX_ASC:SEND_BIT_NUM_MAX_RTU
            //异常处理。 
            if((ui_mid == 0) || (ui_mid > ui_len_limit)) //||(ui_mid > 0x07D0)读位的数量不正确只能在(0x0001 - 0x07d0).只返回错误码。V1.1
            {
                uc_is_excep   = 1;
                uc_excep_code = 3;
                break;
            }
            
            ui_mid -= 1;
            uc_h = ui_mid / 8;
            uc_l = ui_mid % 8;
            *(puc_send_buf + u16_send_count++) = uc_h + 1;
            for(ui_mid = 0; ui_mid < uc_h; ui_mid++)
            {
                *(puc_send_buf + u16_send_count++) = rtu_read_bits(ui_address + ui_mid*8,8, p->uc_port_num);
            }
            
            *(puc_send_buf +u16_send_count++) = rtu_read_bits(ui_address + ui_mid*8,uc_l+1, p->uc_port_num);
        }
        break;
        
        case MB_WRITE_SINGLE_COIL: // 05
        {
            *(puc_send_buf + u16_send_count++) = uc_h;//要写bit位地址的高位。
            *(puc_send_buf + u16_send_count++) = uc_l;//要写bit位地址的低位
            
            uc_h = *(puc_rec_buf + u16_rec_count++);//得到位值的高位
            uc_l = *(puc_rec_buf + u16_rec_count++);//得到位值的低位
            ui_mid = (unsigned short)uc_h << 8 | uc_l;
            //异常检查
            if((ui_mid != 0x0000) && (ui_mid != 0xFF00))  //如果要写的位数据不为0x0000或0xff00,生成异常串
            {
                uc_is_excep   = 1;
                uc_excep_code = 3;
                u16_send_count -= 2;//刚才的发送不要了
                break;
            }
            
            *(puc_send_buf + u16_send_count++) = uc_h;//bit位值的高位。0xff or 0x00.
            *(puc_send_buf + u16_send_count++) = uc_l; //bit位值的低位.
            
            uc_RTU_write_bit(ui_address,uc_h, p->uc_port_num); //不需要uc_l,总是为0,uc_h=0xff/00
        }
        break;
        
        case MB_WRITE_MULTI_COIL: //写多个线圈。
        {
            *(puc_send_buf + u16_send_count++) = uc_h;  //bit位开始地址的高位(写回应串)
            *(puc_send_buf + u16_send_count++) = uc_l;  //bit位开始地址的低位。(写回应串)
            
            uc_h = *(puc_rec_buf + u16_rec_count++);     //获得要写的bit位的数量的高位。
            uc_l = *(puc_rec_buf + u16_rec_count++);  //获得要写的bit位的数量的低位
            //uc_bytes = *(puc_rec_buf + u16_rec_count++);    //获取位值的字节数。
            u16_rec_count++;
            u16_RecCnt = (unsigned short)uc_h << 8 | uc_l;
            
            //异常检查,如果要写位的数量< 0x0001 或 要写位的数量 > 0x07D0 或 由位数量计算的字节数!= 接收帧中字节数，则产生异常。
            if((u16_RecCnt < 0x0001) || (u16_RecCnt > 0x07D0))// ||(((u16_RecCnt % 8)?(u16_RecCnt/8 +1):(u16_RecCnt/8)) != uc_bytes)) //写位的数量不正确只能在(0x0001 - 0x07d0).只返回错误码。
            {
                uc_is_excep   = 1;
                uc_excep_code = 3;
                u16_send_count -= 2;
                break;
            }
            
            *(puc_send_buf + u16_send_count++) = uc_h; // 要写的bit位。
            *(puc_send_buf + u16_send_count++) = uc_l;//回应到此为止
            
            //位的个数，1-0x7B0
            for(ui_mid = 0; ui_mid < u16_RecCnt; ui_mid++)
            {
                uc_RTU_write_bit(ui_address + ui_mid,*(puc_rec_buf + u16_rec_count+ui_mid/8) & BIT(ui_mid%8), p->uc_port_num);
            }
        }
        break;
        
#endif //~MODBUS_COIL      
		
#ifndef MODBUS_17_ONLY        
        case MB_READ_HOLD_REG: //read hold single register 03
        case MB_READ_INPUT_REG: // 04 读保持寄存器。
        case MB_READ_BLOCK_REG:
        {
            if((p->uc_master_slaver_status & BIT(en_Comm_SLAVER_SEND_EN)) == 0) {
                break;
            }
            
            uc_h   = *(puc_rec_buf + u16_rec_count++);//获得要读的字数量的高8位
            uc_l   = *(puc_rec_buf + u16_rec_count++);//获得要读的字数量的低8位
            u16_SendCnt = (unsigned short)uc_h << 8 | uc_l;
            
            ui_len_limit = p->uc_is_asc ?((p->COMM_ARRAY_SendXB - 10) >> 2):((p->COMM_ARRAY_SendXB - 4) >> 1);
            //异常处理 
            if((u16_SendCnt == 0) || (u16_SendCnt > ui_len_limit)) // || (ui_mid > 0x007D)读字的数量不正确只能在(0x0001 - 0x007d).只返回错误码。v1.1
            {
                uc_is_excep   = 1;
                uc_excep_code = 3;
                break;
            }
            
            *(puc_send_buf + u16_send_count++) = (u8)(u16_SendCnt << 1); //uc_l<<1; //获取要读的字节数。字的数量*2 v1.1
            
            if(puc_rec_buf[1] == MB_READ_BLOCK_REG)
            {
                //返回串的结构和原来一样，虽然字节数没啥用，但是返回了
                ui_RTU_ret_Block(ui_address,(u16*)(puc_send_buf + u16_send_count),&u16_send_count,u16_SendCnt,p->uc_port_num);
                //u16_send_count 指针传入，出来的时候已经指向下一个了
            }
            else
            {
                beforeReadRegisters(ui_address,u16_SendCnt,p->uc_port_num);//引发读操作之前的事件
                for(ui_mid = 0; ui_mid < u16_SendCnt; ui_mid++) //字数！！！  v1.1
                {
                    u16_word = ui_RTU_ret_Word(ui_address + ui_mid, p->uc_port_num);
                    *(puc_send_buf + u16_send_count++) = (HI_UINT(u16_word));
                    *(puc_send_buf + u16_send_count++) = (LO_UINT(u16_word));
                }                
            }
            afterReadRegisters(ui_address,u16_SendCnt,p->uc_port_num);//完成了读操作事件
        }
        break;
        
        case MB_WRITE_SINGLE_REG:// 06 写单个保持寄存器。
        {
            *(puc_send_buf + u16_send_count++) = uc_h;//address
            *(puc_send_buf + u16_send_count++) = uc_l;
            uc_h = *(puc_rec_buf + u16_rec_count++);//data
            uc_l = *(puc_rec_buf + u16_rec_count++);
            *(puc_send_buf + u16_send_count++) = uc_h;
            *(puc_send_buf + u16_send_count++) = uc_l;
            Rtu_write_singleReg(ui_address,MAKE_UINT(uc_h,uc_l),p->uc_port_num);
            afterWriteRegisters(ui_address,1, p->uc_port_num); //写本地内存完成
        }
        break;
        
        case MB_WRITE_MULTI_REG: //0x10 写多个寄存器
        case MB_WRITE_BLOCK_REG: //0x20 写寄存器块 自定义
        {
            u16_RecCnt = *(puc_rec_buf + u16_rec_count++);  //获得要写的字寄存器的数量的高位。
            u16_RecCnt <<= 8;
            u16_RecCnt |= *(puc_rec_buf + u16_rec_count++);   //获得要写的字寄存器的数量的的低位
            //uc_bytes  = *(puc_rec_buf + u16_rec_count++);  //获取要写的值的字节数。v1.1其实没啥用
            u16_rec_count++; //这是那个字节数，没用
            
            //异常检查,如果要写位的数量< 0x0001 或 要写位的数量 > 0x007B 或 由要写的字数量计算的字节数!= 接收帧中要写的字节数，则产生异常。
            if((u16_RecCnt == 0) || (u16_RecCnt > ((p->COMM_ARRAY_Rec_XB - 8) >> 1))) // || (ui_mid > 0x007B)||(ui_mid * 2 != uc_bytes)
            {
                uc_is_excep   = 1;
                uc_excep_code = 3;
                break;
            }//异常处理结束
            
            *(puc_send_buf + u16_send_count++) = uc_h;  // 开始地址的高位.
            *(puc_send_buf + u16_send_count++) = uc_l;  //开始地址的低位.
            *(puc_send_buf + u16_send_count++) = (u8)(u16_RecCnt >> 8);  //写的字数量的高位。
            *(puc_send_buf + u16_send_count++) = (u8)u16_RecCnt;//返回到此为止
            
            if(puc_rec_buf[1] == MB_WRITE_MULTI_REG)
            {
                for(ui_mid = 0; ui_mid < u16_RecCnt; ui_mid++) //要写的字的数量。 v1.1
                {
                    uc_h = *(puc_rec_buf + u16_rec_count++);//从接收串获得字
                    uc_l = *(puc_rec_buf + u16_rec_count++);
                    Rtu_write_singleReg(ui_address + ui_mid,(uc_h<<8) | uc_l, p->uc_port_num);
                }
            }
            else //MB_WRITE_BLOCK_REG: //0x20 写寄存器块 自定义
            {
                Rtu_write_BlockReg(ui_address,(u16*)(puc_rec_buf + u16_rec_count),u16_RecCnt,p->uc_port_num);                
            }
            afterWriteRegisters(ui_address,u16_RecCnt, p->uc_port_num); //写本地内存完成
        }
        break;
#endif //MODBUS_17_ONLY 
        
        case MB_READ_WRITE_REG:   //0x17 write and read multiple reg,first write and second read
        case MB_R_Reg_W_Block_REG://0x27 wirte bolck read multiple reg
        {
            uc_h     = *(puc_rec_buf + u16_rec_count++);//要读的数量
            uc_l     = *(puc_rec_buf + u16_rec_count++);//要读的数量
            u16_SendCnt= (unsigned short)uc_h << 8 | uc_l;//要读的字数
            //异常检查
            if(u16_SendCnt > (p->uc_is_asc ?((p->COMM_ARRAY_SendXB - 10) >> 2):((p->COMM_ARRAY_SendXB - 4) >> 1)))
            {
                uc_is_excep   = 1;
                uc_excep_code = 3;
                break;
            }
            
            uc_h     = *(puc_rec_buf + u16_rec_count++);
            uc_l     = *(puc_rec_buf + u16_rec_count++);
            ui_len_limit= (uc_h << 8 | uc_l);//要写的开始地址 ，借用ui_len_limit变量
            
            uc_h     = *(puc_rec_buf + u16_rec_count++);
            uc_l     = *(puc_rec_buf + u16_rec_count++);//要写的数量
            u16_RecCnt= (uc_h << 8 | uc_l);//写来的字数
            //uc_bytes = *(puc_rec_buf + u16_rec_count++);//要写的字节数.木用，但是指针要移动
            u16_rec_count++;
            //写操作
            if(puc_rec_buf[1] == MB_READ_WRITE_REG)
            {
                for(ui_mid = 0; ui_mid < u16_RecCnt; ui_mid++) //要写的字的数量。
                {
                    uc_h = *(puc_rec_buf + u16_rec_count++);//从接收串获得字
                    uc_l = *(puc_rec_buf + u16_rec_count++);
                    Rtu_write_singleReg(ui_len_limit + ui_mid,((uc_h<<8)|uc_l),p->uc_port_num);
                }
            }
            else//MB_R_Reg_W_Block_REG
            {
                Rtu_write_BlockReg(ui_len_limit,(u16*)(puc_rec_buf + u16_rec_count),u16_RecCnt,p->uc_port_num);
            }
            afterWriteRegisters(ui_len_limit,u16_RecCnt, p->uc_port_num); //写本地内存完成
            
            *(puc_send_buf + u16_send_count++) = (u8)(u16_SendCnt << 1);
            //读操作
            beforeReadRegisters(ui_address,u16_SendCnt,p->uc_port_num);//引发读操作之前的事件
            for(ui_mid = 0; ui_mid < u16_SendCnt; ui_mid++) //字数
            {
                u16_word = ui_RTU_ret_Word(ui_address + ui_mid, p->uc_port_num);
                *(puc_send_buf + u16_send_count++) = (HI_UINT(u16_word));
                *(puc_send_buf + u16_send_count++) = (LO_UINT(u16_word));
            }
            afterReadRegisters(ui_address,u16_SendCnt,p->uc_port_num);//完成了读操作事件
        }
        break;
        
        default: //出现不支持的功能码,出现异常(01)
        uc_is_excep   = 1;
        uc_excep_code = 1;
        break;
    }
    
    if(p->uc_master_slaver_status & BIT(en_Comm_SLAVER_SEND_EN))
    {
        if(uc_is_excep) //如果有异常
        {
            *(puc_send_buf + --u16_send_count) = *(puc_rec_buf + 1) + 0x80;//功能码加0x80
            u16_send_count++;
            *(puc_send_buf + u16_send_count++) = uc_excep_code;
        }
        
#ifndef MODBUS_RTU_ONLY
        if(p->uc_is_asc)
        {
            uc_h = lrc(puc_send_buf,u16_send_count);//先将lrc的内容提取出来
            
            u16_send_count = convert_rtu_to_ascii(puc_send_buf,u16_send_count,uc_temp_buf,MB_HEAD_ASCII);
            
            *(puc_send_buf + u16_send_count++) = HI_UCHAR_TO_HEX(uc_h);//lrc 高
            *(puc_send_buf + u16_send_count++) = LO_UCHAR_TO_HEX(uc_h);//lrc 低
            *(puc_send_buf + u16_send_count++) = en_PRE_END_MB_ASCII;         //pre end   0x0D
            *(puc_send_buf + u16_send_count)   = en_END_MB_ASCII;             //end       0x0A
        }
        else
#endif
        {
            u16_word = CRC16(puc_send_buf,u16_send_count,(p->uc_master_slaver_status & BIT(en_Comm_TH_CRC)) | (p->uc_ThCRC));//CRC效验，传的是数组的个数
            *(puc_send_buf + u16_send_count++) = (LO_UINT(u16_word));
            *(puc_send_buf + u16_send_count)   = (HI_UINT(u16_word));
#ifndef MODBUS_NoSECRET
            if(p->uc_master_slaver_status & BIT(en_Comm_TH_SECRET))
            {
                uc_l = LO_UINT(u16_word);
                uc_h = HI_UINT(u16_word);
                for(ui_mid = 0; ui_mid <= u16_send_count - 2; ui_mid++)
                {
                    uc_l++;
                    *(puc_send_buf + ui_mid) = *(puc_send_buf + ui_mid) ^ uc_h ^ uc_l;
                }
            }
#endif
        }
        
        p->u16_sendarray_num = u16_send_count; //是下标，不是个数，这是发送中断所确定的
    }
#endif  //~MODBUS_MSTER_ONLY //没定义仅主模式
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//        modbus协议，本机做主，此函数根据OP表（结构数组），生成发送串。
//        函数开始时按照RTU处理，到函数末尾再根据参数is_modbus_asc转换成相应的ASCII或RTU
//Entry:
//	    st_Comm_TypeDef *p  : 通讯变量结构指针
//局部变量
//      struct Master_OP *pMaster_OP: 结构指针，传入的是OP表（结构数组）
//     u8 puc_send_buf：组成发送串的指针，是将来返回给发送串的全局变量的
//return:
//     u16_send_count   返回的是发送串的长度，是下标，不是个数，返回0为不发送
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u16 rtu_modbus_master_send(st_Comm_TypeDef *p)
{
    u16 u16_send_count = 0;
#ifndef MODBUS_SLV_ONLY
    
    u16 ui_mid,ui_mem_begin_addr;
    u16 u16_i,ui_len_limit; //把原来的字符型修改为整型，原因是读位的长度限制按照以前的计算方法已经超出255,导致读位时会出错，2008-12-16  LBQ
    u8 uc_h,uc_l;
    
    u8 *puc_send_buf;
    Master_OP_TypeDef *pMaster_OP;
    
    puc_send_buf = p->u8_SendBufP;
    pMaster_OP = &p->st_Master_OP[p->uc_Master_OP_P];
    
    *(puc_send_buf + u16_send_count++) = p->st_Master_OP[p->uc_Master_OP_P].uc_slaver_client;//p->uc_client;  //address 2010.8.6,
    //从此，允许呼叫多个从机
    *(puc_send_buf + u16_send_count++) = pMaster_OP->uc_cmd; //func code
    ui_mid = pMaster_OP->ui_plc_begin_addr;
    *(puc_send_buf + u16_send_count++) = ui_mid >>8;  //start address
    *(puc_send_buf + u16_send_count++) = (u8)ui_mid;
    
    switch(pMaster_OP->uc_cmd)
    {
#ifdef MODBUS_COIL
        case 0x05: //写单个位
        {
            *(puc_send_buf + u16_send_count++) = rtu_read_bits(pMaster_OP->ui_mem_begin_addr,1,p->uc_port_num) == 0? 0 : 0xff;
            *(puc_send_buf + u16_send_count++) = 0;
        }
        break;
        case 0x0f://写多个位
        {
            uc_h = (pMaster_OP->u16_lenth-1)/8;
            uc_l = (pMaster_OP->u16_lenth-1)%8;
            *(puc_send_buf + u16_send_count++) = 0;
            *(puc_send_buf + u16_send_count++) = pMaster_OP->u16_lenth;
            *(puc_send_buf + u16_send_count++) = uc_h + 1;
            ui_mid = pMaster_OP->ui_mem_begin_addr;
            for(u16_i = 0; u16_i < uc_h; u16_i++)
            {
                *(puc_send_buf + u16_send_count++) = rtu_read_bits( ui_mid+ u16_i * 8,8,p->uc_port_num);
            }
            *(puc_send_buf +u16_send_count++) = rtu_read_bits(ui_mid + u16_i * 8,uc_l+1,p->uc_port_num);
        }
        break;
#endif //~MODBUS_COIL       

#ifndef MODBUS_17_ONLY 
		case 0x01:
        case 0x02:
        case 0x03:
        case 0x04:
        case MB_READ_BLOCK_REG: //0x24
        {
            *(puc_send_buf + u16_send_count++) = (u8)(pMaster_OP->u16_lenth >> 8);
            *(puc_send_buf + u16_send_count++) = (u8)pMaster_OP->u16_lenth;
        }
        break;
		
        case 0x06: //写单个寄存器
        {
            beforeReadRegisters(pMaster_OP->ui_mem_begin_addr,1,p->uc_port_num);//引发读操作之前的事件
            ui_mid = ui_RTU_ret_Word(pMaster_OP->ui_mem_begin_addr, p->uc_port_num);
            *(puc_send_buf + u16_send_count++) = ui_mid >> 8;
            *(puc_send_buf + u16_send_count++) = (u8)ui_mid;
            afterReadRegisters(pMaster_OP->ui_mem_begin_addr,1,p->uc_port_num);//完成了读操作事件
        }
        break;
        
        case MB_WRITE_MULTI_REG: //写多个寄存器
        {
            *(puc_send_buf + u16_send_count++) = (u8)(pMaster_OP->u16_lenth >> 8);
            *(puc_send_buf + u16_send_count++) = (u8)pMaster_OP->u16_lenth;
            *(puc_send_buf + u16_send_count++) = (u8)(pMaster_OP->u16_lenth << 1);
            
            ui_mem_begin_addr = pMaster_OP->ui_mem_begin_addr;
            
            ui_len_limit = p->uc_is_asc ?((p->COMM_ARRAY_SendXB - 18) >> 2):((p->COMM_ARRAY_SendXB - 8) >> 1); //p->uc_is_asc ?((p->COMM_ARRAY_SendXB - 10) >> 2):((p->COMM_ARRAY_SendXB - 4) >> 1)
            
            //异常处理
            if((pMaster_OP->u16_lenth == 0) ||(pMaster_OP->u16_lenth > ui_len_limit)) //读字的数量不正确只能在(0x0001 - 0x007d).只返回错误码。
            {
                pMaster_OP->u16_lenth = 1;
            }
            
            beforeReadRegisters(pMaster_OP->ui_mem_begin_addr,1,p->uc_port_num);//引发读操作之前的事件
            for(u16_i = 0; u16_i < pMaster_OP->u16_lenth; u16_i++)
            {
                ui_mid = ui_RTU_ret_Word( ui_mem_begin_addr + u16_i, p->uc_port_num);
                *(puc_send_buf + u16_send_count++) = ui_mid >> 8;
                *(puc_send_buf + u16_send_count++) = (u8)ui_mid;
            }
            afterReadRegisters(pMaster_OP->ui_mem_begin_addr,1,p->uc_port_num);//完成了读操作事件
        }
        break;
		
#endif //MODBUS_17_ONLY 		
        
        case 0x17: //写读多个寄存器
        {
            ui_mem_begin_addr = pMaster_OP->ui_write_plc_addr;
            
            *(puc_send_buf + u16_send_count++) = (u8)(pMaster_OP->u16_lenth >> 8);  //读的个数
            *(puc_send_buf + u16_send_count++) = (u8)pMaster_OP->u16_lenth;
            
            *(puc_send_buf + u16_send_count++) = (u8)(ui_mem_begin_addr >> 8);   //写的开始地址
            *(puc_send_buf + u16_send_count++) = (u8)ui_mem_begin_addr;
            
            *(puc_send_buf + u16_send_count++) = (u8)(pMaster_OP->u16_write_len >> 8);  //写的数量
            *(puc_send_buf + u16_send_count++) = (u8)pMaster_OP->u16_write_len;
            
            *(puc_send_buf + u16_send_count++) = (u8)(pMaster_OP->u16_write_len << 1);          //写的字节数
            
            ui_mem_begin_addr                  = pMaster_OP->ui_write_mem_addr;
            ui_len_limit                       = pMaster_OP->u16_write_len;  //期待用寄存器变量提高效率
            
            beforeReadRegisters(ui_mem_begin_addr,ui_len_limit,p->uc_port_num);//引发读操作之前的事件
            for(u16_i = 0; u16_i < ui_len_limit; u16_i++)
            {
                ui_mid = ui_RTU_ret_Word(ui_mem_begin_addr + u16_i, p->uc_port_num);
                *(puc_send_buf + u16_send_count++) = (u8)(ui_mid >> 8);
                *(puc_send_buf + u16_send_count++) = (u8)ui_mid;
            }
            afterReadRegisters(ui_mem_begin_addr,ui_len_limit,p->uc_port_num);//完成了读操作事件
        }
        break;
        default:
        break;
    }
    
#ifndef MODBUS_RTU_ONLY
    if(p->uc_is_asc)
    {
        uc_l      = lrc(puc_send_buf,u16_send_count);//先将lrc的内容提取出来
        
        u16_send_count = convert_rtu_to_ascii(puc_send_buf,u16_send_count,uc_temp_buf,en_HEAD_MB_ASCII);
        
        *(puc_send_buf + u16_send_count++) = HI_UCHAR_TO_HEX(uc_l);//lrc 高
        *(puc_send_buf + u16_send_count++) = LO_UCHAR_TO_HEX(uc_l);//lrc 低
        *(puc_send_buf + u16_send_count++) = en_PRE_END_MB_ASCII;         //pre end   0x0D
        *(puc_send_buf + u16_send_count)   = en_END_MB_ASCII;             //end       0x0A
    }
    else
#endif //MODBUS_RTU_ONLY
    {
#ifdef MODBUS_TCP
        if(p->uc_status & BIT(en_Comm_Status_is_tmpTCP))
        {
            u16_send_count--; //个数
            *(puc_send_buf + 4) = HI_UINT(u16_send_count - 6);
            *(puc_send_buf + 5) = LO_UINT(u16_send_count - 6);
#ifndef     MODBUS_NoSECRET
            if(pMaster_OP->uc_status & BIT(en_Comm_TH_SECRET)) {
                doSECRET(puc_send_buf,6,u16_send_count);
            }
#endif      //~MODBUS_NoSECRET
        }
        else
#endif //~MODBUS_TCP
        {
            ui_mid = CRC16(puc_send_buf,u16_send_count,(pMaster_OP->uc_status & BIT(en_CommOP_TH_CRC)) | p->uc_ThCRC);//CRC效验，传的是数组的个数
            *(puc_send_buf + u16_send_count++) = (u8)(ui_mid & 0x00ff);
            *(puc_send_buf + u16_send_count) = (u8)(ui_mid >> 8);
#ifndef MODBUS_NoSECRET
            if(pMaster_OP->uc_status & BIT(en_Comm_TH_SECRET)) {
                doSECRET(puc_send_buf,0,u16_send_count);
            }
#endif //~MODBUS_NoSECRET
        }
    }
    
#endif //~MODBUS_SLV_ONLY
    
    return(u16_send_count);	//返回的是下标，这是发送中断所确定的
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//        modbus协议，本机做主，此函数用来分析（当本机主发了命令串后）从设备的返回串
//        函数开始时都转换成RTU进行解析
//Entry:
//	    st_Comm_TypeDef *p  : 通讯变量结构指针
//局部变量
//      u8 puc_rec_buf:  需要分析的串的指针，ASCII模式不包含头和尾巴（即不包含0x3A和0x0D 0x0A）。见调用前的工作
//      u8 uc_rec_len：传过来的需要分析串的长度，是个数，不是下标。
//return:
//     无返回
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rtu_reqframe_anlys_master(st_Comm_TypeDef *p)
{
    
#ifndef MODBUS_SLV_ONLY
    
    u8 uc_func_code;
    u8 *puc_rec_buf;
    Master_OP_TypeDef *pMaster_OP;
    
    puc_rec_buf = p->u8_RecBufP;
    pMaster_OP = &p->st_Master_OP[p->uc_Master_OP_P];
    
    uc_func_code = pMaster_OP->uc_cmd;
    if(*(puc_rec_buf + 1) != uc_func_code)//功能码不同
    { 
        return;
    }
    
    switch(uc_func_code) //功能码
    {
#ifndef MODBUS_17_ONLY 
        case MB_READ_COIL: //coil
        case MB_READ_DISCRETE:
        {
            write_bit_by_plc(pMaster_OP->ui_mem_begin_addr, pMaster_OP->u16_lenth, puc_rec_buf + 3, p->uc_port_num);
        }
        break;
        
        case MB_READ_HOLD_REG:
        case MB_READ_INPUT_REG:
        {
            write_word_by_plc(pMaster_OP->ui_mem_begin_addr, pMaster_OP->u16_lenth, puc_rec_buf + 3,p->uc_port_num);
            afterWriteRegisters(pMaster_OP->ui_mem_begin_addr,pMaster_OP->u16_lenth, p->uc_port_num); //写本地内存完成
        }
        break;
        
        case MB_READ_BLOCK_REG: //切记，u16_lenth长度不符合commtable中的数组长度,会死机
        {
            Rtu_write_BlockReg(pMaster_OP->ui_mem_begin_addr,(u16*)(puc_rec_buf + 3),pMaster_OP->u16_lenth,p->uc_port_num);
            afterWriteRegisters(pMaster_OP->ui_mem_begin_addr,pMaster_OP->u16_lenth, p->uc_port_num); //写本地内存完成
        }break;
#endif //MODBUS_17_ONLY 
		
        case MB_READ_WRITE_REG:
        {
            if((*(puc_rec_buf + 2) != 0) && ((pMaster_OP->u16_lenth * 2) == (u16)(*(puc_rec_buf + 2))))
            {
                write_word_by_plc(pMaster_OP->ui_mem_begin_addr, pMaster_OP->u16_lenth, puc_rec_buf + 3,p->uc_port_num);
                afterWriteRegisters(pMaster_OP->ui_mem_begin_addr,pMaster_OP->u16_lenth, p->uc_port_num); //写本地内存完成  
            }
        }
        break;
        default:
        {;}
        break;
    }
    
#endif
    
}

