#include "base.h"
#include "step.h"

void SetInnStep(StepType* me,u16 u8_StepName)
{
		if(me->inn.u8_step != u8_StepName)
		{
			me->inn.step100mS = 0;
			me->inn.u8_step = u8_StepName;					
		}		
}

void SetPart(StepType* me,u8 u8_PartName)
{
		if(me->u8_Now != u8_PartName)
		{
			me->u16_NowTime100mS = me->u16_NowTime10mS = 0;
			me->u8_Now = u8_PartName;	
            
            me->inn.u8_step = 0;
            me->inn.step100mS = 0;
		}		
}

void SetpTime10mS(StepType* me)
{if(++me->u16_NowTime10mS == 0){me->u16_NowTime10mS = 0xffff;}}

void SetpTime100mS(StepType* me)
{
    if(++me->inn.step100mS == 0){me->inn.step100mS = 0xffff;}
    if(++me->u16_NowTime100mS == 0){me->u16_NowTime100mS = 0xffff;}
}




