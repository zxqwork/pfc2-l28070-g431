#include "Comm.h"

#include "Modbus.h"
#include "comm_YaoHua.h"
#include "comm_YUDIAN.h"
#include "comm_Free4.h"
#include "comm_0203CRLF.h"

#ifdef USE_Protocol_WtznAT
#include "comm_WtznAT2CAN.h"
#endif

#ifdef USE_Protocol_YaoHua_EXT_LED
#include "comm_YaoHuaExtLed.h"
#endif

#include "BSP.h" //用到收发转换和发送函数

//// 2020-12-04  255=群发地址

#ifdef IS_NEED_COMM

st_Comm_TypeDef st_comm[MCU_UART_NUM];
u8 u8_SendBuf0[COMM0_ARRAY_SendXB + 1];u8 u8_RecBuf0[COMM0_ARRAY_Rec_XB + 1];
Master_OP_TypeDef st_Master0_OP[OP_TABLE_LENTH_0];

#if MCU_UART_NUM >= 2
u8 u8_SendBuf1[COMM1_ARRAY_SendXB + 1];u8 u8_RecBuf1[COMM1_ARRAY_Rec_XB + 1];
Master_OP_TypeDef st_Master1_OP[OP_TABLE_LENTH_1];
#endif
#if MCU_UART_NUM >= 3
u8 u8_SendBuf2[COMM2_ARRAY_SendXB + 1];u8 u8_RecBuf2[COMM2_ARRAY_Rec_XB + 1];
Master_OP_TypeDef st_Master2_OP[OP_TABLE_LENTH_2];
#endif
#if MCU_UART_NUM >= 4
u8 u8_SendBuf3[COMM3_ARRAY_SendXB + 1];u8 u8_RecBuf3[COMM3_ARRAY_Rec_XB + 1];
Master_OP_TypeDef st_Master3_OP[OP_TABLE_LENTH_3];
#endif
#if MCU_UART_NUM >= 5
u8 u8_SendBuf4[COMM4_ARRAY_SendXB + 1];u8 u8_RecBuf4[COMM4_ARRAY_Rec_XB + 1];
Master_OP_TypeDef st_Master4_OP[OP_TABLE_LENTH_4];
#endif


__flash u16  code_bt[7]={48,96,192,384,576,1152, 1875};
__flash u16 u16_timeout_4byte_ms[7]={8,5,3,2,2,2,2};//each 1ms-- 加长了就成下面这样了，怕有些设备有问题
//__flash u16 u16_timeout_4byte_ms[7]={10,6,5,3,3,3,3};//each 1ms--

////////////////////////////////////////////////////////////////////
// 通讯处理函数
// 传入通讯数组结构指针
// 实际上，整个函数都以1ms的节拍工作
////////////////////////////////////////////////////////////////////
void about_comm(st_Comm_TypeDef *p,u16 u16_Now_ms)
{
    u8 u8_is_right = 0;
	u8 u8_is_NeedAnlys;
	
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //										判断结 束
    //
    //统统由时间决定
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //点灯
    //做从时，发送允许，则什么都对的话，会应答, 则灯亮
    if(p->uc_master_slaver_status & BIT(en_Comm_SLAVER_SEND_EN))
    {
        if(p->uc_status & BIT(en_Comm_Status_is_sending))
        {
            p->uc_comm_led_1ms_count = 10;
            DRV_Comm_Led(TRUE,p);
            
        }//LED_COMM 亮
        else
        {
            if(p->uc_comm_led_1ms_count){p->uc_comm_led_1ms_count--;}
            else{DRV_Comm_Led(FALSE,p);}
        }
    }
    else    //其他的情况：做主->接收到正确回应； 做从->禁止响应，监听：不许响应，都是收到正确串则亮
    {
        if(p->uc_status & BIT(en_Comm_Status_is_need_led_tip))
        {
            p->uc_status &=~ BIT(en_Comm_Status_is_need_led_tip);
            p->uc_comm_led_1ms_count = 10; //因为可能是50ms发送间隔
            DRV_Comm_Led(TRUE,p);
            
        }//LED_COMM 亮
        else
        {
            if(p->uc_comm_led_1ms_count){p->uc_comm_led_1ms_count--;}
            else{DRV_Comm_Led(FALSE,p);}
        }
    }
    //点灯 完毕		
    
    if(p->uc_rec_timeout > 0)
    {
        _CLI();  //消息处理，必须关闭中断，否则在中断中赋值无效
        if(--p->uc_rec_timeout == 0)
        {
            u8_is_right = TRUE;
        }
        _SEI();
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //									有完整的接收串，如何解析？
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    if(u8_is_right) //receive new string. 时间够了
    {
        u8_is_right = FALSE;
        p->uc_status &=~ BIT(en_Comm_Status_is_need_led_tip);
        
        //加速了，收到正确的以后，立即发下一个ＯＰ，因为接收已经延迟过了
        if((p->uc_master_slaver_status & BIT(en_Comm_DisableSpeedUp)) == 0)
        {p->u16_master_lastSend_time = u16_Now_ms - p->u16_master_send_interval + 5;}			
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //分析
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        
        if(p->uc_master_slaver_status & BIT(en_Comm_MASTER_REC_EN))//为主,地址还是刚才自己呼叫的从机
        {       
            switch(p->st_Master_OP[p->uc_Master_OP_P].u8_op_protocol)
            {
                case en_Comm_protocol_Modbus:
                {
#ifndef MODBUS_NoSECRET //注意和下面从模式的不一样，这个是判断OP表的加密状态
                    if(p->st_Master_OP[p->uc_Master_OP_P].uc_status & BIT(en_CommOP_TH_SECRET)){unSECRET(p->u8_RecBufP,0,p->u16_rec_p);}   //还原    
#endif  //~MODBUS_NoSECRET
                    //因为在check_is_right里面删除了TCP的处理部分，所以，TCP接收串的还原，应该在此处做，现在没做
                    if(p->u8_RecBufP[0] == p->st_Master_OP[p->uc_Master_OP_P].uc_slaver_client)//是自己刚才呼叫的地址
                    {
                        u8_is_right = u8_modbus_check_is_right(p); 
                        if(u8_is_right == 0){break;}//校验错，后面不用做了
#ifdef MODBUS_TCP
                        u8 byteLenPos,TcpHeadLen;
                        if(p->uc_status & BIT(en_Comm_Status_is_tmpTCP)){
                            byteLenPos = 8;
                            TcpHeadLen = 6;
                        }
                        else{
                            byteLenPos = 2; //普通modbus表示此串
                            TcpHeadLen = 0;
                        }
                        u8_is_NeedAnlys = 0;
                        if(p->st_Master_OP[p->uc_Master_OP_P].uc_cmd  == 1)
                        {u8_is_NeedAnlys = 1;}//                                                 
                        if(((p->st_Master_OP[p->uc_Master_OP_P].uc_cmd == 3) || (p->st_Master_OP[p->uc_Master_OP_P].uc_cmd == 0x17))
                           && (*(p->u8_RecBufP + byteLenPos) == (u8)(p->u16_rec_p - (5 + TcpHeadLen))))//串中字节长度 == 串总长-地址-命令-2crc-自己  
                        {u8_is_NeedAnlys = 1;}
#else
                        u8_is_NeedAnlys = 0;
                        if(p->st_Master_OP[p->uc_Master_OP_P].uc_cmd  == 1)
                        {u8_is_NeedAnlys = 1;}//                                                 
                        else if(((p->st_Master_OP[p->uc_Master_OP_P].uc_cmd == 3) || (p->st_Master_OP[p->uc_Master_OP_P].uc_cmd == 0x17)  
                                 || (p->st_Master_OP[p->uc_Master_OP_P].uc_cmd == MB_READ_BLOCK_REG) || (p->st_Master_OP[p->uc_Master_OP_P].uc_cmd == 4))
                                && (*(p->u8_RecBufP + 2) == (u8)(p->u16_rec_p - 5)))//串中字节长度 == 串总长-地址-命令-2crc-自己  230316
                        {u8_is_NeedAnlys = 1;}
#endif
                        
                        ////需要分析，来的数，是上次呼叫的命令
                        if(u8_is_NeedAnlys)
                        {
                            rtu_reqframe_anlys_master(p);
                            p->st_Master_OP[p->uc_Master_OP_P].uc_status |= BIT(en_CommOP_MasterGetRec);//做主的时候，op收到了正确的接收串，并处理了
                        }	
                    }
                }break;
                
#ifdef USE_Protocol_YUDIAN_AiBus
                case en_Comm_protocol_YUDIAN_AiBus:					
                {
                    u8_is_right = u8_YUDIAN_AiBus_ask_ans_check_is_right(p);
                    if(u8_is_right){YUDIAN_AiBus_anlys_master(p);}						
                }break;
#endif
                
#ifdef USE_Protocol_FREECOMM4   //暂时不考虑当从
                case en_Comm_protocol_FREE_4CMD:					
                {
                    u8_is_right = u8_FreeComm4_is_check_is_right(p);
                    if(u8_is_right){FreeComm4_reqframe_anlys_master(p);}						
                }break;
#endif                     
                
#ifdef USE_Protocol_0203CRLF   //暂时不考虑当从
                case en_Comm_protocol_0203CRLF:					
                {
                    u8_is_right = u8_Comm0203CRLF_is_check_is_right(p);
                    if(u8_is_right){Comm0203CRLF_reqframe_anlys_master(p);}						
                }break;
#endif 
                
#ifdef USE_Protocol_WtznAT   //暂时不考虑当从
                case en_Comm_protocol_WtznAT:					
                {
                    u8_is_right = u8_CommWtznAT2CAN_is_check_is_right(p);
                    CommWtznAT2CAN_reqframe_anlys_master(u8_is_right,p);
                }break;
#endif                    
                default:{;}break;
            }//sw
            
            if(u8_is_right) //校验成功 呼叫地址不可以是0
            {
                p->uc_status |= BIT(en_Comm_Status_is_need_led_tip);  //接收串对了，也是自己刚才呼叫的从机的应答.点灯用
                p->st_Master_OP[p->uc_Master_OP_P].uc_Master_RecErrCnt = 0;//当主的时候，呼叫别人，接受响应丢失或错误的计数器
            }
        }
        else if(p->uc_master_slaver_status & BIT(en_Comm_SLAVER_REC_EN))//为从,地址还是呼叫本机
        {	
            p->u16_sendarray_num = 0;//不经历下面的校验和计算，不会发送
            
            switch(p->en_protocol)
            {
                case en_Comm_protocol_Modbus:
                {
#ifndef MODBUS_NoSECRET
                    if(p->uc_master_slaver_status & BIT(en_Comm_TH_SECRET)){unSECRET(p->u8_RecBufP,0,p->u16_rec_p);}//还原              
#endif  //~MODBUS_NoSECRET  
                    //因为在check_is_right里面删除了TCP的处理部分，所以，TCP接收串的还原，应该在此处做，现在没做
                    if((p->u8_RecBufP[0] == p->uc_client) || (p->u8_RecBufP[0] == 255)) //群发地址
                    {
                        u8_is_right = u8_modbus_check_is_right(p);
                        
                        if(u8_is_right) 
                        {
                            p->uc_status |= BIT(en_Comm_Status_is_need_led_tip);  //接收串对了，也是呼叫自己的。点灯用
                            rtu_reqframe_anlys_slaver(p);
                        }
                        else
                        {goto EndOfRec;}
                    }
                    else
                    {goto EndOfRec;}
                }break;
#ifdef USE_Protocol_YaoHua_running
                case en_Comm_protocol_YaoHua_running:
                {
                    YaoHua_running_reqframe_anlys_slaver(p);
                }break;
#endif
                
#ifdef USE_Protocol_YaoHua_ask_ans
                case en_Comm_protocol_YaoHua_ask_ans:
                {
                    p->u16_sendarray_num = YaoHua_ask_ans_reqframe_anlys_slaver(p);
                }break;
#endif
                
#ifdef USE_Protocol_YaoHua_EXT_LED
                case en_Comm_protocol_YUDIAN_EXT_LED:
                {
                    if(p->u16_rec_p == 3)
                    {p->u16_sendarray_num = YaoHua_ExtLed_reqframe_anlys_slaver(p);}
                    
                }break;
#endif					
                
                default:{;}break;
            }// sw
            
            if((p->u16_sendarray_num != 0) && (p->uc_master_slaver_status & BIT(en_Comm_SLAVER_SEND_EN)))
            {
                p->uc_status |= BIT(en_Comm_Status_is_need_send); 						
            }
        }//~SLAVER_REC_EN
        EndOfRec:
        p->u16_rec_p = 0;//20211220 为下次接收做准备，只要是时间够，不论串是否正确，处理过后，则开启下一次，避免DMA模式覆盖数组
        p->uc_status |= BIT(en_Comm_Status_is_RecFinished);//接受串处理完毕，可以重启DMA了 . DMA接收rst下一个轮询才生效
        
        
    }//(uc_check_is_right && (uc_rec_address == p->uc_client))		
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //												主发
    //
    //先判断时间消息，是否该发送了，然后根据协议调用发送函数
    //  
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    if(p->uc_master_slaver_status & BIT(en_Comm_MASTER_SEND_EN))
    {
        if((u16)(u16_Now_ms - p->u16_master_lastSend_time) >= p->u16_master_send_interval)//实际间隔  目标间隔
        {                
            //if((p->uc_status & BIT(en_Comm_Status_is_sending)) == 0)
            p->uc_status &=~ BIT(en_Comm_Status_is_sending); //DMA方式有可能发生错误，没有清除is_sending，20211220 到时间强制发送
            
            if(++(p->uc_Master_OP_P) >= p->uc_Master_OP_Lenth)
            {
                p->uc_Master_OP_P = 0;
                if(--p->u8_MasterFreqCNT == 0){p->u8_MasterFreqCNT = 250;}
            }
            
            re_load_Master_OP:
            //这条ＯＰ被禁止了，或者通讯1-10大轮回中，整除u8_MasterFreq不是0
            if(((p->st_Master_OP[p->uc_Master_OP_P].uc_status & BIT(en_CommOP_MASTER_SEND_EN)) == 0) || ((p->u8_MasterFreqCNT % p->st_Master_OP[p->uc_Master_OP_P].u8_MasterFreq) != 0))
            {
                if((++p->uc_Master_OP_P) == p->uc_Master_OP_Lenth){return;}
                goto re_load_Master_OP;
            }
            
            p->u16_master_lastSend_time = u16_Now_ms; //20211220,防止最后一条OP是不发的，导致不连续发送，并没有节省时间片
            
            ///////////////////////////
            ///////////////////////////
            /*
            //根据不同的OP，修改停止位和奇偶校验位
            //根据OP修改串口，奇偶校验和停止位
            if(p->st_Master_OP[p->uc_Master_OP_P].u8_StopBit)//某OP的stop不是0，则说明可能要改设定
            {
            if(p->st_Master_OP[p->uc_Master_OP_P].u8_StopBit != p->u8_StopBit)//和现有的端口停止位不一样
            {
            p->u8_StopBit = p->st_Master_OP[p->uc_Master_OP_P].u8_StopBit;
            Chg_CommPortStopBit(p->uc_port_num,p->u8_StopBit);
        }
            if(p->st_Master_OP[p->uc_Master_OP_P].u8_Parity != p->u8_Parity)//和现有的端口停止位不一样
            {
            p->u8_Parity = p->st_Master_OP[p->uc_Master_OP_P].u8_Parity;
            Chg_CommPortParity(p->uc_port_num,p->u8_Parity);
        }
        }
            */
            ///////////////////////////
            ///////////////////////////
            
            switch(p->st_Master_OP[p->uc_Master_OP_P].u8_op_protocol)//(p->en_protocol) 当主发送到时候，按op的协议发送
            {
#ifdef USE_Protocol_Modbus
                case en_Comm_protocol_Modbus:
                {
                    p->u16_sendarray_num  = rtu_modbus_master_send(p);
                    if((p->uc_master_slaver_status & BIT(en_Comm_MASTER_REC_EN)) == 0)
                    {
                        p->uc_status |= BIT(en_Comm_Status_is_need_led_tip);  //MODBUS如果禁止主收，则也没有应答，所以发送时点灯
                    }
                }break;
#endif
                
#ifdef USE_Protocol_YaoHua_running
                case en_Comm_protocol_YaoHua_running:
                {
                    p->u16_sendarray_num  = YaoHua_Running_Send_Master(p);
                    p->uc_status |= BIT(en_Comm_Status_is_need_led_tip);  //耀华连续发送，没有应答，所以发送时点灯
                }break;
#endif
                
#ifdef USE_Protocol_YUDIAN_AiBus
                case en_Comm_protocol_YUDIAN_AiBus:					
                {
                    p->u16_sendarray_num  = YUDIAN_AiBus_Master_Send(p);
                }break;
#endif      
                
#ifdef USE_Protocol_FREECOMM4   //暂时不考虑当从
                case en_Comm_protocol_FREE_4CMD:					
                {
                    p->u16_sendarray_num  = FreeComm4_master_send(p);
                }break;
#endif 
                
#ifdef USE_Protocol_0203CRLF  
                case en_Comm_protocol_0203CRLF:					
                {
                    p->u16_sendarray_num  = Comm0203CRLF_master_send(p);
                }break;
#endif 
                
#ifdef USE_Protocol_WtznAT   //暂时不考虑当从
                case en_Comm_protocol_WtznAT:					
                {
                    p->u16_sendarray_num  = CommWtznAT2CAN_master_send(p);
                }break;
#endif  
                
                default:{;}break;
                
            }//sw
            
            
            p->uc_status |= BIT(en_Comm_Status_is_need_send);
            p->st_Master_OP[p->uc_Master_OP_P].uc_Master_RecErrCnt++; //当主的时候，呼叫别人，接受响应丢失或错误的计数
            
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //											      发送
    //
    //先判断消息，是否该发送了，然后调用BSP函数
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    if(p->uc_status & BIT(en_Comm_Status_is_need_send))
    {
        p->uc_status &=~ BIT(en_Comm_Status_is_need_send);
        BSP_Comm_485_ForceSend( 1, p); //内部修改了
        
        BSP_Comm_Uart_Send(p->u8_SendBufP,p->u16_sendarray_num + 1,p->uc_port_num); //函数：长度
        p->u16_send_p = 1;
        
        if(p->uc_master_slaver_status & BIT(en_Comm_SLAVER_SEND_EN)){p->uc_comm_led_1ms_count = 10;DRV_Comm_Led(TRUE,p);}
        
    } 
}


//******************************************************************
// 中断接收函数
// 传入通讯数组结构指针
// 判断头，并标志字节间隔
//******************************************************************
void uart_rx(st_Comm_TypeDef *st_comm_p,u8 uc_now_UDR)
{
    st_comm_p->u8_RecBufP[st_comm_p->u16_rec_p] = uc_now_UDR;
	
    if(st_comm_p->u16_rec_p < st_comm_p->COMM_ARRAY_Rec_XB){st_comm_p->u16_rec_p++;}
	
    st_comm_p->uc_rec_timeout = st_comm_p->uc_timeout_4bytes;
}

//******************************************************************
// 中断发送函数
// 传入通讯数组结构指针
//
//character has been transmitted，最后=u16_sendarray_num+1
//大于u16_sendarray_num后，所以不会无限制地加
//******************************************************************
u8 uart_tx_not_over(st_Comm_TypeDef *st_comm_p,u8 *uc_be_send)
{
    //uc_send_status[1]|=BIT(is_sending);在发送启动的时候做
	
    if (st_comm_p->u16_send_p <= st_comm_p->u16_sendarray_num)
    {
        *uc_be_send = st_comm_p->u8_SendBufP[st_comm_p->u16_send_p];
        st_comm_p->u16_send_p++;
        return(TRUE);
    }
    else
    {
        st_comm_p->uc_status &= ~BIT(en_Comm_Status_is_sending);//中断模式，完成后清除发送标志
        BSP_Comm_485_ForceSend(0, st_comm_p);//中断模式，完成后直接硬件收发转换 2021-04-23
		
        return(FALSE);
        //uart_send_disable1; //在ABOUTCOM函数中做
    }
}

#endif
//#ifdef IS_NEED_COMM
