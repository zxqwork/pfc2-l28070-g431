#include "base.h"
#include "crc_lrc.h"
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//          16位的CRC校验
//Entry:
//      u8 *puc_buf : 指向数组的指针。
//      u8 uc_bytes : 数组的长度。
//Return:
//      返回一个16位的CRC值
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u16 CRC16(u8 *puc_buf,u16 u16_bytes,u16 u16_type)
{
    u16  ui_index;            //will index into CRC lookup table
    u8  u8_H,u8_L,i;
    u16 reg_crc = 0xffff;

    //u8_H = *(puc_buf + u16_bytes - 1) + 0x11;
    //u8_L = *(puc_buf + u16_bytes - 2) + 0x33;
	if(u16_type & BIT(5))//为了兼容，只要bit5有，则是要特殊crc
    {
      u8_H = *(puc_buf + u16_bytes - 1) + 0x11 + (u8)u16_type - BIT(5);//如果没加thCRC特殊字，
      u8_L = *(puc_buf + u16_bytes - 2) + 0x33 + (u8)u16_type - BIT(5);//仍和原来一样
    }
    
    while (u16_bytes--) /* pass through message buffer */
    {
        reg_crc ^= *puc_buf++;
        for(i = 0;i < 8;i++)
        {
            if(reg_crc & 0x01)
            {
                reg_crc = (reg_crc >> 1) ^ 0xA001;
            }
            else
            {
                reg_crc >>= 1;
            }
        }
    }
    
    if(u16_type)
    {
        ui_index = (u8_H << 8 | u8_L);    
        ui_index ^= (reg_crc);
        return (ui_index);
    }
    else
    {
        return (reg_crc);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Purpose:
//         lrc校验，用于ascii模式.算法为将除开始(':')和结束(CR,lf)的数累加. 丢弃进位.返回一个8位的数。将这个8位数转为16进制的字符。
//         这个函数假设缓冲中不包含':',CR和LF.注意:缓冲为rtu模式下的二进制数而不是16进制的字符
//Entry:
//      u8 *puc_buf : 指向数组的指针。注意：为rtu模式下的二进制数，不是ASCII模式下的数
//      u8 uc_bytes : 数组的长度。
//Return:
//      返回一个8位的lrc值，注意：返回的数据还是rtu模式的数据，不是ASCII模式的数据
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u8 lrc(u8 *puc_buf,u16 u16_bytes)
{
    return (0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DS18B20的CRC8校验程序

u8 CRC8_Byte(u8 u8_data)
{
	u8 i, crc_1byte;
	crc_1byte = 0;
	for(i = 0;i < 8;i++)
	{
		if((crc_1byte ^ u8_data) & 0x01)
		{
			crc_1byte ^= 0x18;
			crc_1byte >>= 1;
			crc_1byte |= 0x80;
		}
		else
		{
			crc_1byte >>= 1;
		}
		u8_data >>= 1;
	}
	return crc_1byte;
}

u8 CalCRC8(u8 *p,u8 u8_len)
{
	u8 u8_CRC = 0;
	while(u8_len --)
	{
		u8_CRC = CRC8_Byte(u8_CRC ^ *p++);
	}
	return u8_CRC;
}