
#include "main.h"
#include "BSP.h"
#include "Interrupt200Hz.h"
#include "InterruptADC.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////// void VariableInit(void) 所有变量初始化
void VariableInit(void)
{
    SADC.ImosP = SADC.ImosN = 0;//MOS电流变量Q15
    SADC.ImosOffset = 0;//MOS电流偏置变量Q15
    SADC.ImosK = 0;//DCM模式下电流采样矫正系数
    SADC.VacL = 0;//输入电压L变量 Q15，
    SADC.VacN = 0;//输入电压N变量 Q15，
    SADC.Vac = 0;//输入电压变量 Q15
    SADC.Vbus = 0;//母线电压变量 Q15，
    
    PLL.Polar = 0;//Vac极性,电压负半边向正半边相互转变标志位
    PLL.PosCnt = 0;//正半周期输入电压计数@每个开关周期累计
    PLL.NegCnt = 0;//负半周期输入电压计数@每个开关周期累计
    PLL.Step = 6453;//锁相环步长Q12   65khz，每10ms650个点，表/点 1024/650=1.5753846*Q12=6453
    PLL.StepSum = 0;//锁相环输出的step叠加,实际上是锁相环位置量乘以Q12
    
    PLL.Cnt = 0;//当前锁相环输出的正弦表位置
    PLL.CntFianl = 0;//最终当前锁相环输出的正弦表位置
    
    Vac.Sum = 0;//电压累加，用以计算有效值用
    Vac.Peak = 0;//峰值电压
    Vac.Rms = 0;//输入电压有效值
    Vac.Abs = 0;//输入电压绝对值变量 Q15--对应馒头波
    Vac.Sine = 0;//构建的正弦电压
    Vac.u12Rmsover2 = 0;//输入电压有效值倒数的平方
    Vac.isDC = 0;
    
    Iac.Sum = 0;//电流累加，用以计算有效值用
    Iac.Rms = 0;//输入电压有效值
    Iac.Ref = 0;//电流参考值
    Iac.RefssK = 0;//参考电流软起动系数
    
    Vbus.Avg = 0;//平均值
    Vbus.Ref = 0;//电压参考值
    
    Feed.ccm = 0;//CCM模式前馈占空比
    Feed.dcm = 0;//DCM模式前馈占空比
    Feed.out = 0;//最终前馈输出
    Feed.dcmK = 0;//系数
    Feed.ssK = 0;//软起动系数，启动时，该值慢慢增加
    
    ILoop.out = 0;//电流环输出
    //ILoop.Err = 0;//电流环误差
    ILoop.kp = 0;
    ILoop.ki = 0;
    ILoop.Inte = 0;//电流环积分
    
    Vloop.out = 0;//电压环输出
    Vloop.Err = 0;//电压环误差
    Vloop.u10_kp = 0;
    Vloop.u10_ki = 0;
    Vloop.Inte = 0;//电流环积分
    
    Flag.RmsCalReady = 0;//有效值计算标志位
    Flag.ssFinsh = 0;//软起结束标志位
    Flag.PWM = 0;//PWM开启关断标志位
    Flag.LowP = 0;//低功率
    Flag.PFCState = 0;//PFC状态机量
    Flag.Err = 0;//故障标志位
}
////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////// StateM 状态机函数
void StateM(void)
{
    static unsigned short su16_cnt5ms = 25;
    static unsigned char CapChargedCnt = 0;
    static long VbusAvgPre = 0;
    //判断状态类型
    switch(Flag.PFCState)
    {
        case  Init :    //// void ValInit(void)相关参数初始化函数
        {
            CapChargedCnt = 0;
            VbusAvgPre = 0;
            SS_Disable();
            Flag.PFCState++;
        }
        case  Wait :    //// void StateMWait(void)等待状态机
        {
            Flag.ssFinsh = 0;//清软启动标志位,准备软启
            Flag.PWM = 0; //PWM关机状态
            Vbus.Ref = 0; //初始化电压参考值
            PFCokDisable();  //告知后级DC-DC,PFC并未准备好
            
            //当检测到BUS电压变化不大后，说明母线电容已充电至满，两次检测电压相差不超过10V ，如果挂了负载，
            if((Vbus.Avg < 300) || ((Vbus.Avg > (VbusAvgPre + 10)) || (Vbus.Avg < (VbusAvgPre - 10))))
            {CapChargedCnt = 0;}        //BUS电容自然整流充电保持计时器清0
            else
            {
                if(++CapChargedCnt > 100)//保持500MS未快速增长，可认为BUS电容已接近充满电
                {
                    CapChargedCnt = 0; //BUS电容自然整流充电保持计时器清0
                    RelayOn(); //继电器吸合
                    SS_Enable();
                    Flag.PFCState  = Rise; //状态机跳转至等待软启状态 
                }
            }
            //BUS电压前值 = 当前值
            VbusAvgPre = Vbus.Avg;
            Flag.PFC2LCC = PFC_ST_WAIT; //待机
        }
        break; //等待状态
        
        case  Rise :        //// void StateMRise(void) 软启阶段 对于28070，这个阶段不取决于CPU
        {
            if(Vbus.Avg > 600)
            {
                Flag.PFCState = Run;//状态机进入正常运行状态 
                PFCokEnable();  //PFC ok
                Flag.ssFinsh = 1;  //软启标志位置位
            }
            
            if(--su16_cnt5ms == 0){su16_cnt5ms = 5;    PFCokEnable();} //快闪软起
            
            Flag.PFC2LCC = PFC_ST_WAIT; //待机
        } 
        break;  //软启动状态
        
        case  Run :             //// void StateMRun(void) :正常运行（空），主函数进程在中断中运行
        {
            if(--su16_cnt5ms == 0)
            {
                if(Flag.LowP){su16_cnt5ms = 15;}
                else         {su16_cnt5ms = 100;} //低压功率：稍快闪烁  ，正常慢速闪烁
                PFCokEnable();
            }    
            Flag.PFC2LCC = PFC_ST_RUNING; //正常
        } 
        break; //运行状态
        
        case  Err :             //// void StateMErr(void) 故障状态
        {   
            Flag.ssFinsh = 0;//软启动标志位清0   
            PFCokDisable(); //PFC 不OK
            LED0_GPIO_Port->ODR ^= LED0_Pin; //故障指示灯，降低亮度
            SS_Disable();
            
            if(Flag.Err & ERR_AC_LOW)           {Flag.PFC2LCC = PFC_ST_AC_LO;}
            else if(Flag.Err & ERR_TPP)     {Flag.PFC2LCC = PFC_ST_TPP;} 
            else if(Flag.Err & ERR_OCP)     {Flag.PFC2LCC = PFC_ST_OCP;}
            else if(Flag.Err & ERR_UVP)     {Flag.PFC2LCC = PFC_ST_UVP;}
            
            
            if((Flag.Err & 0x07) == 0)//若所有故障已恢复，且等待大于1S
            {
                Flag.PFCState = Wait;   //跳转至空闲等待状态,重新启动
                //清空欠压保护标志位,保护后BUS电压降得很低，一般会发生bus 欠压保护
                if(Flag.Err & ERR_UVP){Flag.Err &=~ ERR_UVP;}
                LED0_GPIO_Port->BSRR = LED0_Pin; //故障指示灯
            }
            
        }break; //故障状态       
    }
}
////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////// void ErrCheck(void) 输入电压检测与保护
//COME FROM: 2L3kw-code.XMCD
#define MIN_AC_RMS      27629  //250//AC欠压保护恢复点电压  AC ADC = 593V           
#define MIN_AC_RMS_RE   36480  //330//AC欠压保护恢复点电压
#define MAX_AC_RMS      46420 //420v V.acOvpDsp//AC过压保护动作点电压   23210
#define MAX_AC_RMS_RE   44762 //405v V.acOvpReDsp//AC过压保护恢复点电压 22381
void ErrCheck(void)
{
    //电压有效值小于AC欠压保护值时，为AC欠压保护 ，
    static u16 su16_AcLowCnt = 0;
    if ((Vac.Rms < 250) && (Flag.PFCState != Init)) //掉电1秒
    {
        if(++su16_AcLowCnt > 200) //5ms*200 = 1秒
        {
            su16_AcLowCnt = 200;
            Flag.Err |= ERR_AC_LOW; //欠压故障标志位置位
            Flag.PFCState = Err;   //进入故障保护状态，
            RelayOff(); //掉电，需要脱开继电器了
        }
    }
    else if(su16_AcLowCnt){su16_AcLowCnt--;}
    
    //但发生AC欠压或者AC过压保护时
    static u16 su16_AcLowReCnt = 0;
    if(Flag.Err & (ERR_AC_LOW))
    {
        if ( (Vac.Rms > 300) )
        {//AC电压均大于欠压恢复值且小于过压恢复值时，表明AC正常电压正常。
            if(++su16_AcLowReCnt > 10)
            {Flag.Err &=~ ERR_AC_LOW;}   //清除AC欠压标志位
        }
        else{su16_AcLowReCnt =  0;}
    }
    
    //电压有效值大于AC过压保护值时，为AC过压保护
    if ((Vbus.Temp3950 > 80) && ( Flag.PFCState != Init) )
    {
        Flag.Err |= ERR_TPP;//过压故障标志位置位
        Flag.PFCState = Err; //进入故障保护状态，    
    }
    
    if(Flag.Err & ERR_TPP)
    {
        if(Vbus.Temp3950 < 50){Flag.Err &=~ ERR_TPP;}   //清除
    }
    
}
