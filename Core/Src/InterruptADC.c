/*****************************************************************************
*
* @author  zxqwork
*
* @file : InterruptADC.c
* @brief: ADC中断函数
readwrite  object InterruptADC.o,  readwrite  object Interrupt1kHz.o

* @date 08-30-2023
******************************************************************************/
#include "main.h"
#include "base.h"
#include "BSP.h"
#include "InterruptADC.h"


struct  _ADI    SADC= {0};//采样变量结构体
struct _PLL     PLL = {0};// 
struct _VAC     Vac = {0};//输入电压结构体
struct _IAC     Iac = {0};//输入电流结构体
struct _VBUS    Vbus = {0};//BUS电压结构体
struct _FEED    Feed = {0};//前馈结构体
struct _ILOOP   ILoop = {0};//电流环结构图
struct _VLOOP   Vloop = {0};//电压环结构体
struct _FLAG Flag = {0,0,0,0,0,0,0};//控制标志位结构体

