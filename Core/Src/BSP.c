//#include "lib_includes.h"
#include "BSP.h"
#include "main.h" //hal生成的定义
/******************************************************************************
Jtag SWJ 禁用 第7.3.4节(JTAG/SWD alternate function remapping)，
下面第一个语句可以释放PB3、PB4和PA15做普通I/O口，
下面的第二个语句可以进一步释放PA13和PA14做普通I/O口；
GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE);
GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable,ENABLE);
如果只需要使用PB3做普通I/O口，只使用第一个语句即可，第二个语句多余了。

若系统不需要JTAG，将PB3当作GPIO，需要如下设置：
RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOB , ENABLE);
GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable,ENABLE);

第一句中的 RCC_APB2Periph_AFIO 设置 AFIO EN（如果没有这一句，后面两句失效）。
如果一个模块的clock DISABLE，那么，寄存器的值也无法读写。
********************************************************************************/


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
void DacDebug(long para,long ch)
{
    //if(para > 4095){para = 4095;}
    if(ch)
    {HAL_DAC_SetValue(&hdac1,DAC_CHANNEL_2,DAC_ALIGN_12B_R, para); }
    else
    {HAL_DAC_SetValue(&hdac1,DAC_CHANNEL_1,DAC_ALIGN_12B_R, para); }    
}
/////////////////////////////////////////////////////////////////////////////
//板载支持包：通讯发送
// https://blog.csdn.net/geek_monkey/article/details/89165040
// https://blog.csdn.net/lcx1837/article/details/119756074

/////////////////////////////////////////////////////////////////////////////
void BSP_Comm_Uart_Send(unsigned char *uc_be_send,unsigned short lenth,unsigned char uc_port_num)
{
    if(uc_port_num == 0)//for RS232,串口1--comm(0)
    {
#ifdef USE_DMA_FOR_USART1
        HAL_UART_Transmit_DMA(&huart1,uc_be_send,lenth);
#else
        //HAL_UART_Transmit_IT(&huart1,uc_be_send,lenth,60000);
#endif
    }
    else if(uc_port_num == 1)//for RS485,串口2--comm(1)
    {
#ifdef USE_DMA_FOR_USART2
        //HAL_UART_Transmit_DMA(&huart2,uc_be_send,lenth);       没有串口2了
#else
        //HAL_UART_Transmit_IT(&huart2,uc_be_send,lenth,60000);
#endif
    }
}

/////////////////////////////////////////////////////////////////////////////
//板载支持包：强制收发转换切换
/////////////////////////////////////////////////////////////////////////////
void BSP_Comm_485_ForceSend(unsigned char uc_is_send,st_Comm_TypeDef *p)
{ 
    switch(p->uc_port_num)
    {
        /************************************************************************************huart1**/
        case 0:   //huart1   可以作为485口
        {
            if(uc_is_send){USART1_DR_GPIO_Port->BRR = USART1_DR_Pin;}//发送拉低
            else{USART1_DR_GPIO_Port->BSRR = USART1_DR_Pin;}
        }break;
        /***********************************************************************************huart2**/
        case 1:   //huart2   
        { ;}break;
        default:{;}break;
    }
    //尽快完成端口转换， 下面拖延点时间
    if(uc_is_send){p->uc_status |= BIT(en_Comm_Status_is_sending);}
    else{p->uc_status &=~ BIT(en_Comm_Status_is_sending);}
    
}

/////////////////////////////////////////////////////////////////////////////
u8 ReadRx(u8 uc_port_num)
{
    switch(uc_port_num)
    {
        case 0:{return(huart1.Instance->RDR);}
        case 1:{return(0);} 
        default:{return(0);}
    }
}
/////////////////////////////////////////////////////////////////////////////
//串口DMA定时接收
//每毫秒检测DMA接收缓冲区是否还在接收数据,如果没有数据，直接把超时时间改为1.因为检测本身也延时了2.5字符
//2021-12-20 配合comm。c 的修改
/////////////////////////////////////////////////////////////////////////////
void rstDMA_rec(st_Comm_TypeDef *p)
{
    _CLI();
    HAL_UART_DMAStop(p->pUart);////关闭接收通道，以便更改接收的参数 
    ReadRx(p->uc_port_num);       //空读接收数据寄存器，避免DMA失效 
    HAL_UART_Receive_DMA(p->pUart,p->u8_RecBufP,p->COMM_ARRAY_Rec_XB);//打开DMA
    _SEI();
}

/////////////////////////////////////////////////////////////////////////////
//为了支持高速通讯，不在限制1ms调用一次
/////////////////////////////////////////////////////////////////////////////
void DMA_check_each_channel_rec_finish(st_Comm_TypeDef *p, u16 u16_Now_us)
{    
    //COMM没处理呢，计算rec_p 
    //if((p->uc_status & BIT(en_Comm_Status_is_RecFinished)) == 0) //COMM通讯没处理，通讯中
    {
        p->u16_rec_p = p->COMM_ARRAY_Rec_XB - p->pDMArx->Instance->CNDTR;  //__HAL_DMA_GETCOUNTER(&hdma_usart1_rx);//得到接收到的字节数 
        
        if(p->u16_rec_p != p->u16_DMA_last_rec_p)  //有变，还在接收中
        {
            p->u16_DMA_last_rec_p = p->u16_rec_p; 
            p->u16_DMA_Timeout_1ms_count = 0;   
        }
        else //没变
        {
            if(p->u16_rec_p) //非空，缓存中有数据存在，没有数据不用处理
            {
                if(++p->u16_DMA_Timeout_1ms_count >= p->uc_timeout_4bytes)  //产生超时   st_comm[0].uc_rec_timeout
                {
                    p->uc_rec_timeout = 1;
                    rstDMA_rec(p);
                }//等待comm循环--读取,并没关闭DMA，如果和COMM间隙仍有数据进入，rec_p并没变化,而且数据排在最尾   
                p->u16_DMA_rec_p_Remain0Cnt = 500;
            }
            else //rec_p==0
            {
                if(--p->u16_DMA_rec_p_Remain0Cnt == 0)
                {
                    p->u16_DMA_rec_p_Remain0Cnt = 500;
                    rstDMA_rec(p);//异常处理，如果100m秒后，接收缓冲区依然是0，则复位DMA
                }
            }
        }
    } 
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void DMA_check_All_channel_rec_finish(u16 u16_Now_us)
{
#ifdef USE_DMA_FOR_USART1
#if MCU_UART_NUM >= 1
    DMA_check_each_channel_rec_finish(&st_comm[0],u16_Now_us);
#endif
#endif
    
#ifdef USE_DMA_FOR_USART2
#if MCU_UART_NUM >= 2
    DMA_check_each_channel_rec_finish(&st_comm[1],u16_Now_us);
#endif
#endif
    
#ifdef USE_DMA_FOR_USART3
#if MCU_UART_NUM >= 3
    DMA_check_each_channel_rec_finish(&st_comm[2],u16_Now_us);
#endif
#endif
    
#ifdef USE_DMA_FOR_USART4
#if MCU_UART_NUM >= 4
    DMA_check_each_channel_rec_finish(&st_comm[3],u16_Now_us);
#endif
#endif
}

/////////////////////////////////////////////////////////////////////////////////////////////////////               
void DMA_check_each_channel_send_finish(st_Comm_TypeDef *p)
{
    if(p->uc_status & BIT(en_Comm_Status_is_sending))
    {
        if(((p->pUart->Instance->ISR & USART_ISR_TC) ) && ((p->pUart->Instance->ISR & USART_ISR_TXE) )) //空了，发完了
        {
            BSP_Comm_485_ForceSend(0, p);//转成收，并且is_sending=0    
            rstDMA_rec(p); //复位，为了接收做准备
        } 
    }
    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void DMA_check_All_channel_send_finish(void)
{
    
#ifdef USE_DMA_FOR_USART1
#if MCU_UART_NUM >= 1
    DMA_check_each_channel_send_finish(&st_comm[0]);
#endif
#endif
    
#ifdef USE_DMA_FOR_USART2
#if MCU_UART_NUM >= 2    
    DMA_check_each_channel_send_finish(&st_comm[1]);
#endif    
#endif
    
#ifdef USE_DMA_FOR_USART3
#if MCU_UART_NUM >= 3 
    DMA_check_each_channel_send_finish(&st_comm[2]);
#endif
#endif
    
#ifdef USE_DMA_FOR_USART4
#if MCU_UART_NUM >= 4 
    DMA_check_each_channel_send_finish(&st_comm[3]);
#endif
#endif
}               

/////////////////////////////////////////////////////////////////////////////
//输入共计40路
//从ID0-ID39分别存在：st_GPIO.u8_input[0],st_GPIO.u8_input[1],st_GPIO.u8_input[2],st_GPIO.u8_input[3],st_GPIO.u8_input[4]
//去抖滤波后分别在：st_GPIO.u8_filter_input[0-4]
//每一位为高时，表示有信号输入
////////////////////////////////////////////////////////////////////////////
u8 BSP_input(u8 uc_channel)
{
    u8 u8_temp = 0;
    switch(uc_channel)
    {
        case 0:
        {
            u8_temp = (u8)~(GPIOC->IDR & 0x00ff);
        }break;
        case 1:
        {
            u8_temp = (u8)~(GPIOD->IDR & 0x00ff);
            if((GPIOB->IDR & BIT(3))){u8_temp &= (~BIT(2));} else {u8_temp |= BIT(2);}
        }break;
        case 2:
        {
            u8_temp = (u8)~((GPIOD->IDR >> 8) & 0x00ff);
        }break;
        case 3:
        {
            u8_temp = (u8)~(GPIOE->IDR & 0x00ff);
        }break;
        case 4:
        {
            u8_temp = (u8)~((GPIOE->IDR >> 8) & 0x00ff);
        }break;
        default:{break;}
    }//end switch
    return u8_temp;
}
/////////////////////////////////////////////////////////////////////////////
//输出共计32路
//从OD0-OD31分别存在：st_GPIO.u8_out[0-3]
//硬件上是低电平有输出，在程序上颠倒过来，输出变量高时有输出
////////////////////////////////////////////////////////////////////////////
void BSP_output(void)
{
    
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//通讯驱动：通讯灯
//
/////////////////////////////////////////////////////////////////////////////
void DRV_Comm_Led(u8 LED_ON,st_Comm_TypeDef *C)
{
    //if(C->uc_port_num == 0)
    {
        //if(LED_ON == 0){Comm_LED_OFF;}else{Comm_LED_ON;}
    }
    
}

void bspFLASH_ClearFlag(void)
{
    //FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | 7FLASH_FLAG_WRPRTERR);//不加这句话在优化时好像会出问题
}

