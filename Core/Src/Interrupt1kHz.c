
#include "main.h"
#include "Interrupt1kHz.h"
#include "InterruptADC.h"
#include "Table.h"

void ISR_1kHz(void)
{
    //////////////////////////////////////////////////////////////////////// VbusAvgCal();输出电压采样求平均计算
    //每ms都获得，不论±半周
    static long VSum = 0;
    VSum = VSum + (SADC.Vbus >> 3) - (VSum >> 2);    //母线电压滑动平均，4次是4ms
    Vbus.Avg = VSum >> 2;   //实际上每ms都获得新的母线电压
    ////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// VloopKpKiCal(); 电压环变增益函数计算
    //电压环Kp和Ki变增益，当BUS电压过高或者过低时能够快速调节会参考值
#define VLOOP_KP 2000//我计算出来是6400+ ，范例是 3079
#define VLOOP_KI  100//我计算出来是 400+ ，范例是 162
#define V_OFFSET 1207//参考值+30V Q15->814V
#define V_OFFSET_RE 805
    
    if(Flag.ssFinsh)   //只有在启动结束后才进行电压环变增益
    {
        //当正BUS电压在正常范围内，退出变增益 上下正负20V
        if( (Vbus.Avg < (Vbus.Ref + V_OFFSET_RE))  &&  (Vbus.Avg > (Vbus.Ref - V_OFFSET_RE)) )
        {
            Vloop.u10_kp = VLOOP_KP;//正常KP KI
            Vloop.u10_ki = VLOOP_KI;//正常KP KI
        }
        else if(Vbus.Avg > ( Vbus.Ref + V_OFFSET))//电压过大变增益
        {
            Vloop.u10_kp = VLOOP_KP * 4;//*倍变增益KP
            Vloop.u10_ki = VLOOP_KI * 4;//*倍变增益KI
        }
        else if(Vbus.Avg < (Vbus.Ref - V_OFFSET))//电压过小变增益
        {
            Vloop.u10_kp = VLOOP_KP * 2;//*倍变增益KP
            Vloop.u10_ki = VLOOP_KI * 2;//*倍变增益KI
        }
    }
    else
    {
        Vloop.u10_kp = VLOOP_KP;
        Vloop.u10_ki = VLOOP_KI;
    }
    //Vloop.u10_kp =2639;
    //Vloop.u10_ki = 1391;
    ////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// VoltageLoop();恒压环路
    Vloop.Err = Vbus.Ref - Vbus.Avg;   //BUS电压环路计算，bus电压与参考量之间误差  Q15
    Vloop.Inte += (Vloop.Err  * Vloop.u10_ki); //计算BUS电压环积分量=误差*KI
    if(Vloop.Inte > 33550000)   {Vloop.Inte = 33550000;} //Q10*Q15
    else if(Vloop.Inte < 0)     {Vloop.Inte = 0;} //对积分尽量进行最大最小值限制，防止溢出
    
    //计算BUS电压环输出量= 积分量+误差*KP
    Vloop.out = (Vloop.Inte + Vloop.Err * Vloop.u10_kp) >> 10;
    //对环路输出最大最小进行限制
    if(Vloop.out < 0){Vloop.out = 0;}       
    if(Vloop.out > 32767){Vloop.out = 32767;}   //Q15 
    ////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// VrefCal();BUS参考电压处理  
    if((Flag.PFCState == Run) || (Flag.PFCState == Rise))//只有在PFC运行状态下处理
    {
        if( Vbus.Ref < VBUS_REF) //30180 750V 采样上限是814.2V 750/814.2*32768=30814
        {Vbus.Ref += 10;}//当参考电压小于VBUS_REF 缓慢增加           
        
        ////////////////////////////////////////////////////////////////////////VbusLimit();OVP函数-母线电压限制
        static long OVPFlag = 0;//只有在PFC运行状态下处理
        if(Vbus.Avg > 32197)//当bus出现过压限制 800v
        {
            OVPFlag = 1;    //标志位置位
            Flag.PWM = 0;   //关闭PWM波            
            Iac.RefssK = 0; //当出现过压时，电流参考量重新软起
            ILoop.Inte = 0; //当出现过压时，电流环积分量=0
        }
        
        if(OVPFlag && (Vbus.Avg < 30989)) //当发生过压后770v V.BUSovpredsp
        {
            OVPFlag = 0;    //标志位清零
            Flag.PWM = 1;   //电压恢复到理想值后复位
            Iac.RefssK = 0;
            ILoop.Inte = 0;
        }
    }
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////VbusUVP();母线欠压保护，防止驱动损坏的情况下输出电压不能满足要求
    //只有在PFC运行状态下处理当,BUS电压 < 整流电压-50V时，发生欠压保护，PFC关闭PWM
    if((Vbus.Avg < (Vac.Rms - 2012)) && (Flag.PFCState == Run))// 50V/814V*Q15=2012
    {
        Flag.PWM = 0;//关闭PWM波
        Flag.Err |= ERR_UVP;//硬件标志位置位
        Flag.PFCState = Err;//进入故障状态 
        PFCokDisable() ;//告知LLCPFC有故障
    }
    ////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// VIacRmsCal();计算AC有效值计算  
#define AVG_TO_RMS  18198 //正弦平均值至有效值转换系数  Q14 PI/(2*1.414)
#define RMS_TO_PEAK 23166 //正弦有效值至最大值转换系数  Q14 1.414
    //电压电流有效值的平均值滑动滤波求和， 每20ms计算一次
    static long VacRmsSum = 0,IacRmsSum = 0;
    
    //有效值计算准备标志位置1，在进输入进入正半周期电压电流累加完成，进入负半周期>100次计数后该标志位置1
    //程序要正半周期进入数值累加，在负半周期进行计算
    if(Flag.RmsCalReady) //每20ms    //if 内部的局部变量，出去了应该回收 
    {
        long couter = (long)65536 / PLL.PosCnt;      //累加标定，常规数字平均值累加求平均计算  
        //限幅
        if(couter <  91){couter =  91;}//45Hz            
        if(couter > 131){couter = 131;}//65Hz
           
        //电压平均值 = 电压累加值/累加计数量 。电压有效值 = 电压平均值*平均值至有效值转换系数
        long VacRmsTemp = (Vac.Sum * couter >> 16) * AVG_TO_RMS >> 14;
        //电流平均值 = 电流累加值/累加计数量 。电流有效值 = 电流平均值*平均值至有效值转换系数
        long IacRmsTemp = (Iac.Sum * couter >> 16) * AVG_TO_RMS >> 14;
        
        Vac.Sum = Iac.Sum = PLL.PosCnt = Flag.RmsCalReady = 0;    //清电压电流累加量
        
        VacRmsSum = VacRmsSum + VacRmsTemp - (VacRmsSum >> 2);//4个波的平均，80ms@50Hz
        Vac.Rms = VacRmsSum >> 2; //电压有效值的平均值
        
        IacRmsSum = IacRmsSum + IacRmsTemp - (IacRmsSum >> 2);//原文>>3
        Iac.Rms = IacRmsSum >> 2; //电流有效值的平均值
        
        Vac.Peak= Vac.Rms * RMS_TO_PEAK >> 14;  //电压最大值（正弦量）=R相电压有效值*1.414
    }
    ////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// DCMFeedCal();DCM模式下前馈系数计算
#define DCM_FEED_COEFF   2516    //DCM情况下计算电流前馈系数Q10
#define MIN_POWER_RE 18415395   //前馈加入功率恢复点Q30 200W
#define MIN_POWER    13811546   //当输出功率小时，去前馈功率动作点Q30 150W

    long SqrtP,SqrtPAvg;//电压有效值、电流有效值、功率开根号,功率开根号平均,
    long Temp = 0;
    static long SqrtPSum = 0;//功率开根号求平均和
    static long U30 = 1073741824;

    //SqrtP = (*(u15SqrU10 + (int)(Vac.Rms >> 5))) * (*(u15SqrU10 + (int)(Iac.Rms >> 5))) >> 15; //计算功率开根号，查表法Q15
    SqrtP = (u15SqrU10[Vac.Rms >> 5] * u15SqrU10[Iac.Rms >> 5]) >> 15; 
    
    SqrtPSum = SqrtPSum + SqrtP - (SqrtPSum >> 5); //32次，32ms？
    SqrtPAvg = SqrtPSum >> 5; 
    
    //计算电压峰值倒数限制，防止电压过小，倒数计算过大，限制170的峰值
    if(Vac.Peak < 23444){Temp = 23444;} //300v对应的峰值
    else{Temp = Vac.Peak;}
        
    //计算Peak倒数Q15
    Temp = U30 / Temp;
    //计算DCM前馈系数Q10
    Feed.dcmK = (((SqrtPAvg * Temp) >> 15) * DCM_FEED_COEFF) >> 15;
    
    //计算过程对电压进行限幅300V,避免求倒数值过大 AC电压最低值有效值Q15
    if(Vac.Rms < 16577){Temp = 16577;} //300/593*32768
    else{Temp = Vac.Rms;} //380/593*Q12=20998
        
    //Vac.u12Rmsover2=1/(average phase voltage)^2
    Vac.u12Rmsover2 = ((U30 / Temp) << 12) / Temp;//VacRmsTemp 9975@380
    
    Temp = Vac.Rms * Iac.Rms; //计算单相功率平均值PowerAc
    //当输出功率较小时，取消前馈
    if(Temp < MIN_POWER) //PowerAc
    {
        Flag.LowP = 1;//标志位置1
        Feed.ssK = 0;//取消前馈
    }
    if(Flag.LowP && (Temp > MIN_POWER_RE))//PowerAc
    {
        Flag.LowP  = 0;
    }
    //////////////////////////////////////////////////////////////////////// IloopKpKiCal();电流环变增益

#define DSP_ILOOP_KP_K  -586    //kp变增益计算函数K值Q12//-2850
#define DSP_ILOOP_KP_B  2346    //kp变增益计算函数B值//7979
    
#define DSP_ILOOP_KI_K  -666    //ki变增益计算函数K值Q12//-2898
#define DSP_ILOOP_KI_B  2666    //kI变增益计算函数B值//8114    
    
#define MAX_ILOOP_KP    2011    //电流环KP最大值限制//6839
#define MIN_ILOOP_KP    1005    //电流环KP最小限制//3420
#define MAX_ILOOP_KI    2285    //电流环KI最大值限制//6955
#define MIN_ILOOP_KI    1143    //电流环KI最小限制//3478
    static long IacRmsAvg = 0,IacRms8Sum = 0;
    //滑动求平均-8次
    IacRms8Sum = IacRms8Sum + Iac.Rms - (IacRms8Sum >> 3);
    IacRmsAvg = IacRms8Sum >> 3;
    
    //计算电流环KI变增益，随着电压越大，kp值越大
    ILoop.ki = ((IacRmsAvg * DSP_ILOOP_KI_K) >> 12) + DSP_ILOOP_KI_B;
    //kI最大最小值限制
    if(ILoop.ki > MAX_ILOOP_KI){ILoop.ki = MAX_ILOOP_KI;}
    if(ILoop.ki < MIN_ILOOP_KI){ILoop.ki = MIN_ILOOP_KI;}
        
    //计算电流环KP变增益，随着电压越大，kp值越大
    ILoop.kp = ((IacRmsAvg * DSP_ILOOP_KP_K) >> 12) + DSP_ILOOP_KP_B;
    //kp最大最小值限制
    if(ILoop.kp > MAX_ILOOP_KP){ILoop.kp = MAX_ILOOP_KP;}
    if(ILoop.kp < MIN_ILOOP_KP){ILoop.kp = MIN_ILOOP_KP;}
    //ILoop.kp = 3700;
    //ILoop.ki  = 453;
    ////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// Feedss();前馈系数软启
//前馈软启增量减量
#define FEED_SS_STEP 100
    if(Flag.ssFinsh)//软启动过程结束后，软启动系数逐渐增加
    {
        if(Feed.ssK < 32767)//电流环前馈软启系数最大值限制Q15 系数为1  
        {
            Feed.ssK = Feed.ssK + FEED_SS_STEP;
            if(Feed.ssK > 32767){Feed.ssK = 32767;}
        }
    }
    ////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// Irefss();电流参考值软启
#define IREF_SS_STEP 100
    if(Iac.RefssK < 32767)//电流环前馈软启系数最大值限制Q15 系数为1
    {
        Iac.RefssK = Iac.RefssK + IREF_SS_STEP;
        if(Iac.RefssK > 32767){Iac.RefssK = 32767;}
    }
}


