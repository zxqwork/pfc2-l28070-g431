#include "app_var.h"
#include "Comm.h"
#include "Modbus.h"
#include "sys_var.h"
#include "dma.h"
#include "main.h" //用到定义 hdma


vu32 gu32SysFlag = 0;
vu32 gu32_1ms    = 0;

SYS_Type SYS;

__flash st_CommVarListTypedef u16_CommVar[] =
{	
        &SYS.u16_app_ver,
        &SYS.u16_app_ver,
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//      初始化系统变量
///////////////////////////////////////////////////////////////////////////////////////////////////////////
void init_sys_var(void)
{
    
    SYS.u16_app_ver = 0;
    
    st_comm[0].uc_port_num			= 0;
    st_comm[0].en_protocol = en_Comm_protocol_Modbus; 
    st_comm[0].uc_is_asc = 0;//RTU模式
    st_comm[0].uc_rec_timeout		= 0;
    st_comm[0].uc_timeout_4bytes	= 2;//{10,6,5,3,3,3,3}对应{48,96,192,384,576,1152, 1875};
    st_comm[0].u16_rec_p			= 0;
    st_comm[0].uc_status			= 0;
    st_comm[0].uc_ThCRC             = 0;
    st_comm[0].uc_client            = 1;//PLC地址
    st_comm[0].uc_Master_OP_Lenth   = OP_TABLE_LENTH_0;
    st_comm[0].u16_master_send_interval = 50; 
    st_comm[0].uc_master_slaver_status = BIT(en_Comm_SLAVER_SEND_EN) | BIT(en_Comm_SLAVER_REC_EN);//; BIT(en_Comm_MASTER_SEND_EN) | BIT(en_Comm_MASTER_REC_EN) | BIT(en_Comm_DisableSpeedUp);
    
    st_comm[0].st_Master_OP = st_Master0_OP;
    st_comm[0].u8_SendBufP = u8_SendBuf0;
    st_comm[0].u8_RecBufP = u8_RecBuf0;
    st_comm[0].COMM_ARRAY_Rec_XB = COMM0_ARRAY_Rec_XB;
    st_comm[0].COMM_ARRAY_SendXB = COMM0_ARRAY_SendXB;
    st_comm[0].pDMArx = &hdma_usart1_rx;
    st_comm[0].pDMAtx = &hdma_usart1_tx;
    st_comm[0].pUart = &huart1;
    
    //应用程序选择那些OP执行
    st_comm[0].st_Master_OP[0].uc_status =  BIT(en_CommOP_MASTER_SEND_EN);
    st_comm[0].st_Master_OP[0].u8_op_protocol = en_Comm_protocol_Modbus;
    st_comm[0].st_Master_OP[0].uc_slaver_client = 1;
    st_comm[0].st_Master_OP[0].uc_cmd = MB_READ_HOLD_REG;
    st_comm[0].st_Master_OP[0].ui_mem_begin_addr = 0;
    st_comm[0].st_Master_OP[0].ui_plc_begin_addr = 0;
    st_comm[0].st_Master_OP[0].u16_lenth = 2;
    st_comm[0].st_Master_OP[0].u8_MasterFreq = 1;//发送多个OP表时，需要发送的频率。如果频率为2，那么，发送全部的OP表两遍时，才会发送本OP表一遍。

    /*
    //---------------------RS485通讯口，对应stm32的串口2
    //
    st_comm[1].uc_port_num			= 1;
    st_comm[1].en_protocol = en_Comm_protocol_Modbus;
    st_comm[1].uc_is_asc   = 0;//RTU模式
    st_comm[1].uc_rec_timeout		= 0;
    st_comm[1].uc_timeout_4bytes	= 2;//{10,6,5,3,3,3,3}对应{48,96,192,384,576,1152, 1875};
    st_comm[1].u16_rec_p			= 0;
    st_comm[1].uc_status			= 0;
    st_comm[1].uc_ThCRC             = 0;
    st_comm[1].uc_client            = 1;
    st_comm[1].uc_Master_OP_Lenth   = OP_TABLE_LENTH_1;
    st_comm[1].u16_master_send_interval = 20; //
    st_comm[1].uc_master_slaver_status =  BIT(en_Comm_SLAVER_SEND_EN) | BIT(en_Comm_SLAVER_REC_EN);
    
    st_comm[1].st_Master_OP = st_Master1_OP;
    st_comm[1].u8_SendBufP = u8_SendBuf1;
    st_comm[1].u8_RecBufP = u8_RecBuf1;
    st_comm[1].COMM_ARRAY_Rec_XB = COMM1_ARRAY_Rec_XB;
    st_comm[1].COMM_ARRAY_SendXB = COMM1_ARRAY_SendXB;
    st_comm[1].pDMArx = &hdma_usart2_rx;
    st_comm[1].pDMAtx = &hdma_usart2_tx;
    st_comm[1].pUart = &huart2;
    
    //此口为485， 为了兼容光口，电源接电口光口都行
    //5008扩展模块 地址4
	st_comm[1].st_Master_OP[0].uc_status = BIT(en_CommOP_MASTER_SEND_EN); //
	st_comm[1].st_Master_OP[0].u8_op_protocol = en_Comm_protocol_Modbus;
    st_comm[1].st_Master_OP[0].uc_slaver_client = 1;
    st_comm[1].st_Master_OP[0].uc_cmd = MB_READ_HOLD_REG;
    st_comm[1].st_Master_OP[0].ui_mem_begin_addr = 110; //读
    st_comm[1].st_Master_OP[0].ui_plc_begin_addr = 0;
    st_comm[1].st_Master_OP[0].u16_lenth = 2;
    st_comm[1].st_Master_OP[0].uc_Master_RecErrCnt = 0;
    st_comm[1].st_Master_OP[0].u8_MasterFreq = 1; 
    */
}