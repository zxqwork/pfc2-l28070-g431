/* USER CODE BEGIN Header */
/**
******************************************************************************
* @file           : main.c
* @brief          : Main program body
******************************************************************************
* @attention
*
* Copyright (c) 2023 STMicroelectronics.
* All rights reserved.
*
* This software is licensed under terms that can be found in the LICENSE file
* in the root directory of this software component.
* If no LICENSE file comes with this software, it is provided AS-IS.
*
******************************************************************************

think about IAP
think about IAP
think about IAP  
*/
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dac.h"
#include "dma.h"
#include "iwdg.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "base.h"
#include "BSP.h"
#include "app_var.h"
#include "InterruptADC.h"
#include "Interrupt200Hz.h"
#include "Table.h"
#include "Comm.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
int main(void);

/* USER CODE END 0 */

/**
* @brief  The application entry point.
* @retval int
*/
int main(void)
{
    /* USER CODE BEGIN 1 */
    
    /* USER CODE END 1 */
    
    /* MCU Configuration--------------------------------------------------------*/
    
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();
    
    /* USER CODE BEGIN Init */
    
    /* USER CODE END Init */
    
    /* Configure the system clock */
    SystemClock_Config();
    
    /* USER CODE BEGIN SysInit */
    
    HAL_DMA_DeInit(&hdma_usart1_tx);  
    
    /* USER CODE END SysInit */
    
    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DMA_Init();
    MX_IWDG_Init();
    MX_ADC1_Init();
    MX_ADC2_Init();
    MX_DAC1_Init();
    MX_SPI3_Init();
    MX_TIM2_Init();
    MX_USART1_UART_Init();
    MX_TIM3_Init();
    /* USER CODE BEGIN 2 */
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    RelayOff();
    SS_Disable();
    
    HAL_ADCEx_Calibration_Start(&hadc1,ADC_SINGLE_ENDED);
    HAL_ADCEx_Calibration_Start(&hadc2,ADC_SINGLE_ENDED);
    
    
    init_sys_var();
    HAL_UART_Receive_DMA(&huart1,st_comm[0].u8_RecBufP,st_comm[0].COMM_ARRAY_Rec_XB);//打开DMA接收
    //通讯完全使用DMA查询模式， 不需要开DMA接受和发送中断，也不用开串口中断
    
    HAL_DAC_Start(&hdac1,DAC_CHANNEL_1);
    HAL_DAC_Start(&hdac1,DAC_CHANNEL_2);
    HAL_DAC_SetValue(&hdac1,DAC_CHANNEL_1,DAC_ALIGN_12B_R,0);
    HAL_DAC_SetValue(&hdac1,DAC_CHANNEL_2,DAC_ALIGN_12B_R,0);
    
    
    htim3.Instance->CCR1 = 26001; 
    HAL_TIM_Base_Start(&htim3);
    HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1);
    
    __HAL_ADC_ENABLE_IT(&hadc1,ADC_IT_EOC); 
    __HAL_ADC_ENABLE_IT(&hadc2,ADC_IT_EOC);
    HAL_ADC_Start_IT(&hadc1); 
    HAL_ADC_Start_IT(&hadc2);
    HAL_ADCEx_InjectedStart(&hadc1);//?
    HAL_ADCEx_InjectedStart(&hadc2);
    
    __HAL_SPI_ENABLE(&hspi3);
    
    HAL_IWDG_Refresh(&hiwdg); 
    VariableInit();  //所有变量初始化
    while(gu32_1ms < 500)
    {
        LED0_GPIO_Port->BRR = LED0_Pin;
        LED1_GPIO_Port->BRR = LED1_Pin;
        //LED2_GPIO_Port->BRR = LED2_Pin; 等待/快闪软起/慢闪工作
        LED3_GPIO_Port->BRR = LED3_Pin;
        HAL_IWDG_Refresh(&hiwdg);
    } //for test 看是否重启了
    LED0_GPIO_Port->BSRR = LED0_Pin;
    LED1_GPIO_Port->BSRR = LED1_Pin;
    LED2_GPIO_Port->BSRR = LED2_Pin;
    LED3_GPIO_Port->BSRR = LED3_Pin;
    
    /* USER CODE END 2 */
    
    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    while (1)
    {
        
        if(gu32SysFlag & SYS_FLAG_1MS_INT)
        {
            gu32SysFlag &=~ SYS_FLAG_1MS_INT;
            //看门狗的知识 https://blog.csdn.net/qq153471503/article/details/117804429
            HAL_IWDG_Refresh(&hiwdg);
            
            DMA_check_All_channel_send_finish();  
            DMA_check_All_channel_rec_finish((u16)gu32_1ms);
            
            about_comm(&st_comm[0],(u16)gu32_1ms); //必须1ms调用一次
            
            static unsigned short su16_5ms = 5;
            if(--su16_5ms == 0)
            {
                su16_5ms = 5; 
                
                ErrCheck();//输入电压保护
                StateM();//PFC状态机函数
                
                static unsigned short su16_100ms = 10;
                if(--su16_100ms == 0){su16_100ms = 10; LED0_GPIO_Port->ODR ^= LED0_Pin;}
            }
        }
        
        /* USER CODE END WHILE */
        
        /* USER CODE BEGIN 3 */
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    /* USER CODE END 3 */
}

/**
* @brief System Clock Configuration
* @retval None
*/
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    
    /** Configure the main internal regulator output voltage
    */
    HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);
    
    /** Initializes the RCC Oscillators according to the specified parameters
    * in the RCC_OscInitTypeDef structure.
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV5;
    RCC_OscInitStruct.PLL.PLLN = 68;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV6;
    RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
    RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    
    /** Initializes the CPU, AHB and APB buses clocks
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
        |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
    {
        Error_Handler();
    }
}

/* USER CODE BEGIN 4 */
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* USER CODE END 4 */

/**
* @brief  This function is executed in case of error occurrence.
* @retval None
*/
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1)
    {
    }
    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
* @brief  Reports the name of the source file and the source line number
*         where the assert_param error has occurred.
* @param  file: pointer to the source file name
* @param  line: assert_param error line source number
* @retval None
*/
void assert_failed(uint8_t *file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
